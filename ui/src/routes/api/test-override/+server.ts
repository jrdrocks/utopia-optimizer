import { error } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import { JULIA_SERVER } from '$env/static/private';

export const POST: RequestHandler = async ({ request }) => {
    try {
        const json= await request.json();
        
        const response = await fetch(JULIA_SERVER + 'test-override', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(json)
        });

        if (!response.ok) {
            throw error(response.status, await response.text());
        }

        const result = await response.json();
        return new Response(JSON.stringify(result));
    } catch (err) {
        throw error(500, err instanceof Error ? err.message : 'Unknown error');
    }
}
