import { error } from '@sveltejs/kit';
import type {  RequestHandler } from './$types'
import hash from 'hash-it';

import { JULIA_SERVER } from '$env/static/private';
import { keyv } from '$lib/kv';



async function get_data(json_request: any)  {

    let h = hash(json_request).toString(16);

    if(await keyv.has(h)) {
        console.log(`cached result for ${h}`);
        return await keyv.get(h);
    }

    let response = await fetch(JULIA_SERVER + 'optimize', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(json_request)
    });

    let resp = await response.json();

    await keyv.set(h, resp);
    await keyv.set(`${h}-req`, json_request);

    console.log(resp);
    return resp
}

export const POST: RequestHandler = async ({ request }) => {
    let json = await request.json()
   let result = await get_data(json)
    return new Response(JSON.stringify(result));
}