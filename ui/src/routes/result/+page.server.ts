import { error } from '@sveltejs/kit';
import type { PageServerLoad } from './$types';

import { keyv } from '$lib/kv';

export const load: PageServerLoad = async ({ url }) => {
    let h = url.search.slice(1)
    let request = await keyv.get(`${h}-req`)
    if (request) {
        console.log("found cached request for " + h);
        return { cached_request: request }
    } else {
        return { cached_request: null }
    }
};