import { current_optimizer, navigationSource } from '$lib/store';

import { get } from 'svelte/store';
import OptimizerResult from '$lib/components/OptimizerResult.svelte';
import type { PageLoad } from './$types';
import { recent_optimizer_forms } from '$lib/store';

export const load: PageLoad = ({ data }) => {
    let request = get(current_optimizer);
    if (data.cached_request) {
        request = data.cached_request

    }
    let from_nav = false;
    navigationSource.subscribe(value => {
        from_nav = value.from_nav;
    })(); // Immediately invoke to unsubscribe

    if (!from_nav) {
        if (data.cached_request?.form?.ALL_inputs) {
            console.log("direct request, load values")
            let inputs = data.cached_request?.form?.ALL_inputs
            inputs.tabSet=0
            recent_optimizer_forms.update((old) => {
                old.unshift(data.cached_request?.form?.ALL_inputs);
                old.splice(25);
                return old;
            });
        }
    }
    navigationSource.set({ from_nav: false })
    console.log("load", request)
    return {
        drawer: {
            el: OptimizerResult,
            props: { request: request }
        }
    }
} 