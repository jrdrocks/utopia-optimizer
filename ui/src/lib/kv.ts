import Keyv from 'keyv'; 
import { SQLITE_FILE } from '$env/static/private';

export const keyv = new Keyv(SQLITE_FILE);