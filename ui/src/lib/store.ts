import { localStorageStore } from '@skeletonlabs/skeleton';
import type { Writable } from 'svelte/store';
import { writable } from 'svelte/store';

import base_data from './data';

let extra_options = base_data.EXTRA_TARGETS;
let building_options = base_data.BUILDINGS;
let science_options = base_data.SCIENCES;

let inputs = {
    tabSet: 0,
    self_spells: {
        ...base_data.SELF_SPELLS.reduce((acc, cur) => ({ ...acc, [cur]: false }), {})
    },
    races: {
        ...base_data.RACES.reduce((acc, cur) => ({ ...acc, [cur]: false }), {})
    },
    personalities: {
        ...base_data.PERSONALITIES.reduce((acc, cur) => ({ ...acc, [cur]: false }), {})
    },
    opt_target: {
        DPA: -1,
        OPA: -1,
        OPA_DPA_RATIO: -1
    },
    optimize_target: 'OPA',
    building_options_entries: [
        ...building_options.map((e) => ({
            name: e,
            min: -1,
            max: -1,
            eq: -1,
            as_effective: false
        }))
    ],
    science_options_entries: [...science_options.map((e) => ({ name: e, value: -1 }))],
    extra_options_entries: [...extra_options.map((e) => ({ name: e, value: -1 }))],
    RACE: '',
    PERS: '',
    HONOR: 'PEASANT',
    ACRES: 1000,
    SCIENCE: -1,
    SCIENCE_DAYS: 28,
    INCOME: 10000,
    RUNES: 1000,
    MOD_WPA: 1,
    MOD_TPA: 2,
    overrides: [],
};

export const ensure_uptodate_inputs = (given_input: any) => {
    // Update for self_spells, races, and personalities
    const updateKeys = (template: any, given: any) => {
        const updated = { ...given };
        // Add missing keys
        Object.keys(template).forEach(key => {
            if (updated[key] === undefined) {
                updated[key] = template[key];
            }
        });
        // Remove keys not in the template
        Object.keys(updated).forEach(key => {
            if (template[key] === undefined) {
                delete updated[key];
            }
        });
        return updated;
    };

    given_input.self_spells = updateKeys(inputs.self_spells, given_input.self_spells || {});
    given_input.races = updateKeys(inputs.races, given_input.races || {});
    given_input.personalities = updateKeys(inputs.personalities, given_input.personalities || {});
 
};


export const recent_optimizer_forms: Writable<[any]> = localStorageStore('_uo_109_004', [inputs]);


 
export const current_optimizer: Writable<any> = writable(null);

 

export const navigationSource = writable({ from_nav: false });