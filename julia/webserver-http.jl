include("src/utopia.jl")

module Main
using ..Utopia
using ..DataStructs

using HTTP, JSON3, StructTypes, Setfield
using UUIDs: uuid4

# Your existing struct definitions remain the same
struct Form
    RACE_selected::Vector{RACE_OPTS.RACE}
    PERS_selected::Vector{PERS_OPTS.PERS}
end

StructTypes.StructType(::Type{Form}) = StructTypes.Struct()
StructTypes.StructType(::Type{OptimizationTarget}) = StructTypes.Struct()
StructTypes.StructType(::Type{ExtraOptimizationTarget}) = StructTypes.Struct()

struct Overrides
    races::Union{Vector{RACE_OPTS.RACE},Nothing}
    personalities::Union{Vector{PERS_OPTS.PERS},Nothing}
    override::ExtraOptimizationTarget
end

StructTypes.StructType(::Type{Overrides}) = StructTypes.Struct()


function merge_optimization_targets(base::OptimizationTarget, extra::ExtraOptimizationTarget)
    # Create a new OptimizationTarget with the same values as base
    result = deepcopy(base)

    # Iterate through all fields of ExtraOptimizationTarget
    for field in fieldnames(typeof(extra))
        extra_value = getfield(extra, field)

        # Skip if the extra value is nothing
        if extra_value === nothing
            continue
        end

        base_value = getfield(base, field)

        # Handle different types of fields
        if extra_value isa Dict
            # For dictionaries, merge/replace keys and handle nothing values
            merged_dict = deepcopy(base_value)
            for (k, v) in extra_value
                if v === nothing
                    # Remove the key if value is nothing
                    delete!(merged_dict, k)
                else
                    # Otherwise, set or update the value
                    merged_dict[k] = v
                end
            end
            setfield!(result, field, merged_dict)
        elseif extra_value isa AbstractArray
            # For arrays, append new values
            merged_array = vcat(base_value, extra_value)
            setfield!(result, field, merged_array)
        else
            # For other types, simply replace the value
            setfield!(result, field, extra_value)
        end
    end

    return result
end

# Global state storage
const optimization_states = Dict{String,Any}()
const state_lock = ReentrantLock()

# Route handlers
function handle_variables(req::HTTP.Request)
    response = Dict(
        :RACES => string.(instances(DataStructs.RACE_OPTS.RACE)),
        :PERSONALITIES => string.(instances(DataStructs.PERS_OPTS.PERS)),
        :HONORS => string.(instances(DataStructs.HONOR_OPTS.HONOR)),
        :BUILDINGS => string.(instances(DataStructs.BUILDING_OPTS.BUILDING)),
        :SCIENCES => string.(instances(DataStructs.SCIENCE_OPTS.SCIENCE)),
        :SCIENCE_GROUPS => string.(instances(DataStructs.SCIENCE_GROUP_OPTS.SCIENCE_GROUP)),
        :SELF_SPELLS => string.(instances(DataStructs.SELF_SPELLS.SPELL)),
        :EXTRA_TARGETS => string.(instances(DataStructs.EXTRA_TARGETS.EXTRA_TARGET))
    )
    return HTTP.Response(200, JSON3.write(response))
end

function handle_optimize(req::HTTP.Request)
    payload = JSON3.read(String(req.body))
    form = JSON3.read(JSON3.write(payload.form), Form)
    target = JSON3.read(req.body, OptimizationTarget)

    overrides = try
        JSON3.read(JSON3.write(payload.OVERRIDES), Vector{Overrides})
    catch
        []
    end

    result = []

    for race in form.RACE_selected
        for pers in form.PERS_selected
            current_target = deepcopy(target)
            current_target.RACE = race
            current_target.PERS = pers

            if overrides !== nothing
                for override in overrides
                    if override.races !== nothing || override.personalities !== nothing
                        #println("race: $(race), type: $(typeof(race))")
                        #println("override.races: $(override.races), type: $(typeof(override.races))")
                        #println("override.personalities: $(override.personalities), type: $(typeof(override.personalities))")
                        if (isnothing(override.races) || isempty(override.races) || race in override.races) &&
                           (isnothing(override.personalities) || isempty(override.personalities) || pers in override.personalities)
                            println("Override race {} and pers {}", race, pers)
                            current_target = merge_optimization_targets(current_target, override.override)
                            #println(JSON3.write(current_target))
                        end
                    end
                end
            end

            (frames, values, definitions) = Utopia.optimizeProvince(current_target, true)
            if (isnan(values[2]))
                push!(result, (race, pers, "infeasible"))
            else
                push!(result, (race, pers, Dict(
                    :frames => frames,
                    :values => values,
                    :definitions => definitions,
                    :target => current_target
                )))
            end
        end
    end

    return HTTP.Response(200, JSON3.write(result))
end

function handle_optimize_progress(req::HTTP.Request)
    body = String(req.body)
    @show payload = JSON3.read(body)
    form = JSON3.read(JSON3.write(payload.form), Form)
    target = JSON3.read(String(body), OptimizationTarget)

    overrides = try
        JSON3.read(JSON3.write(payload.OVERRIDES), Vector{Overrides})
    catch
        []
    end

    id = string(uuid4())

    lock(state_lock) do
        optimization_states[id] = Dict(
            :form => form,
            :target => target,
            :overrides => overrides,
            :total => length(form.RACE_selected) * length(form.PERS_selected),
            :completed => 0
        )
    end

    return HTTP.Response(200, JSON3.write(Dict(:id => id)))
end

function handle_optimize_progress_stream(stream::HTTP.Stream)
    HTTP.setheader(stream, "Access-Control-Allow-Origin" => "*")
    HTTP.setheader(stream, "Access-Control-Allow-Methods" => "GET, OPTIONS")
    HTTP.setheader(stream, "Content-Type" => "text/event-stream")


    id = HTTP.getparams(stream.message)["id"]

    state = lock(state_lock) do
        get(optimization_states, id, nothing)
    end

    if state === nothing
        println("Invalid ID")
        write(stream, "data: $(JSON3.write(Dict(:error => "Invalid ID")))\n\n")
        return nothing
    else

        total = state[:total]
        completed = 0

        result = []

        form = state[:form]
        target = state[:target]
        overrides = state[:overrides]

        for race in form.RACE_selected
            for pers in form.PERS_selected
                try
                    current_target = deepcopy(target)
                    current_target.RACE = race
                    current_target.PERS = pers

                    if overrides !== nothing
                        for override in overrides
                            if override.races !== nothing || override.personalities !== nothing
                                #println("race: $(race), type: $(typeof(race))")
                                #println("override.races: $(override.races), type: $(typeof(override.races))")
                                #println("override.personalities: $(override.personalities), type: $(typeof(override.personalities))")
                                if (isnothing(override.races) || isempty(override.races) || race in override.races) &&
                                (isnothing(override.personalities) || isempty(override.personalities) || pers in override.personalities)
                                    println("Override race {} and pers {}", race, pers)
                                    current_target = merge_optimization_targets(current_target, override.override)
                                    #println(JSON3.write(current_target))
                                end
                            end
                        end
                    end

                    write(stream, "data: $(JSON3.write(Dict(:progress => [race, pers, completed / total])))\n\n")
                    @show current_target
                    @show (frames, values, definitions) = Utopia.optimizeProvince(current_target, true)
                    if (isnan(values[2]))
                        push!(result, (race, pers, "infeasible"))
                    else
                        push!(result, (race, pers, Dict(
                            :frames => frames,
                            :values => values,
                            :definitions => definitions,
                            :target => current_target
                        )))
                    end
                catch
                    push!(result, (race, pers, "infeasible"))
                end

                completed += 1
                write(stream, "data: $(JSON3.write(Dict(:progress => [race, pers, completed / total])))\n\n")
            end
        end

        lock(state_lock) do
            delete!(optimization_states, id)
        end

        write(stream, "data: $(JSON3.write(Dict(:done => result), allow_inf=true))\n\n")
    end

    return nothing
end

# Router setup
const ROUTER = HTTP.Router()

HTTP.register!(ROUTER, "GET", "/variables", HTTP.streamhandler(handle_variables))
HTTP.register!(ROUTER, "POST", "/optimize", HTTP.streamhandler(handle_optimize))
HTTP.register!(ROUTER, "POST", "/optimize-progress", HTTP.streamhandler(handle_optimize_progress))
HTTP.register!(ROUTER, "GET", "/optimize-progress/{id}", handle_optimize_progress_stream)


# Start server
const SERVER = HTTP.serve(ROUTER, "127.0.0.1", 3220; stream=true)
end
