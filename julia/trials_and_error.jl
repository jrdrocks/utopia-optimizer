include("src/utopia.jl")
 
module Main 
using ..Utopia
using ..DataStructs
using ..DataStructs.RACE_OPTS
using ..DataStructs.PERS_OPTS
using ..DataStructs.HONOR_OPTS
using ..DataStructs.SELF_SPELLS
using ..GameDefinitions

building_keys = collect(keys(BUILDING_DATA))
building_count = length(building_keys)



for idx in 1:building_count
    println(building_keys[idx])
end

for (building, pair) in enumerate(BUILDING_DATA)
    println("~~~~~~~~~~~~~~~~~")
    println(building)
    
    for effect in pair.second
        println(effect)
    end
end

building_keys_lookup = Dict([v => idx for (idx, v) in enumerate(building_keys)])
println(building_keys_lookup)

x = Dict([
    effect.first => merge(effect.second, (; building = building)) for (building, effects) in BUILDING_DATA for effect in effects
    ])
for (_, x) in x
    println(x)
end

end