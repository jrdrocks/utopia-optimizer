include("src/utopia.jl")

module Main 
using ..Utopia
using ..DataStructs
using NamedArrays, Dates, PrettyTables


races = [RACE_OPTS.AVIAN, RACE_OPTS.DARKELF,  RACE_OPTS.DRYAD, RACE_OPTS.DWARF, RACE_OPTS.ELF, RACE_OPTS.FAERY, RACE_OPTS.GNOME, RACE_OPTS.HALFLING, RACE_OPTS.HUMAN, RACE_OPTS.ORC]
pers = [PERS_OPTS.NONE, PERS_OPTS.ARTISAN, PERS_OPTS.CLERIC, PERS_OPTS.HERETIC, PERS_OPTS.MYSTIC, PERS_OPTS.NECROMANCER, PERS_OPTS.ROGUE, PERS_OPTS.TACTICIAN, PERS_OPTS.WARHERO, PERS_OPTS.WARRIOR]

#pers = [PERS_OPTS.HERETIC]
result_size = zeros(length(races), length(pers))

result_dpa = NamedArray(deepcopy(result_size), (  map(string, races), map(string, pers) ), ("Race", "Pers"))
result_turtle_dpa = NamedArray(deepcopy(result_size), (  map(string, races), map(string, pers) ), ("Race", "Pers"))
result_opa = NamedArray(deepcopy(result_size), (  map(string, races), map(string, pers) ), ("Race", "Pers"))
result_opnw = NamedArray(deepcopy(result_size), (  map(string, races), map(string, pers) ), ("Race", "Pers"))
result_nw = NamedArray(deepcopy(result_size), (  map(string, races), map(string, pers) ), ("Race", "Pers"))

pers_rogue = [PERS_OPTS.ROGUE]


runs = []

for (idx_race, race) in enumerate(races)
    for (idx_pers, pers) in enumerate(pers)              
        
        target = OptimizationTarget(
            RACE =race,
            PERS = pers,
            HONOR = HONOR_OPTS.BARON,
            ACRES = 2000,
            SCIENCE_DAYS = 7 * 6,
            INCOME = 10_000,
            RUNES = 2_000,
            MOD_WPA = 2,
            MOD_TPA = 5,
            #DPA = 100,
            #OPA = -1,
            OPA_DPA_RATIO = 3,
            OPT_FOR_NW = true,
            OPT_FOR_TURTLE_DEF = false, #target turtle def
            OPT_RAW_TPA_AND_WPA = false, #OPT_RAW_TPA_AND_WPA instead of mod
            SELF_SPELLS_EXCLUDE = [
                SELF_SPELLS.MINOR_PROTECTION,
                SELF_SPELLS.GREATER_PROTECTION,
                SELF_SPELLS.FANATICISM
            ],    
            TARGETS = Dict(
                EXTRA_TARGETS.THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER => 0.66, #max x*100 pct of damage
                EXTRA_TARGETS.LANDLOSS_MULTIPLIER => 0.85, #max
                EXTRA_TARGETS.LOSSES_MULTIPLIER => 0.75, #max
                EXTRA_TARGETS.ATTACK_TIME_MULTIPLIER => 0.9, #max
                #EXTRA_TARGETS.COMBAT_GAINS_MULTIPLIER => 1.1, #min
                #EXTRA_TARGETS.THIEF_DAMAGE_MULTIPLIER => 1.2, #min
                #EXTRA_TARGETS.THIEF_LOSSES_MULTIPLIER => 0.1, #min
                #EXTRA_TARGETS.MAGE_DAMAGE_MULTIPLIER => 1.2, #min
                EXTRA_TARGETS.MAX_PRISONERS => 750
            ),
            BUILDING_TARGETS = Dict(
                BUILDING_OPTS.HOMES => (;min = 0, max = 10),
                BUILDING_OPTS.GUILDS => (;eq = 15, as_effective = true)
            ),
            SCIENCE_BASE = Dict(
                SCIENCE_OPTS.SHIELDING => 10_000,
                SCIENCE_OPTS.CRIME => 10_000
            )
        )

        if (race == RACE_OPTS.UNDEAD)            
            delete!(target.TARGETS, EXTRA_TARGETS.LOSSES_MULTIPLIER)
            delete!(target.TARGETS, EXTRA_TARGETS.COMBAT_GAINS_MULTIPLIER)
        end
        
        if (race == RACE_OPTS.HUMAN)
            #target.TARGETS[EXTRA_TARGETS.MAX_ELITE_DSPEC_RATIO] = 1
        end

        #if (race == RACE_OPTS.AVIAN)
        #    target.TARGETS[EXTRA_TARGETS.MAX_ELITE_OSPEC_RATIO] = 1 
        #end        
        
        try
            (frames, values) = optimizeProvince(target)
            (acres, nw, off, def, turtle_def) = values
            result_opa[string(race), string(pers)] = off / acres
            result_opnw[string(race), string(pers)] = 100 * off / nw
            result_dpa[string(race), string(pers)] = def / acres
            result_turtle_dpa[string(race), string(pers)] = turtle_def / acres
            result_nw[string(race), string(pers)] = nw
            push!(runs, ("$race-$pers", frames))      
        catch  e
            println("ERROR: $race-$pers")
            println(e)
            # print stacktrace
            println(stacktrace(catch_backtrace()))
            throw(e)
            return

        end            
        
        #open("races.txt", "a") do io
        #    redirect_stdout(io) do
        #        println(target)
        #        pretty_table(df_core, crop=:none, formatters=ft_printf("%'.d"));
        #    end
        #end
        
    end
end


open("julia/out/opt-any-past-mid-3at.109.txt", "w") do io
    redirect_stdout(io) do
        current_dt = Dates.format(Dates.now(), "yyyy-mm-dd at HH:MM:SS")
        println("Run: $current_dt")
        #println("90% attack time (forcing racks if not avian or gnome) -25% thievery damage (dens), 75% army losses (hosp and science), 80% land losses (gs)")
        println("")


        pretty_table(result_opa, header = map(string, pers), crop=:none, formatters=ft_printf("%d"), row_labels  =  map(string, races), title="OPA");
        pretty_table(result_opnw,  header = map(string, pers), crop=:none, formatters=ft_printf("%d"), row_labels  =  map(string, races), title="OPNW");
        pretty_table(result_dpa,  header = map(string, pers), crop=:none, formatters=ft_printf("%d"), row_labels = map(string, races), title="DPA");
        pretty_table(result_turtle_dpa,  header = map(string, pers), crop=:none, formatters=ft_printf("%d"), row_labels = map(string, races), title="TURTLE DPA");
        pretty_table(result_nw, header = map(string, pers), crop=:none, formatters=ft_printf("%d"), row_labels  =  map(string, races), title="Networth");

        #data = result_opa
        #xlabel = map(string, pers)
        #ylabel = map(string, races)
        #heatmap(data, xticks=(1:length(pers), xlabel), yticks=(1:length(races), ylabel),
        #    fill_z=data, aspect_ratio=:equal)
        #png("hm.png")
#
        #fontsize = 15
        #nrow, ncol = size(data)
        #ann = [(i,j, Plots.text(round(data[i,j], digits=2), fontsize, :white, :center))
        #            for i in 1:nrow for j in 1:ncol]
        #annotate!(ann, linecolor=:white)
        #png("hm-annotate.png")

        println("")
        println("")
        println("")
        for run in runs
            (name, (df_scy, def_scy_groups, df_build, df_pop, df_core)) = run
            println(name)
            println("===================================================")
            pretty_table(df_scy,  crop=:none, formatters=ft_printf("%d"))
            pretty_table(def_scy_groups,  crop=:none, formatters=ft_printf("%d"))
            pretty_table(df_build, crop=:none,  formatters=ft_printf("%.0f"))
            pretty_table(df_pop, crop=:none,  formatters=ft_printf("%d"))
            pretty_table(df_core, crop=:none, formatters=ft_printf("%.d"))
            println("")
            println("")
        end
    end
end
end