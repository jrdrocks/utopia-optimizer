#import Pkg
#Pkg.activate(".")
#Pkg.instantiate()

println("Julia version $(VERSION.major).$(VERSION.minor).$(VERSION.patch)")

include("src/utopia.jl")
 
module Main 
using ..Utopia
using ..DataStructs

using NamedTupleTools

target = OptimizationTarget(
    RACE = RACE_OPTS.HUMAN,
    PERS = PERS_OPTS.WARRIOR,
    HONOR = HONOR_OPTS.KNIGHT,
    ACRES = 1000,
    #SCIENCE = 3_500_000,
    SCIENCE_DAYS = 28,
    INCOME = 6_000,
    RUNES = 4_000,
    MOD_WPA = 10.0,
    MOD_TPA = 4.5,
    #DPA = 120,
    #OPA = -1,
    OPA_DPA_RATIO = 2,
    OPT_FOR_NW = false,
    OPT_FOR_TURTLE_DEF = false, #target turtle def
    OPT_RAW_TPA_AND_WPA = false, #OPT_RAW_TPA_AND_WPA instead of mod
    SELF_SPELLS_EXCLUDE = [
        SELF_SPELLS.MINOR_PROTECTION,
        SELF_SPELLS.GREATER_PROTECTION,
        SELF_SPELLS.MAGES_FURY
    ],    
    TARGETS = Dict(
        #:thief_losses_factor => 0.5, #min
        EXTRA_TARGETS.THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER => 0.66, #max x*100 pct of damage
        EXTRA_TARGETS.LANDLOSS_MULTIPLIER => 0.85, #max
        #EXTRA_TARGETS.LOSSES_MULTIPLIER => 0.7, #max
        EXTRA_TARGETS.ATTACK_TIME_MULTIPLIER => 0.9, #max
        #EXTRA_TARGETS.COMBAT_GAINS_MULTIPLIER => 1.2, #min
        #EXTRA_TARGETS.THIEF_DAMAGE_MULTIPLIER => 1.4, #min
        #EXTRA_TARGETS.MAGE_DAMAGE_MULTIPLIER => 1.3, #min
        EXTRA_TARGETS.MAX_PRISONERS => 1000
    ),
    BUILDING_TARGETS = Dict(
        BUILDING_OPTS.HOMES => (;min = 10, max = 15),
        BUILDING_OPTS.GUILDS => (;min = 15, as_effective = false),
        BUILDING_OPTS.TRAINING_GROUNDS => (;min = 0, max = 15),
        BUILDING_OPTS.LIBRARIES => (;min = 0, max = 15),
    ),
    SCIENCE_BASE = Dict(
        SCIENCE_OPTS.SHIELDING => 10_000,
        SCIENCE_OPTS.CRIME => 10_000
    )
)
 
 

(frames, values, definitions) = Utopia.optimizeProvince(target)
 
using JSON3
using StructTypes



#StructTypes.StructType(::Type{OptimizationTarget}) = StructTypes.Struct()

#println(JSON3.pretty(frames))

using JlrsCore.Reflect

struct RustyOptimizationTarget
    RACE::RACE_OPTS.RACE
    PERS::PERS_OPTS.PERS
    HONOR::HONOR_OPTS.HONOR
    ACRES::Number
    SCIENCE::Union{Number, Nothing}   
    SCIENCE_DAYS::Union{Number, Nothing}
    SCIENCE_STATE::Union{ScienceState, Nothing}
    SELF_SPELLS_EXCLUDE::AbstractArray{SELF_SPELLS.SPELL}
    INCOME::Number
    RUNES::Number
    MOD_WPA::Number
    MOD_TPA::Number
    DPA::Union{Number, Nothing}
    OPA::Union{Number, Nothing}
    OPA_DPA_RATIO::Union{Number, Nothing}
    OPT_FOR_NW::Bool
    OPT_FOR_TURTLE_DEF::Bool
    OPT_RAW_TPA_AND_WPA::Bool
    BUILDING_TARGETS::Dict{BUILDING_OPTS.BUILDING, Dict{Symbol, Number}}
    TARGETS::Dict{EXTRA_TARGETS.EXTRA_TARGET,Number}
    SCIENCE_BASE::Dict{SCIENCE_OPTS.SCIENCE,Number}
end

a::NamedTuple =  (;min = 0, max = 15)

println(typeof(a))

layouts = reflect([RustyOptimizationTarget]);

# Print layouts to standard output
#println(layouts)


end