include("src/utopia.jl")

module Main
using ..Utopia
using ..DataStructs

using Genie, Genie.Renderer.Json, Genie.Requests
using JSON3, StructTypes, Setfield
using  Genie.Responses.streamresponse

import UUIDs: uuid4

struct Form
    RACE_selected::Vector{RACE_OPTS.RACE}
    PERS_selected::Vector{PERS_OPTS.PERS}
end


StructTypes.StructType(::Type{Form}) = StructTypes.Struct()

StructTypes.StructType(::Type{OptimizationTarget}) = StructTypes.Struct()

StructTypes.StructType(::Type{ExtraOptimizationTarget}) = StructTypes.Struct()

struct Overrides
    races::Union{Vector{RACE_OPTS.RACE},Nothing}
    personalities::Union{Vector{PERS_OPTS.PERS},Nothing}
    override::ExtraOptimizationTarget
end

StructTypes.StructType(::Type{Overrides}) = StructTypes.Struct()


Genie.config.run_as_server = true
Genie.config.cors_headers["Access-Control-Allow-Origin"] = "http://localhost:5173"
# This has to be this way - you should not include ".../*"
Genie.config.cors_headers["Access-Control-Allow-Headers"] = "Content-Type"
Genie.config.cors_headers["Access-Control-Allow-Methods"] = "GET,POST,PUT,DELETE,OPTIONS"
Genie.config.cors_allowed_origins = ["*"]

route("/variables") do
    json(Dict(
        :RACES => string.(instances(DataStructs.RACE_OPTS.RACE)),
        :PERSONALITIES => string.(instances(DataStructs.PERS_OPTS.PERS)),
        :HONORS => string.(instances(DataStructs.HONOR_OPTS.HONOR)),
        :BUILDINGS => string.(instances(DataStructs.BUILDING_OPTS.BUILDING)),
        :SCIENCES => string.(instances(DataStructs.SCIENCE_OPTS.SCIENCE)),
        :SCIENCE_GROUPS => string.(instances(DataStructs.SCIENCE_GROUP_OPTS.SCIENCE_GROUP)),
        :SELF_SPELLS => string.(instances(DataStructs.SELF_SPELLS.SPELL)),
        :EXTRA_TARGETS => string.(instances(DataStructs.EXTRA_TARGETS.EXTRA_TARGET))
    ))
end

function merge_optimization_targets(base::OptimizationTarget, extra::ExtraOptimizationTarget)
    # Create a new OptimizationTarget with the same values as base
    result = deepcopy(base)

    # Iterate through all fields of ExtraOptimizationTarget
    for field in fieldnames(typeof(extra))
        extra_value = getfield(extra, field)

        # Skip if the extra value is nothing
        if extra_value === nothing
            continue
        end

        base_value = getfield(base, field)

        # Handle different types of fields
        if extra_value isa Dict
            # For dictionaries, merge/replace keys and handle nothing values
            merged_dict = deepcopy(base_value)
            for (k, v) in extra_value
                if v === nothing
                    # Remove the key if value is nothing
                    delete!(merged_dict, k)
                else
                    # Otherwise, set or update the value
                    merged_dict[k] = v
                end
            end
            setfield!(result, field, merged_dict)
        elseif extra_value isa AbstractArray
            # For arrays, append new values
            merged_array = vcat(base_value, extra_value)
            setfield!(result, field, merged_array)
        else
            # For other types, simply replace the value
            setfield!(result, field, extra_value)
        end
    end

    return result
end


route("/optimize", method=POST) do
    @show rawpayload()
    # there must be an easier way
    form = JSON3.read(JSON3.write(JSON3.read(rawpayload()).form), Form)
    @show form
    target = JSON3.read(rawpayload(), OptimizationTarget)

    overrides = nothing

    try

        overrides = JSON3.read(JSON3.write(JSON3.read(rawpayload()).OVERRIDES), Vector{Overrides})
        @show overrides
    catch e
        overrides = []
    end

    result = []

    for race in form.RACE_selected
        for pers in form.PERS_selected
            current_target = deepcopy(target)
            current_target.RACE = race
            current_target.PERS = pers

            if overrides !== nothing
                for override in overrides
                    if override.races !== nothing || override.personalities !== nothing
                        #println("race: $(race), type: $(typeof(race))")
                        #println("override.races: $(override.races), type: $(typeof(override.races))")
                        #println("override.personalities: $(override.personalities), type: $(typeof(override.personalities))")
                        if (isnothing(override.races) || isempty(override.races) || race in override.races) &&
                           (isnothing(override.personalities) || isempty(override.personalities) || pers in override.personalities)
                            println("Override race {} and pers {}", race, pers)
                            current_target = merge_optimization_targets(current_target, override.override)
                            #println(JSON3.write(current_target))
                        end
                    end
                end
            end

            (frames, values, definitions) = Utopia.optimizeProvince(current_target, true)
            if (isnan(values[2]))
                push!(result, (race, pers, "infeasible"))
            else
                push!(result, (race, pers, Dict(
                    :frames => frames,
                    :values => values,
                    :definitions => definitions,
                    :target => current_target
                )))
            end
        end
    end

    json(result)
end

# Global state storage for optimization progress
const optimization_states = Dict{String,Any}()
const state_lock = ReentrantLock()

route("/optimize-progress", method=POST) do
    form = JSON3.read(JSON3.write(JSON3.read(rawpayload()).form), Form)
    target = JSON3.read(rawpayload(), OptimizationTarget)

    overrides = nothing
    try
        overrides = JSON3.read(JSON3.write(JSON3.read(rawpayload()).OVERRIDES), Vector{Overrides})
    catch e
        overrides = []
    end

    # Generate unique ID
    id = string(uuid4())

    # Store initial state
    lock(state_lock) do
        optimization_states[id] = Dict(
            :form => form,
            :target => target,
            :overrides => overrides,
            :total => length(form.RACE_selected) * length(form.PERS_selected),
            :completed => 0
        )
    end

    json(Dict(:id => id))
end

route("/optimize-progress/:id", method=GET) do
    id = params(:id)
    
    # Set SSE headers
    headers = [
        "Content-Type" => "text/event-stream",
        "Cache-Control" => "no-cache",
        "Connection" => "keep-alive"
    ]

    # Get state
    state = lock(state_lock) do
        get(optimization_states, id, nothing)
    end

    if state === nothing
        return json(Dict(:error => "Invalid ID"), status=404)
    end

    # Create channel for async communication
    channel = Channel{Any}(32)

    # Create the streaming response
    return Genie.Router.streamresponse(headers) do io
        @async begin
            try
                form = state[:form]
                target = state[:target]
                overrides = state[:overrides]
                total = state[:total]
                completed = 0

                for race in form.RACE_selected
                    for pers in form.PERS_selected
                        current_target = deepcopy(target)
                        current_target.RACE = race
                        current_target.PERS = pers

                        if !isnothing(overrides)
                            for override in overrides
                                if !isnothing(override.races) || !isnothing(override.personalities)
                                    if (isnothing(override.races) || isempty(override.races) || race in override.races) &&
                                       (isnothing(override.personalities) || isempty(override.personalities) || pers in override.personalities)
                                        current_target = merge_optimization_targets(current_target, override.override)
                                    end
                                end
                            end
                        end

                        # Send progress update
                        progress_data = Dict(:progress => [race, pers, completed / total])
                        write(io, "data: $(JSON3.write(progress_data))\n\n")
                        flush(io)

                        (frames, values, definitions) = Utopia.optimizeProvince(current_target, true)
                        result = if isnan(values[2])
                            (race, pers, "infeasible")
                        else
                            (race, pers, Dict(
                                :frames => frames,
                                :values => values,
                                :definitions => definitions,
                                :target => current_target
                            ))
                        end

                        completed += 1
                        
                        # Update progress
                        progress_data = Dict(:progress => [race, pers, completed / total])
                        write(io, "data: $(JSON3.write(progress_data))\n\n")
                        flush(io)

                        # Update state
                        lock(state_lock) do
                            optimization_states[id][:completed] = completed
                        end
                    end
                end

                # Send final result
                write(io, "data: $(JSON3.write(Dict(:done => result)))\n\n")
                flush(io)

                # Clean up state
                lock(state_lock) do
                    delete!(optimization_states, id)
                end
            catch e
                # Handle errors
                write(io, "data: $(JSON3.write(Dict(:error => string(e))))\n\n")
                flush(io)
                @error "Error in optimization process" exception=(e, catch_backtrace())
            end
        end
    end
end


route("/test-override", method=POST) do
    try
        @show rawpayload()

        overrides = JSON3.read(rawpayload(), Vector{Overrides})
        @show overrides



        json(overrides)
    catch e
        error = Dict(
            :error => true,
            :message => sprint(showerror, e)
        )
        json(error)
    end
end
up(3220, async=false)

end
