"""
Military-related calculations for the Utopia optimizer.
"""

using JuMP
using Logging

# Import required modules
using ..DataStructs
using ..DataStructs.SCIENCE_OPTS
using ..DataStructs.BUILDING_OPTS

# Population indices as constants
const P_SOLDIERS = 0
const P_OSPECS = 1
const P_DSPECS = 2
const P_ELITES_OFF = 3
const P_ELITES_DEF = 4
const P_WIZARDS = 5
const P_THIEVES = 6

"""
    validate_military_inputs(model::JuMP.Model, vars, effects)

Validate inputs for military calculations.
"""
function validate_military_inputs(model::JuMP.Model, vars, effects)
    if !JuMP.is_valid(model)
        throw(ArgumentError("Invalid JuMP model"))
    end
    
    buildings, population, books, wages = vars
    if any(isnothing.([buildings, population, books, wages]))
        throw(ArgumentError("Missing required variables"))
    end
    
    if length(population) != 7
        throw(ArgumentError("Population vector must have 7 elements"))
    end
    
    building_effects, science_effects = effects[1:2]
    required_effects = [:ome_increase, :dme_increase, :prisoner_holding, :horse_holding]
    for effect in required_effects
        if !haskey(building_effects, effect)
            throw(ArgumentError("Missing required building effect: $effect"))
        end
    end
    
    required_science = [SCIENCE_OPTS.TACTICS, SCIENCE_OPTS.STRATEGY]
    for science in required_science
        if !haskey(science_effects, science)
            throw(ArgumentError("Missing required science effect: $science"))
        end
    end
end

"""
    calculate_base_military_efficiency(model::JuMP.Model, wages)

Calculate base military efficiency from wages.
"""
function calculate_base_military_efficiency(model::JuMP.Model, wages)
    return @NLexpression(model, 33 + 67 * (wages / 100)^0.25)
end

"""
    calculate_military_efficiencies(model::JuMP.Model, base_me, building_effects, science_effects, definitions, SELECTED_HONOR)

Calculate offensive and defensive military efficiency.
"""
function calculate_military_efficiencies(model::JuMP.Model, base_me, building_effects, science_effects, definitions, SELECTED_HONOR)
    ome = @NLexpression(model, 
        (base_me / 100 + building_effects[:ome_increase]) * 
        (1 + science_effects[SCIENCE_OPTS.TACTICS]) * 
        definitions.OME_MODIFIER * 
        (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1)
    )
    
    dme = @NLexpression(model, 
        (base_me / 100 + building_effects[:dme_increase]) * 
        (1 + science_effects[SCIENCE_OPTS.STRATEGY]) * 
        definitions.DME_MODIFIER * 
        (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1)
    )

    return ome, dme
end

"""
    calculate_prisoners_and_horses(model::JuMP.Model, population, building_effects, definitions)

Calculate number of prisoners and horses available.
"""
function calculate_prisoners_and_horses(model::JuMP.Model, population, building_effects, definitions)
    prisoners = if definitions.PRISONER_PER_ACRE > 0
        @NLexpression(model, (population[P_OSPECS] + population[P_ELITES_OFF]) / 5)
    else
        building_effects[:prisoner_holding]
    end

    horses = if definitions.HORSE_PER_ACRE > 0
        @NLexpression(model, population[P_OSPECS] + population[P_ELITES_OFF])
    else
        building_effects[:horse_holding]
    end

    return prisoners, horses
end

"""
    calculate_offense(model::JuMP.Model, population, prisoners, horses, ome, definitions)

Calculate offensive power.
"""
function calculate_offense(model::JuMP.Model, population, prisoners, horses, ome, definitions)
    return @NLexpression(model, 
        (population[P_OSPECS] * definitions.OSPEC_OFF + 
         population[P_ELITES_OFF] * definitions.ELITE_OFF +
         prisoners * definitions.PRISONER_OFF + 
         horses * definitions.HORSE_OFF) * ome
    )
end

"""
    calculate_defense(model::JuMP.Model, population, dme, definitions)

Calculate defensive power.
"""
function calculate_defense(model::JuMP.Model, population, dme, definitions)
    return @NLexpression(model,
        (population[P_DSPECS] * definitions.DSPEC_DEF + 
         population[P_ELITES_DEF] * definitions.ELITE_DEF) * dme
    )
end

"""
    calculate_turtle_defense(model::JuMP.Model, population, dme, definitions)

Calculate turtle defense power (including offensive elites).
"""
function calculate_turtle_defense(model::JuMP.Model, population, dme, definitions)
    return @NLexpression(model,
        (population[P_DSPECS] * definitions.DSPEC_DEF + 
         (population[P_ELITES_OFF] + population[P_ELITES_DEF]) * 
         definitions.ELITE_DEF) * dme
    )
end

"""
    calculate_military_metrics(model::JuMP.Model, target::OptimizationTarget, vars, effects)

Calculate offense, defense, and turtle defense metrics.
Returns a tuple of (offense, defense, turtle_defense).
"""
function calculate_military_metrics(model::JuMP.Model, target::OptimizationTarget, vars, effects)
    @info "Calculating military metrics" _module=:UtopiaOptimizer _group=:military
    
    try
        validate_military_inputs(model, vars, effects)
        
        buildings, population, books, wages = vars
        building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
        definitions, SELECTED_HONOR, merged_building_data = effects

        # Calculate base military efficiency and modifiers
        base_me = calculate_base_military_efficiency(model, wages)
        ome, dme = calculate_military_efficiencies(model, base_me, building_effects, science_effects, definitions, SELECTED_HONOR)

        # Calculate prisoners and horses
        prisoners, horses = calculate_prisoners_and_horses(model, population, building_effects, definitions)

        # Calculate military metrics
        offense = calculate_offense(model, population, prisoners, horses, ome, definitions)
        defense = calculate_defense(model, population, dme, definitions)
        turtle_defense = calculate_turtle_defense(model, population, dme, definitions)

        @info "Military metrics calculated successfully" _module=:UtopiaOptimizer _group=:military
        return (offense, defense, turtle_defense)
    catch e
        @error "Error calculating military metrics" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:military
        rethrow(e)
    end
end

"""
    calculate_networth_components(model::JuMP.Model, target::OptimizationTarget, vars, effects, peasants)

Calculate individual networth components.
"""
function calculate_networth_components(model::JuMP.Model, target::OptimizationTarget, vars, effects, peasants)
    buildings, population, books, wages = vars
    building_effects = effects[1]
    definitions = effects[5]

    land_value = target.ACRES * 60
    peasant_value = @NLexpression(model, peasants * 0.25)
    ospec_value = @NLexpression(model, population[P_OSPECS] * definitions.OSPEC_OFF * 0.4)
    dspec_value = @NLexpression(model, population[P_DSPECS] * definitions.DSPEC_DEF * 0.5)
    elite_value = @NLexpression(model, 
        (population[P_ELITES_OFF] + population[P_ELITES_DEF]) * definitions.ELITE_NW)
    horse_value = @NLexpression(model, 
        calculate_horses(model, target, vars, effects) * definitions.HORSE_NW_BASE)
    prisoner_value = @NLexpression(model, 
        building_effects[:prisoner_holding] * definitions.PRISONER_OFF * 0.2)
    specialist_value = @NLexpression(model, 
        population[P_THIEVES] * 5 + population[P_WIZARDS] * 7)
    science_value = target.ACRES * 0.000007 * target.science

    return (
        land_value = land_value,
        peasant_value = peasant_value,
        ospec_value = ospec_value,
        dspec_value = dspec_value,
        elite_value = elite_value,
        horse_value = horse_value,
        prisoner_value = prisoner_value,
        specialist_value = specialist_value,
        science_value = science_value
    )
end

"""
    calculate_networth(model::JuMP.Model, target::OptimizationTarget, vars, effects)

Calculate the total networth of the province.
"""
function calculate_networth(model::JuMP.Model, target::OptimizationTarget, vars, effects)
    @info "Calculating networth" _module=:UtopiaOptimizer _group=:military
    
    try
        validate_military_inputs(model, vars, effects)
        
        # Calculate peasants
        pop_space = calculate_population_space(model, target, vars, effects)
        peasants = @NLexpression(model, pop_space - sum(x for x in population))

        # Calculate networth components
        components = calculate_networth_components(model, target, vars, effects, peasants)

        networth = @NLexpression(model, 
            components.land_value + components.peasant_value + 
            components.ospec_value + components.dspec_value + 
            components.elite_value + components.horse_value + 
            components.prisoner_value + components.specialist_value + 
            components.science_value
        )

        @info "Networth calculated successfully" _module=:UtopiaOptimizer _group=:military
        return networth
    catch e
        @error "Error calculating networth" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:military
        rethrow(e)
    end
end

"""
    calculate_population_space(model::JuMP.Model, target::OptimizationTarget, vars, effects)

Calculate total population space available.
"""
function calculate_population_space(model::JuMP.Model, target::OptimizationTarget, vars, effects)
    @info "Calculating population space" _module=:UtopiaOptimizer _group=:military
    
    try
        validate_military_inputs(model, vars, effects)
        
        buildings, population, books, wages = vars
        building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
        definitions, SELECTED_HONOR, merged_building_data = effects

        pop_space = @NLexpression(model, 
            ((1 + science_effects[SCIENCE_OPTS.HOUSING]) * 
             definitions.POP_MULTIPLIER * 
             (((SELECTED_HONOR.pop - 1) * definitions.HONOR_EFFECTS) + 1) * 
             ((buildings[building_keys_lookup[BUILDING_OPTS.HOMES]] * target.ACRES / 100 * 
               (10 + definitions.HOME_POP_INCREASE) * definitions.HOME_POP_MULTIPLIER) + 
              (sum(x for x in buildings) * target.ACRES / 100 * 25)))
        )

        @info "Population space calculated successfully" _module=:UtopiaOptimizer _group=:military
        return pop_space
    catch e
        @error "Error calculating population space" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:military
        rethrow(e)
    end
end

"""
    calculate_horses(model::JuMP.Model, target::OptimizationTarget, vars, effects)

Calculate number of horses available.
"""
function calculate_horses(model::JuMP.Model, target::OptimizationTarget, vars, effects)
    @info "Calculating horses" _module=:UtopiaOptimizer _group=:military
    
    try
        validate_military_inputs(model, vars, effects)
        
        buildings, population, books, wages = vars
        building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
        definitions, SELECTED_HONOR, merged_building_data = effects

        horses = if definitions.HORSE_PER_ACRE > 0
            @NLexpression(model, population[P_OSPECS] + population[P_ELITES_OFF])
        else
            building_effects[:horse_holding]
        end

        @info "Horses calculated successfully" _module=:UtopiaOptimizer _group=:military
        return horses
    catch e
        @error "Error calculating horses" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:military
        rethrow(e)
    end
end
