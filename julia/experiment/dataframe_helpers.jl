"""
DataFrame creation helpers for the Utopia optimizer.
"""

using Logging
using DataFrames
using PrettyTables
using JuMP
using CSV
using JSON

# Import required modules
using ..DataStructs
using ..DataStructs.SCIENCE_OPTS
using ..DataStructs.SCIENCE_GROUP_OPTS
using ..DataStructs.BUILDING_OPTS

# Import helper functions
using ..UtopiaOptimizer: calculate_population_space, calculate_horses, calculate_military_metrics, calculate_networth

"""
    validate_model_vars(model::JuMP.Model, vars, effects)

Validate model variables and effects before creating DataFrames.
"""
function validate_model_vars(model::JuMP.Model, vars, effects)
    if !JuMP.is_valid(model)
        throw(ArgumentError("Invalid JuMP model"))
    end
    
    if !JuMP.has_values(model)
        throw(ArgumentError("Model has no values - must be solved first"))
    end
    
    buildings, population, books, wages = vars
    if any(isnothing.([buildings, population, books, wages]))
        throw(ArgumentError("Missing required variables"))
    end
    
    building_effects, science_effects = effects[1:2]
    if isempty(building_effects) || isempty(science_effects)
        throw(ArgumentError("Missing required effects"))
    end
end

"""
    safe_value(expr::JuMP.NonlinearExpression)

Safely get the value of a JuMP expression, returning 0.0 if it fails.
"""
function safe_value(expr::JuMP.NonlinearExpression)
    try
        return JuMP.value(expr)
    catch e
        @warn "Failed to get value" expression=expr exception=e
        return 0.0
    end
end

"""
    calculate_income_expenses(model::JuMP.Model, target::OptimizationTarget, vars, effects)

Calculate income and expenses expressions.
"""
function calculate_income_expenses(model::JuMP.Model, target::OptimizationTarget, vars, effects)
    buildings, population, books, wages = vars
    building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
    definitions, SELECTED_HONOR, merged_building_data = effects

    # Population indices
    p_ospecs = 1
    p_dspecs = 2
    p_elites_off = 3
    p_elites_def = 4

    # Calculate peasants
    pop_space = calculate_population_space(model, target, vars, effects)
    peasants = @NLexpression(model, pop_space - sum(x for x in population))

    # Calculate income
    income = @NLexpression(model, 
        ((3 + definitions.MINERS_MYSTIQUE_EFFECT) * peasants + 
         building_effects[:gc_production]) * 
        (1 + building_effects[:income_increase]) * 
        (1 + science_effects[SCIENCE_OPTS.ALCHEMY]) * 
        definitions.INCOME_MULTIPLIER * 
        (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1)
    )

    # Calculate expenses
    expenses = @NLexpression(model,
        ((population[p_ospecs] + population[p_dspecs]) * 0.5 + 
         (population[p_elites_off] + population[p_elites_def]) * 0.75) *
        (1 - building_effects[:wages_decrease]) * 
        (1 - science_effects[SCIENCE_OPTS.BOOKKEEPING]) *
        (wages / 100) * definitions.WAGES_MULTIPLIER
    )

    return income, expenses
end

"""
    create_science_dataframe(model::JuMP.Model, books, science_effects, books_keys_lookup)

Create a DataFrame summarizing science investments and effects.
"""
function create_science_dataframe(model::JuMP.Model, books, science_effects, books_keys_lookup)
    @info "Creating science DataFrame" _module=:UtopiaOptimizer _group=:results
    
    try
        validate_model_vars(model, (books, nothing, nothing, nothing), (Dict(), science_effects))
        
        df = DataFrame(
            Science_Effect = SCIENCE_OPTS.SCIENCE[],
            Books = Number[],
            Effect = Number[],
            Group = SCIENCE_GROUP_OPTS.SCIENCE_GROUP[]
        )

        for (k, v) in science_effects
            book_count = safe_value(books[books_keys_lookup[k]])
            effect = safe_value(v)
            if book_count > 10
                push!(df, (k, book_count, effect * 100, SCIENCE_DATA.cats[k].group))
            end
        end

        sort!(df, [:Group, :Science_Effect])
        @info "Science DataFrame created successfully" rows=size(df, 1) _module=:UtopiaOptimizer _group=:results
        return df
    catch e
        @error "Error creating science DataFrame" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end

"""
    create_buildings_dataframe(model::JuMP.Model, buildings, building_effects, building_keys_lookup)

Create a DataFrame summarizing building allocations and effects.
"""
function create_buildings_dataframe(model::JuMP.Model, buildings, building_effects, building_keys_lookup)
    @info "Creating buildings DataFrame" _module=:UtopiaOptimizer _group=:results
    
    try
        validate_model_vars(model, (buildings, nothing, nothing, nothing), (building_effects, Dict()))
        
        df = DataFrame(
            Building_Effect = Symbol[],
            Building_Name = BUILDING_OPTS.BUILDING[],
            Building_Pct = Number[],
            Effect = Number[]
        )

        for (k, v) in building_effects
            building_keys = building_effects_by_effect[k][:buildings]
            for building_key in building_keys
                building = buildings[building_keys_lookup[building_key]]
                effect = safe_value(v)
                if effect > 0.01
                    push!(df, (k, building_key, safe_value(building), effect > 2 ? effect : effect * 100))
                end
            end
        end

        sort!(df, [:Building_Name, :Building_Effect])
        @info "Buildings DataFrame created successfully" rows=size(df, 1) _module=:UtopiaOptimizer _group=:results
        return df
    catch e
        @error "Error creating buildings DataFrame" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end

"""
    create_population_dataframe(model::JuMP.Model, population, target, vars, effects)

Create a DataFrame summarizing population distribution.
"""
function create_population_dataframe(model::JuMP.Model, population, target, vars, effects)
    @info "Creating population DataFrame" _module=:UtopiaOptimizer _group=:results
    
    try
        validate_model_vars(model, vars, effects)
        
        population_names = ["soldiers", "ospec", "dspec", "elites_off", "elites_def", "wizards", "thieves"]
        
        df = DataFrame(
            Troop = String[],
            Amount = Number[]
        )

        # Add peasants
        pop_space = calculate_population_space(model, target, vars, effects)
        peasants = safe_value(pop_space - sum(x for x in population))
        push!(df, ("peasants", peasants))

        # Add military units
        for (i, v) in enumerate(population)
            push!(df, (population_names[i], safe_value(v)))
        end

        # Add horses and prisoners if applicable
        horses = calculate_horses(model, target, vars, effects)
        push!(df, ("horses", safe_value(horses)))

        if target.definitions.PRISONER_PER_ACRE > 0
            prisoners = (population[1] + population[3]) / 5  # ospecs + elites_off
            push!(df, ("prisoners", safe_value(prisoners)))
        end

        @info "Population DataFrame created successfully" rows=size(df, 1) _module=:UtopiaOptimizer _group=:results
        return df
    catch e
        @error "Error creating population DataFrame" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end

"""
    create_core_metrics_dataframe(model::JuMP.Model, target::OptimizationTarget, vars, effects)

Create a DataFrame summarizing core province metrics.
"""
function create_core_metrics_dataframe(model::JuMP.Model, target::OptimizationTarget, vars, effects)
    @info "Creating core metrics DataFrame" _module=:UtopiaOptimizer _group=:results
    
    try
        validate_model_vars(model, vars, effects)
        
        buildings, population, books, wages = vars
        building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
        definitions, SELECTED_HONOR, merged_building_data = effects

        # Calculate military metrics
        offense, defense, turtle_defense = calculate_military_metrics(model, target, vars, effects)
        nw = calculate_networth(model, target, vars, effects)
        base_me = @NLexpression(model, 33 + 67 * (wages / 100)^0.25)

        # Calculate military efficiencies
        ome = @NLexpression(model, 
            (base_me / 100 + building_effects[:ome_increase]) * 
            (1 + science_effects[SCIENCE_OPTS.TACTICS]) * 
            definitions.OME_MODIFIER * 
            (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1)
        )
        
        dme = @NLexpression(model, 
            (base_me / 100 + building_effects[:dme_increase]) * 
            (1 + science_effects[SCIENCE_OPTS.STRATEGY]) * 
            definitions.DME_MODIFIER * 
            (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1)
        )

        # Calculate TPA/WPA
        p_thieves = 6
        p_wizards = 5
        tpa_raw = safe_value(population[p_thieves]) / target.ACRES
        wpa_raw = safe_value(population[p_wizards]) / target.ACRES
        
        tpa = tpa_raw * (1 + safe_value(building_effects[:tpa_increase])) * 
              (1 + safe_value(science_effects[SCIENCE_OPTS.CRIME])) * 
              definitions.TPA_MODIFIER * 
              (((SELECTED_HONOR.tpa - 1) * definitions.HONOR_EFFECTS) + 1)
              
        wpa = wpa_raw * (1 + safe_value(science_effects[SCIENCE_OPTS.CHANNELING])) * 
              definitions.WPA_MODIFIER * 
              (((SELECTED_HONOR.wpa - 1) * definitions.HONOR_EFFECTS) + 1)

        # Calculate income and expenses
        income, expenses = calculate_income_expenses(model, target, vars, effects)

        # Calculate other metrics
        pop_space = calculate_population_space(model, target, vars, effects)
        jobs = @NLexpression(model, 
            sum(x for x in buildings) * target.ACRES / 100 * 25 - 
            buildings[building_keys_lookup[BUILDING_OPTS.HOMES]] * target.ACRES / 100 * 25
        )
        peasants = @NLexpression(model, pop_space - sum(x for x in population))
        pct_jobs_performed = @NLexpression(model, 
            (3 * peasants + building_effects[:prisoner_holding] / 2) / 
            (2 * (jobs * definitions.GHOST_WORKER_EFFECT))
        )

        be = @NLexpression(model, 
            (0.5 * (1.0 + pct_jobs_performed)) * 
            (1 + science_effects[SCIENCE_OPTS.TOOLS]) * 
            definitions.BE_MULTIPLIER
        )

        attack_time_factor = @NLexpression(model, 
            definitions.ATTACK_TIME_MODIFIER * 
            (1 - building_effects[:attack_time_decrease])
        )

        thief_losses_factor = @NLexpression(model, 
            definitions.THIEF_LOSSES_FACTOR * 
            (1 - building_effects[:thief_losses_decrease]) * 
            (1 - science_effects[SCIENCE_OPTS.FINESSE])
        )

        thief_damage_multiplier = @NLexpression(model, 
            definitions.THIEF_DAMAGE_MULTIPLIER * 
            (1 + science_effects[SCIENCE_OPTS.CUNNING])
        )

        df = DataFrame(Core = String[], Amount = Number[])
        
        metrics = [
            ("NW", safe_value(nw)),
            ("OPNW", 100 * safe_value(offense) / safe_value(nw)),
            ("DPNW", 100 * safe_value(defense) / safe_value(nw)),
            ("ACRES", target.ACRES),
            ("OPA", safe_value(offense) / target.ACRES),
            ("DPA", safe_value(defense) / target.ACRES),
            ("TURTLE DPA", safe_value(turtle_defense) / target.ACRES),
            ("TPA (raw)", tpa_raw),
            ("WPA (raw)", wpa_raw),
            ("TPA (mod)", tpa),
            ("WPA (mod)", wpa),
            ("OME", 100 * safe_value(ome)),
            ("DME", 100 * safe_value(dme)),
            ("Total Books", target.science),
            ("Income", safe_value(income)),
            ("Wage Rate", safe_value(wages)),
            ("Expenses", safe_value(expenses)),
            ("BE", 100 * safe_value(be)),
            ("AttackTime", 100 * safe_value(attack_time_factor)),
            ("Thief Losses", 100 * safe_value(thief_losses_factor)),
            ("Thief Damage Multiplier", 100 * safe_value(thief_damage_multiplier))
        ]

        for (name, value) in metrics
            push!(df, (name, value))
        end

        @info "Core metrics DataFrame created successfully" rows=size(df, 1) _module=:UtopiaOptimizer _group=:results
        return df
    catch e
        @error "Error creating core metrics DataFrame" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end

"""
    format_dataframe(df::DataFrame, formatters::Dict{Symbol,Function})

Apply formatters to specific columns in a DataFrame.
"""
function format_dataframe(df::DataFrame, formatters::Dict{Symbol,Function})
    try
        for (col, formatter) in formatters
            if hasproperty(df, col)
                df[!, col] = formatter.(df[!, col])
            end
        end
        return df
    catch e
        @error "Error formatting DataFrame" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end

"""
    print_dataframe(df::DataFrame, title::String, config::AbstractOptimizerConfig)

Print a DataFrame with a title if not in silent mode.
"""
function print_dataframe(df::DataFrame, title::String, config::AbstractOptimizerConfig)
    if !config.silent
        println("\n", title, ":")
        pretty_table(df, crop=:none)
    end
end

"""
    save_dataframe(df::DataFrame, path::String, format::Symbol=:csv)

Save a DataFrame to a file in the specified format.
"""
function save_dataframe(df::DataFrame, path::String, format::Symbol=:csv)
    try
        if format == :csv
            CSV.write(path, df)
        elseif format == :json
            open(path, "w") do io
                JSON.print(io, df, 4)  # 4 spaces indentation
            end
        else
            throw(ArgumentError("Unsupported format: $format"))
        end
        @info "DataFrame saved successfully" path=path format=format _module=:UtopiaOptimizer _group=:results
    catch e
        @error "Error saving DataFrame" exception=(e, catch_backtrace()) path=path format=format _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end

"""
    merge_dataframes(dfs::Vector{DataFrame}, on::Symbol)

Merge multiple DataFrames on a common column.
"""
function merge_dataframes(dfs::Vector{DataFrame}, on::Symbol)
    try
        result = dfs[1]
        for df in dfs[2:end]
            result = leftjoin(result, df, on=on)
        end
        return result
    catch e
        @error "Error merging DataFrames" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:results
        rethrow(e)
    end
end
