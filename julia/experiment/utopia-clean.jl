"""
    UtopiaOptimizer

A module for optimizing province configurations in Utopia game.
"""
module UtopiaOptimizer

import JuMP
import DataFrames
import PrettyTables
import NamedArrays
import Cbc
import Ipopt
import Juniper
import Alpine
import Logging

using ..DataStructs
using ..DataStructs.RACE_OPTS
using ..DataStructs.PERS_OPTS
using ..DataStructs.HONOR_OPTS
using ..DataStructs.SELF_SPELLS
using ..GameDefinitions
using ..Science

# Configuration types
abstract type AbstractOptimizerConfig end

"""
    StandardOptimizerConfig

Standard configuration for province optimization.
"""
struct StandardOptimizerConfig <: AbstractOptimizerConfig
    silent::Bool
    opt_for_turtle_def::Bool
    opt_for_nw::Bool
    opt_raw_tpa_and_wpa::Bool
    time_limit::Int  # Added time limit parameter
end

# Default constructor with reasonable defaults
StandardOptimizerConfig(;
    silent=false,
    opt_for_turtle_def=false,
    opt_for_nw=false,
    opt_raw_tpa_and_wpa=false,
    time_limit=60  # Default 60 second time limit
) = StandardOptimizerConfig(silent, opt_for_turtle_def, opt_for_nw, opt_raw_tpa_and_wpa, time_limit)

"""
    OptimizationTarget

Structure holding target values for optimization.

# Fields
- `acres::Int`: Number of acres in the province
- `income::Float64`: Target income
- `runes::Float64`: Target runes production
- `science::Union{Nothing,Float64}`: Total science books (or nothing if using science_state)
- `science_state::Any`: Current science state (used if science is nothing)
- `science_days::Int`: Number of days for science calculation
- `honor::Symbol`: Honor selection
- `opa::Union{Nothing,Float64}`: Offense per acre target
- `dpa::Union{Nothing,Float64}`: Defense per acre target
- `opa_dpa_ratio::Union{Nothing,Float64}`: OPA/DPA ratio target
- `mod_tpa::Float64`: Modified thieves per acre target
- `mod_wpa::Float64`: Modified wizards per acre target
- `building_targets::Dict`: Building percentage targets
- `targets::Dict`: Additional optimization targets
- `science_base::Dict`: Base science requirements
"""
struct OptimizationTarget
    acres::Int
    income::Float64
    runes::Float64
    science::Union{Nothing,Float64}
    science_state::Any
    science_days::Int
    honor::Symbol
    opa::Union{Nothing,Float64}
    dpa::Union{Nothing,Float64}
    opa_dpa_ratio::Union{Nothing,Float64}
    mod_tpa::Float64
    mod_wpa::Float64
    building_targets::Dict
    targets::Dict
    science_base::Dict

    function OptimizationTarget(
        acres::Int,
        income::Float64,
        runes::Float64,
        science::Union{Nothing,Float64},
        science_state::Any,
        science_days::Int,
        honor::Symbol,
        opa::Union{Nothing,Float64},
        dpa::Union{Nothing,Float64},
        opa_dpa_ratio::Union{Nothing,Float64},
        mod_tpa::Float64,
        mod_wpa::Float64,
        building_targets::Dict,
        targets::Dict,
        science_base::Dict
    )
        # Validate inputs
        acres <= 0 && throw(ArgumentError("Acres must be positive"))
        income < 0 && throw(ArgumentError("Income target must be non-negative"))
        runes < 0 && throw(ArgumentError("Runes target must be non-negative"))
        science_days < 0 && throw(ArgumentError("Science days must be non-negative"))
        mod_tpa < 0 && throw(ArgumentError("Modified TPA target must be non-negative"))
        mod_wpa < 0 && throw(ArgumentError("Modified WPA target must be non-negative"))
        
        # Validate honor selection
        if !haskey(HONOR_BONUSSES, honor)
            throw(ArgumentError("Invalid honor selection: $honor"))
        end

        # Validate optimization targets
        if sum(x !== nothing for x in [opa, dpa, opa_dpa_ratio]) != 1
            throw(ArgumentError("Exactly one of opa, dpa, or opa_dpa_ratio must be specified"))
        end

        # Validate building targets
        for (building, target) in building_targets
            if !haskey(BUILDING_DATA, building)
                throw(ArgumentError("Invalid building target: $building"))
            end
            if haskey(target, :min) && target[:min] < 0
                throw(ArgumentError("Building minimum target must be non-negative"))
            end
            if haskey(target, :max) && target[:max] > 100
                throw(ArgumentError("Building maximum target must not exceed 100"))
            end
            if haskey(target, :eq) && (target[:eq] < 0 || target[:eq] > 100)
                throw(ArgumentError("Building equality target must be between 0 and 100"))
            end
        end

        # Validate science base requirements
        for (category, amount) in science_base
            if !haskey(SCIENCE_DATA.cats, category)
                throw(ArgumentError("Invalid science category: $category"))
            end
            if amount < 0
                throw(ArgumentError("Science base requirement must be non-negative"))
            end
        end

        new(acres, income, runes, science, science_state, science_days, honor,
            opa, dpa, opa_dpa_ratio, mod_tpa, mod_wpa, building_targets,
            targets, science_base)
    end
end

"""
    add_variables!(model::JuMP.Model, data::OptimizationTarget, logger::Logging.AbstractLogger)

Add variables to the optimization model.
"""
function add_variables!(model::JuMP.Model, data::OptimizationTarget, logger::Logging.AbstractLogger)
    @info "Adding model variables" _module=:UtopiaOptimizer _group=:optimization
    
    # Building variables
    building_count = length(keys(BUILDING_DATA))
    @variable(model, 0 <= buildings[1:building_count] <= 50, start = 5)
    
    # Population variables
    @variable(model, population[0:6] >= 0, start = 0)
    
    # Science book variables
    books_count = length(keys(SCIENCE_DATA.cats))
    @variable(model, books[1:books_count] >= 0)
    
    # Wage variable
    @variable(model, 20 <= wages <= 200, start = 100)
    
    @info "Added variables" buildings=building_count population=7 books=books_count _module=:UtopiaOptimizer _group=:optimization
    
    return (buildings, population, books, wages)
end

"""
    add_constraints!(model::JuMP.Model, data::OptimizationTarget, vars, effects, logger::Logging.AbstractLogger)

Add constraints to the optimization model.
"""
function add_constraints!(model::JuMP.Model, data::OptimizationTarget, vars, effects, logger::Logging.AbstractLogger)
    @info "Adding constraints" _module=:UtopiaOptimizer _group=:optimization
    
    try
        buildings, population, books, wages = vars
        building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
        definitions, SELECTED_HONOR, merged_building_data = effects
        
        # Population indices
        p_soldiers = 0
        p_ospecs = 1
        p_dspecs = 2
        p_elites_off = 3
        p_elites_def = 4
        p_wizards = 5
        p_thieves = 6

        @info "Adding basic constraints" _module=:UtopiaOptimizer _group=:optimization
        # Basic constraints
        @constraint(model, sum(buildings) == 100)
        @constraint(model, sum(books) <= data.science)
        
        # Science base constraints
        for (k, v) in data.SCIENCE_BASE
            @constraint(model, books[books_keys_lookup[k]] >= v)
        end

        @info "Adding population and jobs constraints" _module=:UtopiaOptimizer _group=:optimization
        # Population space and jobs constraints
        pop_space = calculate_population_space(model, data, vars, effects)
        jobs = @NLexpression(model, 
            sum(x for x in buildings) * data.ACRES / 100 * 25 - 
            buildings[building_keys_lookup[BUILDING_OPTS.HOMES]] * data.ACRES / 100 * 25
        )
        peasants = @NLexpression(model, pop_space - sum(x for x in population))
        @NLconstraint(model, peasants >= 0)

        # Jobs performance constraint
        pct_jobs_performed = @NLexpression(model, 
            (3 * peasants + building_effects[:prisoner_holding] / 2) / 
            (2 * (jobs * definitions.GHOST_WORKER_EFFECT))
        )
        @NLconstraint(model, pct_jobs_performed <= 1.0)

        @info "Adding resource constraints" _module=:UtopiaOptimizer _group=:optimization
        # Building efficiency
        be = @NLexpression(model, 
            (0.5 * (1.0 + pct_jobs_performed)) * 
            (1 + science_effects[SCIENCE_OPTS.TOOLS]) * 
            definitions.BE_MULTIPLIER
        )

        # Food constraints
        food_needed = @NLexpression(model, 
            (peasants + sum(x for x in population)) * 0.25 * 
            definitions.FOOD_NEEDED_MULTIPLIER
        )
        @NLconstraint(model, food_needed <= 
            (building_effects[:food_production] + 
             definitions.LAND_GENERATES_FOOD * data.ACRES + 
             definitions.DRYAD_LAND_FOOD_RUNES_MULTIPLIER * data.ACRES * 3) * 
            (1 + science_effects[SCIENCE_OPTS.PRODUCTION])
        )

        # Income and expenses
        income = @NLexpression(model, 
            ((3 + definitions.MINERS_MYSTIQUE_EFFECT) * peasants + 
             building_effects[:gc_production]) * 
            (1 + building_effects[:income_increase]) * 
            (1 + science_effects[SCIENCE_OPTS.ALCHEMY]) * 
            definitions.INCOME_MULTIPLIER * 
            (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1)
        )

        expenses = @NLexpression(model,
            ((population[p_ospecs] + population[p_dspecs]) * 0.5 + 
             (population[p_elites_off] + population[p_elites_def]) * 0.75) *
            (1 - building_effects[:wages_decrease]) * 
            (1 - science_effects[SCIENCE_OPTS.BOOKKEEPING]) *
            (wages / 100) * definitions.WAGES_MULTIPLIER
        )

        @NLconstraint(model, income - expenses >= data.income)

        # Runes
        runes_produced = @NLexpression(model, 
            (definitions.ELF_GUILD_RUNES * buildings[building_keys_lookup[BUILDING_OPTS.GUILDS]] * data.ACRES +
             definitions.DRYAD_LAND_FOOD_RUNES_MULTIPLIER * data.ACRES * 0.5 +
             building_effects[:rune_production]) * 
            (1 + science_effects[SCIENCE_OPTS.PRODUCTION]) * 
            definitions.RUNE_PROD_MULTIPLIER
        )
        @NLconstraint(model, runes_produced >= data.runes)

        @info "Adding TPA/WPA constraints" _module=:UtopiaOptimizer _group=:optimization
        # TPA/WPA constraints
        if data.OPT_RAW_TPA_AND_WPA
            @NLconstraint(model, population[p_thieves] / data.ACRES >= data.MOD_TPA)
            @NLconstraint(model, population[p_wizards] / data.ACRES >= data.MOD_WPA)
        else
            tpa = @NLexpression(model, 
                definitions.TPA_MODIFIER * 
                (population[p_thieves] * (1 + building_effects[:tpa_increase]) * 
                 (1 + science_effects[SCIENCE_OPTS.CRIME])) * 
                (((SELECTED_HONOR.tpa - 1) * definitions.HONOR_EFFECTS) + 1) / 
                data.ACRES
            )
            wpa = @NLexpression(model, 
                definitions.WPA_MODIFIER * 
                (population[p_wizards] * (1 + science_effects[SCIENCE_OPTS.CHANNELING])) * 
                (((SELECTED_HONOR.wpa - 1) * definitions.HONOR_EFFECTS) + 1) / 
                data.ACRES
            )
            @NLconstraint(model, tpa >= data.MOD_TPA)
            @NLconstraint(model, wpa >= data.MOD_WPA)
        end

        @info "Adding building target constraints" _module=:UtopiaOptimizer _group=:optimization
        # Building target constraints
        for (building, building_target) in data.BUILDING_TARGETS
            land_effect_multiplier = 1
            if :as_effective in keys(building_target) && building_target[:as_effective]
                if building in keys(definitions.LAND_EFFECT_MULTIPLIERS)
                    land_effect_multiplier = definitions.LAND_EFFECT_MULTIPLIERS[building]
                end
            end
            if :min in keys(building_target)
                @NLconstraint(model, 
                    buildings[building_keys_lookup[building]] * land_effect_multiplier >= 
                    building_target[:min]
                )
            end
            if :max in keys(building_target)
                @NLconstraint(model, 
                    buildings[building_keys_lookup[building]] * land_effect_multiplier <= 
                    building_target[:max]
                )
            end
            if :eq in keys(building_target)
                @NLconstraint(model, 
                    buildings[building_keys_lookup[building]] * land_effect_multiplier == 
                    building_target[:eq]
                )
            end
        end

        @info "Adding extra target constraints" _module=:UtopiaOptimizer _group=:optimization
        # Extra target constraints
        if haskey(data.TARGETS, EXTRA_TARGETS.MAX_PRISONERS)
            if (definitions.PRISONER_PER_ACRE == 0)
                @NLconstraint(model, 
                    building_effects[:prisoner_holding] <= 
                    data.TARGETS[EXTRA_TARGETS.MAX_PRISONERS]
                )
            end
        end

        if haskey(data.TARGETS, EXTRA_TARGETS.MAX_ELITE_OSPEC_RATIO)
            @NLconstraint(model, 
                (population[p_elites_def] + population[p_elites_off]) / 
                population[p_ospecs] <= 
                data.TARGETS[EXTRA_TARGETS.MAX_ELITE_OSPEC_RATIO]
            )
        end

        if haskey(data.TARGETS, EXTRA_TARGETS.MAX_ELITE_DSPEC_RATIO)
            @NLconstraint(model, 
                (population[p_elites_def] + population[p_elites_off]) / 
                population[p_dspecs] <= 
                data.TARGETS[EXTRA_TARGETS.MAX_ELITE_DSPEC_RATIO]
            )
        end

        @info "Adding horse and prisoner constraints" _module=:UtopiaOptimizer _group=:optimization
        # Horse and prisoner constraints
        if definitions.HORSE_PER_ACRE > 0
            horses = @NLexpression(model, population[p_ospecs] + population[p_elites_off])
        else
            horses = building_effects[:horse_holding]
            @NLconstraint(model, horses <= population[p_ospecs] + population[p_elites_off])
        end

        if definitions.PRISONER_PER_ACRE > 0
            prisoners = @NLexpression(model, (population[p_ospecs] + population[p_elites_off]) / 5)
            @NLconstraint(model, prisoners <= data.ACRES * definitions.PRISONER_PER_ACRE)
        end

        @NLconstraint(model, 
            building_effects[:prisoner_holding] <= 
            (population[p_ospecs] + population[p_elites_off]) / 5
        )

        @info "All constraints added successfully" _module=:UtopiaOptimizer _group=:optimization
    catch e
        @error "Error adding constraints" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:optimization
        rethrow(e)
    end
end

"""
    calculate_effects(model::JuMP.Model, target::OptimizationTarget, vars, logger::Logging.AbstractLogger)

Calculate building and science effects for the optimization model.
"""
function calculate_effects(model::JuMP.Model, target::OptimizationTarget, vars, logger::Logging.AbstractLogger)
    @info "Calculating effects" target_acres=target.acres target_honor=target.honor _module=:UtopiaOptimizer _group=:optimization
    
    buildings, population, books, wages = vars
    
    # Population indices
    p_soldiers = 0
    p_ospecs = 1
    p_dspecs = 2
    p_elites_off = 3
    p_elites_def = 4
    p_wizards = 5
    p_thieves = 6
    
    # Get definitions
    definitions = generate_definitions(target)
    SELECTED_HONOR = deepcopy(HONOR_BONUSSES[target.HONOR])
    
    @info "Calculating science effects" _module=:UtopiaOptimizer _group=:optimization
    # Calculate science effects
    s_effects = deepcopy(SCIENCE_DATA.cats)
    s_effects[SCIENCE_OPTS.CRIME] = merge(s_effects[SCIENCE_OPTS.CRIME], 
        (; mul=s_effects[SCIENCE_OPTS.CRIME].mul * definitions.CRIME_SCIENCE_MULTIPLIER))
    s_effects[SCIENCE_OPTS.CHANNELING] = merge(s_effects[SCIENCE_OPTS.CHANNELING], 
        (; mul=s_effects[SCIENCE_OPTS.CHANNELING].mul * definitions.CHANNELING_SCIENCE_MULTIPLIER))
    s_effects[SCIENCE_OPTS.CUNNING] = merge(s_effects[SCIENCE_OPTS.CUNNING], 
        (; mul=s_effects[SCIENCE_OPTS.CUNNING].mul * definitions.CUNNING_SCIENCE_MULTIPLIER))
    
    for (k, effect) in definitions.SCIENCE_BONUS_MODS
        s_effects[k] = merge(s_effects[k], (; mul=s_effects[k].mul * effect))
    end
    
    @info "Calculating building effects" _module=:UtopiaOptimizer _group=:optimization
    # Calculate building effects
    merged_building_data = deepcopy(BUILDING_DATA)
    for (building_idx, pair) in enumerate(definitions.WICKED_BUILDING_DATA)
        building = pair.first
        for (k, effect) in pair.second
            if haskey(merged_building_data, building)
                push!(merged_building_data[building], k => effect)
            else
                merged_building_data[building] = [k => effect]
            end
        end
    end
    
    # Create lookup dictionaries
    building_keys = collect(keys(merged_building_data))
    building_keys_lookup = Dict([v => idx for (idx, v) in enumerate(building_keys)])
    books_keys = collect(keys(s_effects))
    books_keys_lookup = Dict([v => idx for (idx, v) in enumerate(books_keys)])
    
    # Calculate building effects expressions
    building_effects = Dict{Symbol,JuMP.NonlinearExpression}()
    
    # Calculate non-BE affected building effects
    for (building_idx, pair) in enumerate(merged_building_data)
        for (k, effect) in pair.second
            if !effect[:be]
                if :flat_base in keys(effect)
                    mul = 1
                    if :flat_mul in keys(effect)
                        mul *= effect.flat_mul(definitions)
                    end
                    if :flat_cap_mul in keys(effect)
                        mul *= effect.flat_cap_mul(definitions)
                    end
                    if haskey(building_effects, k)
                        old_expr = building_effects[k]
                        building_effects[k] = @NLexpression(model, old_expr + 
                            (mul * effect.flat_base * buildings[building_idx] * target.ACRES / 100))
                    else
                        building_effects[k] = @NLexpression(model, 
                            mul * effect.flat_base * buildings[building_idx] * target.ACRES / 100)
                    end
                else
                    # Handle non-flat base effects
                    if :mul in keys(effect)
                        mul = effect.mul(definitions)
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + 
                                (mul * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        else
                            building_effects[k] = @NLexpression(model, 
                                mul * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000)
                        end
                    elseif :land_mul in keys(effect)
                        mul = effect.land_mul(definitions)
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + 
                                (mul * effect.base * buildings[building_idx] * (100 - mul * buildings[building_idx]) / 10000))
                        else
                            building_effects[k] = @NLexpression(model, 
                                mul * effect.base * buildings[building_idx] * (100 - mul * buildings[building_idx]) / 10000)
                        end
                    else
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + 
                                (effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        else
                            building_effects[k] = @NLexpression(model, 
                                effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000)
                        end
                    end
                end
            end
        end
    end
    
    @info "Calculating science expressions" _module=:UtopiaOptimizer _group=:optimization
    # Calculate science effects expressions
    science_effects = Dict(
        k => @NLexpression(
            model,
            (books[books_keys_lookup[k]]^(1 / 2.125)) * v.mul / 100 * 
            (1 + building_effects[:science_effect_increase]) * definitions.SCIENCE_MULTIPLIER
        )
        for (k, v) in s_effects
    )
    
    @info "Effects calculation complete" _module=:UtopiaOptimizer _group=:optimization
    return (building_effects, science_effects, building_keys_lookup, books_keys_lookup, 
            definitions, SELECTED_HONOR, merged_building_data)
end

"""
    optimize_province(target::OptimizationTarget, config::AbstractOptimizerConfig=StandardOptimizerConfig())

Optimize a province configuration based on given targets and configuration.

Returns a tuple of (DataFrames, values, definitions) containing the optimization results.
"""
function optimize_province(
    target::OptimizationTarget, 
    config::AbstractOptimizerConfig=StandardOptimizerConfig()
)::Union{Nothing,Tuple}
    # Set up logging
    logger = if config.silent
        Logging.NullLogger()
    else
        Logging.ConsoleLogger(stderr, Logging.Info)
    end
    
    # Initialize model with solver configuration
    nl_solver = JuMP.optimizer_with_attributes(Ipopt.Optimizer, 
        "print_level" => config.silent ? 0 : 5,
        "print_user_options" => config.silent ? "no" : "yes",
        "max_cpu_time" => config.time_limit
    )
    mip_solver = JuMP.optimizer_with_attributes(Cbc.Optimizer, 
        "logLevel" => config.silent ? 0 : 1,
        "seconds" => config.time_limit
    )
    model = JuMP.Model(JuMP.optimizer_with_attributes(Juniper.Optimizer,
        "nl_solver" => nl_solver,
        "mip_solver" => mip_solver,
        "branch_strategy" => :MostInfeasible,
        "feasibility_pump" => true,
        "time_limit" => config.time_limit
    ))

    Logging.with_logger(logger) do
        @info "Starting optimization" acres=target.ACRES honor=target.HONOR _module=:UtopiaOptimizer _group=:optimization
        
        try
            @info "Setting up model variables and constraints" _module=:UtopiaOptimizer _group=:optimization
            # Add variables
            vars = add_variables!(model, target, logger)
            
            # Calculate effects
            effects = calculate_effects(model, target, vars, logger)
            
            # Add constraints
            add_constraints!(model, target, vars, effects, logger)
            
            # Set objective
            set_objective!(model, target, vars, effects)
            
            @info "Starting solver" _module=:UtopiaOptimizer _group=:optimization
            # Optimize
            JuMP.optimize!(model)
            
            # Process results with detailed error handling
            status = JuMP.termination_status(model)
            if status == JuMP.OPTIMAL
                @info "Found optimal solution" _module=:UtopiaOptimizer _group=:optimization
                return process_results(model, target, vars, effects, config)
            elseif status == JuMP.TIME_LIMIT
                @warn "Optimization stopped due to time limit" _module=:UtopiaOptimizer _group=:optimization
                if JuMP.has_values(model) && JuMP.primal_status(model) == JuMP.FEASIBLE_POINT
                    @info "Returning best solution found" _module=:UtopiaOptimizer _group=:optimization
                    return process_results(model, target, vars, effects, config)
                end
                return nothing
            elseif status == JuMP.INFEASIBLE
                @error "Problem is infeasible - check your constraints and targets" _module=:UtopiaOptimizer _group=:optimization
                return nothing
            elseif status == JuMP.NUMERICAL_ERROR
                @error "Encountered numerical issues - try adjusting your targets or solver parameters" _module=:UtopiaOptimizer _group=:optimization
                return nothing
            else
                @error "Optimization failed" status=status _module=:UtopiaOptimizer _group=:optimization
                return nothing
            end
        catch e
            @error "Error during optimization" exception=(e, catch_backtrace()) _module=:UtopiaOptimizer _group=:optimization
            rethrow(e)
        end
    end
end

# Helper functions for military calculations
include("military_calculations.jl")

# Helper functions for DataFrame creation
include("dataframe_helpers.jl")

end # module
