module GameDefinitions

using DataStructures

using ..DataStructs

export generate_definitions, HONOR_BONUSSES, DataStructs, SCIENCE_DATA, BUILDING_DATA

HONOR_BONUSSES = Dict(
        HONOR_OPTS.PEASANT => (pop=1, me=1, income=1, wpa=1, tpa=1),
        HONOR_OPTS.KNIGHT => (pop=1.01, me=1.01, income=1.02, wpa=1.03, tpa=1.03),
        HONOR_OPTS.LORD => (pop=1.02, me=1.02, income=1.04, wpa=1.06, tpa=1.06),
        HONOR_OPTS.BARON => (pop=1.03, me=1.03, income=1.06, wpa=1.09, tpa=1.09),
        HONOR_OPTS.VISCOUNT => (pop=1.04, me=1.04, income=1.08, wpa=1.12, tpa=1.12),
        HONOR_OPTS.COUNT => (pop=1.06, me=1.06, income=1.12, wpa=1.18, tpa=1.18),
        HONOR_OPTS.MARQUIS => (pop=1.08, me=1.08, income=1.16, wpa=1.24, tpa=1.24),
        HONOR_OPTS.DUKE => (pop=1.1, me=1.1, income=1.2, wpa=1.3, tpa=1.3),
        HONOR_OPTS.PRINCE => (pop=1.12, me=1.12, income=1.24, wpa=1.36, tpa=1.36)
    )

SCIENCE_DATA = (
    levels = [60,80,100,120],
    rank_up = [1440,5280,12480],
    training_speed = 2,
    cats = Dict(
        SCIENCE_OPTS.ALCHEMY => (mul=0.0724, group=SCIENCE_GROUP_OPTS.ECONOMY),
        SCIENCE_OPTS.TOOLS => (mul=0.0524, group=SCIENCE_GROUP_OPTS.ECONOMY),
        SCIENCE_OPTS.HOUSING => (mul=0.0262, group=SCIENCE_GROUP_OPTS.ECONOMY),
        SCIENCE_OPTS.PRODUCTION => (mul=0.2172, group=SCIENCE_GROUP_OPTS.ECONOMY),
        SCIENCE_OPTS.BOOKKEEPING => (mul=0.068, group=SCIENCE_GROUP_OPTS.ECONOMY),
        SCIENCE_OPTS.ARTISAN => (mul=0.0478, group=SCIENCE_GROUP_OPTS.ECONOMY),
        SCIENCE_OPTS.STRATEGY => (mul=0.0367, group=SCIENCE_GROUP_OPTS.WAR),
        SCIENCE_OPTS.SIEGE => (mul=0.0262, group=SCIENCE_GROUP_OPTS.WAR),
        SCIENCE_OPTS.TACTICS => (mul=0.0367, group=SCIENCE_GROUP_OPTS.WAR),
        SCIENCE_OPTS.VALOR => (mul=0.0582, group=SCIENCE_GROUP_OPTS.WAR),
        SCIENCE_OPTS.RESILIENCE => (mul=0.0367 * 4 / 3, group=SCIENCE_GROUP_OPTS.WAR),
        SCIENCE_OPTS.HEROISM => (mul=0.0418, group=SCIENCE_GROUP_OPTS.WAR),
        SCIENCE_OPTS.CRIME => (mul=0.1557, group=SCIENCE_GROUP_OPTS.ARCANE),
        SCIENCE_OPTS.CHANNELING => (mul=0.1875, group=SCIENCE_GROUP_OPTS.ARCANE),
        SCIENCE_OPTS.SHIELDING => (mul=0.0314, group=SCIENCE_GROUP_OPTS.ARCANE),
        SCIENCE_OPTS.CUNNING => (mul=0.0314, group=SCIENCE_GROUP_OPTS.ARCANE),
        SCIENCE_OPTS.SORCERY => (mul=0.0314, group=SCIENCE_GROUP_OPTS.ARCANE),
        SCIENCE_OPTS.FINESSE => (mul=0.0724  * 4 / 3, group=SCIENCE_GROUP_OPTS.ARCANE),
    )
)

BUILDING_DATA =  OrderedDict{BUILDING_OPTS.BUILDING, Vector{Pair{Symbol, NamedTuple}}}(
    BUILDING_OPTS.HOMES => [
        :birthrate => (base=4, be=true)
    ],
    BUILDING_OPTS.FARMS => [
        :food_production => (flat_base=60, be=true, flat_mul= (d) -> d.BUILD_PROD_MULTIPLIER)
    ],
    BUILDING_OPTS.MILLS => [
        :build_cost_decrease => (base=4, be=true),
        :explore_cost_gold_decrease => (base=3, be=true),
        :explore_cost_soldier_decrease => (base=2, be=true),
    ],
    BUILDING_OPTS.BANKS => [
        :gc_production => (flat_base=25, be=true, flat_mul = (d) -> d.BUILD_PROD_MULTIPLIER),
        :income_increase => (base=1.5, be=true),
    ],
    BUILDING_OPTS.TRAINING_GROUNDS => [
        :ome_increase => (base=1.5, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.TRAINING_GROUNDS, 1)),
        #:kills_increase => (base=0.5, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.TRAINING_GROUNDS, 1)),
        :training_time_decrease => (base=1, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.TRAINING_GROUNDS, 1)),
    ],
    BUILDING_OPTS.ARMOURIES => [
        :draft_cost_decrease => (base=2, be=true),
        :wages_decrease => (base=2, be=true),
        :training_cost_decrease => (base=1.5, be=true),
    ],
    BUILDING_OPTS.MILITARY_BARRACKS => [
        :attack_time_decrease => (base=1.5, be=true, mul = (d) -> d.BARRACKS_EFFECT_MULTIPLIER),
        :merc_cost_decrease => (base=2, be=true, mul = (d) -> d.BARRACKS_EFFECT_MULTIPLIER),
    ],
    BUILDING_OPTS.FORTS => [
        :dme_increase => (base=1.5, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.FORTS, 1)),
    ],
    BUILDING_OPTS.CASTLES => [
        :land_loss_decrease => (base=1.5, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.CASTLES, 1)),
        :honor_loss_decrease => (base=1.5, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.CASTLES, 1)),
    ],
    BUILDING_OPTS.HOSPITALS => [
        :military_losses_decrease => (base=3, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.HOSPITALS, 1)),
    ],
    BUILDING_OPTS.GUILDS => [
        :wizard_production => (flat_base=0.02, be=false),

    ],
    BUILDING_OPTS.TOWERS => [
        :rune_production => (flat_base=12, be=true, flat_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.TOWERS, 1) * d.BUILD_PROD_MULTIPLIER),

    ],
    BUILDING_OPTS.THIEVES_DENS => [
        :thief_losses_decrease => (base=3.6, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.THIEVES_DENS, 1)),
        :tpa_increase => (base=3, be=true, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.THIEVES_DENS, 1)),
    ],
    BUILDING_OPTS.WATCH_TOWERS => [
        :catch_thieves => (base=1.5, be=true, mul = (d) -> d.WT_EFFECT_MULTIPLIER),
        :thief_damage_decrease => (base=2.5, be=true, mul = (d) -> d.WT_EFFECT_MULTIPLIER),
    ],
    BUILDING_OPTS.UNIVERSITIES => [
        :scientist_spawn_increase => (base=1.5, be=false),
        #:science_loss_decrease => (base=2, be=true),
        :science_books_increase => (base=1, be=false),
    ],
    BUILDING_OPTS.LIBRARIES => [
        :science_effect_increase => (base=1.0, be=false, land_mul = (d) -> get(d.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.LIBRARIES, 1)),
    ],
    BUILDING_OPTS.STABLES => [
        :horse_holding => (flat_base=80, be=false, flat_cap_mul = (d) -> d.BUILD_CAP_MULTIPLIER),
    ],
    BUILDING_OPTS.DUNGEONS => [
        :prisoner_holding => (flat_base=30, be=false, flat_cap_mul = (d) -> d.BUILD_CAP_MULTIPLIER * d.DUNGEON_CAPACITY_MODIFIER),
    ]
)

function generate_definitions(target::DataStructs.OptimizationTarget)
    definitions = OptimizationDefinitions()

    ### RACES ###

    #     
    # AVIAN
    # Bonuses
    # -25% Attack Time
    # +60% Birth Rate
    # Immunity to Ambush
    # 
    # Access to Greater Protection, Clear Sight
    # 
    # Unique Ability: Skybound Strike
    # 
    # Unique Ability:Once activated, the next attack will automatically succeed, and suffer no offensive losses. In addition, gains will be reduced based on offense sent compared to defense of the target (examples: 75% break means 75% gains, 25% break means a 25% damage massacre ). Cooldown: 24 Utopian Days
    # 
    # 
    # Penalties
    # +15% Military Casualties
    # No Access to Stables and War Horses
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 11/0, 4.4nw
    # Defensive Specialist - 0/10, 5.0nw
    # Elite Unit - 14/3, 900gc, 6.5nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - n/a
    #     
    if target.RACE == RACE_OPTS.AVIAN
        # Unique Ability: Skybound Strike
        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 11 # Offensive Specialist - 11/0, 4.4nw
        definitions.DSPEC_DEF = 10
        definitions.ELITE_OFF = 14 # Elite Unit - 14/3, 900gc, 6.5nw
        definitions.ELITE_DEF = 3 # Elite Unit - 14/3, 900gc, 6.5nw
        definitions.ELITE_NW = 6.5 # Elite Unit - 14/3, 900gc, 6.5nw
        definitions.HORSE_OFF = 0  # No Access to Stables and War Horses

        # Bonuses
        definitions.ATTACK_TIME_MODIFIER *= 0.75  # -25% Attack Time
        # +60% Birth Rate
        # Immunity to Ambush - Implementation depends on game mechanics

        # Penalties
        definitions.ATTACK_LOSSES_MODIFIER *= 1.15  # +15% Military Casualties
        # No Access to Stables and War Horses already handled by HORSE_OFF = 0

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.GREATER_PROTECTION, SELF_SPELLS.CLEAR_SIGHT]) # Access to Greater Protection, Clear Sight
    end

    # 
    # DARK ELF
    # Bonuses
    # No Rune Cost (Excluding Ritual)
    # +15% Magic Effectiveness (WPA)
    # Can train Thieves with Specialist Credits
    # 
    # Access to Blizzard, Guile, Mage's Fury, Fools Gold, Magic Ward, Invisibility
    # 
    # Unique Ability: Shadow Surge
    # Activated Ability: When activated, offensive spells have a 20% chance to cast again at no mana cost for 6 Utopian Days. Cooldown: 24 Utopian Days
    # 
    # Penalties
    # +30% Exploration Costs
    # +30% Military Wages
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 11/0, 4.4nw
    # Defensive Specialist - 0/10, 5.0nw
    # Elite Unit - 10/10, 1050gc, 7.0nw
    # Mercenary - 8/0, 0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.DARKELF
        # Unique Ability: Shadow Surge
        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 11 # Offensive Specialist - 11/0, 4.4nw
        definitions.DSPEC_DEF = 10 # Defensive Specialist - 0/10, 5.0nw
        definitions.ELITE_OFF = 10 # Elite Unit - 10/10, 1050gc, 7.0nw
        definitions.ELITE_DEF = 10 # Elite Unit - 10/10, 1050gc, 7.0nw
        definitions.ELITE_NW = 7.0 # Elite Unit - 10/10, 1050gc, 7.0nw
        definitions.HORSE_OFF = 2 # War Horse - 2/0, 0.6nw

        # Bonuses
        definitions.RUNES_WANTED_MOD *= 0.0  # No Rune Cost (Excluding Ritual)
        definitions.WPA_MODIFIER *= 1.15  # +15% Magic Effectiveness (WPA)
        # Can train Thieves with Specialist Credits

        # Penalties
        # +30% Exploration Costs
        definitions.WAGES_MULTIPLIER *= 1.30  # +30% Military Wages

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.GUILE, SELF_SPELLS.INVISIBILITY, SELF_SPELLS.MAGES_FURY]) # Access to Blizzard, Guile, Mage's Fury, Fools Gold, Magic Ward, Invisibility
    end

    # 
    # DRYAD
    # Bonuses
    # +10% Offensive Military Efficiency
    # +10% Defensive Military Efficiency
    # Wages are "always paid" ***See warning at start of post***
    # 
    # Access to no additional spells
    # 
    # Unique Ability: Roots of Ruin
    # Passive: Upon successful attacks, Dryads extend their enchanted roots into enemy lands, draining vitality and destroying 1% of their entire population
    # (the intent is that its kills 1% of your peasants, thieves, wizards, peasants, and military. army in-out-and in training---these will all be tested and noted here)
    # 
    # Penalties
    # -30% Thievery effectiveness (TPA)
    # -15% income
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 10/0, 4.0nw
    # Defensive Specialist - 0/10, 5.0nw
    # Elite Unit - 13/5, 950gc, 7.0nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.DRYAD
        # Unique Ability: Roots of Ruin
        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 10 # Offensive Specialist - 10/0, 4.0nw
        definitions.DSPEC_DEF = 10 # Defensive Specialist - 0/10, 5.0nw
        definitions.ELITE_OFF = 13 # Elite Unit - 13/5, 950gc, 7.0nw
        definitions.ELITE_DEF = 5 # Elite Unit - 13/5, 950gc, 7.0nw
        definitions.ELITE_NW = 7.0 # Elite Unit - 13/5, 950gc, 7.0nw
        definitions.HORSE_OFF = 2  # War Horse - 2/0, 0.6nw
        # Mercenary - 8/0, 0.0nw
        definitions.PRISONER_OFF = 8  # Prisoner - 8/0, 1.6nw

        # Bonuses
        definitions.OME_MODIFIER *= 1.1 # +10% Offensive Military Efficiency
        definitions.DME_MODIFIER *= 1.1 # +10% Defensive Military Efficiency
        # Wages are "always paid"

        # Penalties
        definitions.TPA_MODIFIER *= 0.7 # -30% Thievery effectiveness (TPA)
        definitions.INCOME_MULTIPLIER *= 0.85 # -15% income
    end

    # 
    # DWARF
    # Bonuses
    # +30% Building Efficiency
    # -50% Construction Time
    # 
    # Access to Miner’s Mystique
    # 
    # Unique Ability: Earthshaker
    # Activated Ability: When activated, the next attack if successful will raze all Forts and 5% of all other buildings on the target province. Cooldown: 24 Utopian Days.
    # 
    # Penalties
    # Cannot use Accelerated Construction
    # +100% Food Consumption
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 10/0, 4.0nw
    # Defensive Specialist - 0/10, 5.0nw
    # Elite Unit - 13/6, 1000gc, 7.5nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.DWARF
        # Unique Ability: Earthshaker

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 10 # Offensive Specialist - 10/0, 4.0nw
        definitions.DSPEC_DEF = 10 # Defensive Specialist - 0/10, 5.0nw
        definitions.ELITE_OFF = 13 # Elite Unit - 13/6, 1000gc, 7.5nw
        definitions.ELITE_DEF = 6 # Elite Unit - 13/6, 1000gc, 7.5nw
        definitions.ELITE_NW = 7.5 # Elite Unit - 13/6, 1000gc, 7.5nw
        definitions.HORSE_OFF = 2 # War Horse - 2/0, 0.6nw

        # Bonuses
        definitions.BE_MULTIPLIER *= 1.3  # +30% Building Efficiency
        # -50% Construction Time
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.MINERS_MYSTIQUE]) # Access to Miner’s Mystique

        # Penalties
        # Cannot use Accelerated Construction - Not directly implementable in modifier
        definitions.FOOD_NEEDED_MULTIPLIER *= 2.0  # +100% Food Consumption
    end

    # 
    # ELF
    # Bonuses
    # +25% Magic Effectiveness (WPA)
    # -20% Military Casualties
    # 
    # Access to Chastity, Mist, Pitfalls, Wrath
    # 
    # Unique Ability: Mana Well
    # Activated ability: When activated, fully restores the caster's mana by 50%. Cooldown: 24 Utopian Days.
    # 
    # Penalties
    # +20% Military Wages
    # No Access to Dungeons and Prisoners
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 10/0, 4.0nw
    # Defensive Specialist - 0/11, 5.5nw
    # Elite Unit - 12/4, 900gc, 5.75nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.ELF
        # Unique Ability: Mana Well

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 10 # Offensive Specialist - 10/0, 4.0nw
        definitions.DSPEC_DEF = 11 # Defensive Specialist - 0/11, 5.5nw
        definitions.ELITE_OFF = 12 # Elite Unit - 12/4, 900gc, 5.75nw
        definitions.ELITE_DEF = 4 # Elite Unit - 12/4, 900gc, 5.75nw
        definitions.ELITE_NW = 5.75 # Elite Unit - 12/4, 900gc, 5.75nw
        definitions.HORSE_OFF = 2 # War Horse - 2/0, 0.6nw

        # Bonuses
        definitions.WPA_MODIFIER *= 1.25  # +25% Magic Effectiveness (WPA)
        definitions.ATTACK_LOSSES_MODIFIER *= 0.80  # -20% Military Casualties

        # Penalties
        definitions.WAGES_MULTIPLIER *= 1.20  # +20% Military Wages
        definitions.LAND_EFFECT_MULTIPLIERS[BUILDING_OPTS.DUNGEONS] = 0  # No Access to Dungeons and Prisoners

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.MIST, SELF_SPELLS.WRATH]) # Access to Chastity, Mist, Pitfalls, Wrath
    end

    # 
    # FAERY
    # Bonuses
    # +25% Offensive Spell Duration
    # +25% Self Spell Duration
    # +1 Mana per Tick in War
    # 
    # Access to Tree of Gold, Quick Feet, Town Watch, Vermin, Chastity, Mist, Pitfalls, Wrath, Miner’s Mystique, Blizzard, Guile, Mage's Fury, Greater Protection, Clear Sight, Fountain of Knowledge, Salvation, Magic Ward
    # 
    # Unique Ability: Ethereal Mirage
    # Passive: Mystic Vortex removes all active spells from the target province when successfully cast.
    # 
    # Penalties
    # -10% Population
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 10/0, 4.0nw
    # Defensive Specialist - 0/10, 5.0nw
    # Elite Unit - 4/12, 850gc, 6.75nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.0nw
    #     
    if target.RACE == RACE_OPTS.FAERY
        # Unique Ability: Ethereal Mirage

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 10 # Offensive Specialist - 10/0, 4.0nw
        definitions.DSPEC_DEF = 10 # Defensive Specialist - 0/10, 5.0nw
        definitions.ELITE_OFF = 4 # Elite Unit - 4/12, 850gc, 6.75nw
        definitions.ELITE_DEF = 12 # Elite Unit - 4/12, 850gc, 6.75nw
        definitions.ELITE_NW = 6.75 # Elite Unit - 4/12, 850gc, 6.75nw
        definitions.HORSE_OFF = 2  # War Horse - 2/0, 0.0nw

        # Bonuses
        # +25% Offensive Spell Duration
        # +25% Self Spell Duration
        # +1 Mana per Tick in War

        # Penalties
        definitions.POP_MULTIPLIER *= 0.90  # -10% Population

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.TREE_OF_GOLD, SELF_SPELLS.QUICKFEET, SELF_SPELLS.TOWN_WATCH, SELF_SPELLS.MIST, SELF_SPELLS.WRATH, SELF_SPELLS.MINERS_MYSTIQUE, SELF_SPELLS.GUILE, SELF_SPELLS.MAGES_FURY, SELF_SPELLS.GREATER_PROTECTION, SELF_SPELLS.CLEAR_SIGHT, SELF_SPELLS.FOUNTAIN_OF_KNOWLEDGE]) # Access to Tree of Gold, Quick Feet, Town Watch, Vermin, Chastity, Mist, Pitfalls, Wrath, Miner’s Mystique, Blizzard, Guile, Mage's Fury, Greater Protection, Clear Sight, Fountain of Knowledge, Salvation, Magic Ward
    end

    # 
    # HALFLING
    # Bonuses
    # +15% Population
    # +25% Thievery Effectiveness (TPA)
    # -10% Military Training Time
    # 
    # Access to Quick Feet, Town Watch, Vermin
    # 
    # Unique Ability: Sneak Attack
    # Activated ability: When activated, Thievery operation incur no losses for 1 Utopian Day. Cooldown: 24 Utopian Days.
    # 
    # Penalties
    # -25% Birth Rate
    # +15% Military Wages
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 9/0, 3.6nw
    # Defensive Specialist - 0/9, 4.5nw
    # Elite Unit - 11/5, 850gc, 6.25nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.HALFLING
        # Unique Ability: Sneak Attack

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 9 # Offensive Specialist - 9/0, 3.6nw
        definitions.DSPEC_DEF = 9 # Defensive Specialist - 0/9, 4.5nw
        definitions.ELITE_OFF = 11 # Elite Unit - 11/5, 850gc, 6.25nw
        definitions.ELITE_DEF = 5 # Elite Unit - 11/5, 850gc, 6.25nw
        definitions.ELITE_NW = 6.25 # Elite Unit - 11/5, 850gc, 6.25nw
        definitions.HORSE_OFF = 2 # War Horse - 2/0, 0.6nw

        # Bonuses
        definitions.POP_MULTIPLIER *= 1.15  # +15% Population
        definitions.TPA_MODIFIER *= 1.25  # +25% Thievery Effectiveness (TPA)
        # -10% Military Training Time

        # Penalties
        # -25% Birth Rate
        definitions.WAGES_MULTIPLIER *= 1.15 # +15% Military Wages

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.QUICKFEET, SELF_SPELLS.TOWN_WATCH]) # Access to Quick Feet, Town Watch, Vermin
    end

    # 
    # HUMAN
    # Bonuses
    # +30% Science Efficiency
    # +15% Income
    # 
    # Access to Aggression, Fountain of Knowledge, Greater Protection, Reflect Magic
    # 
    # Unique Ability: Interest
    # Activated ability: When activated, provides an additional extreme activity bonus for the province. Cooldown: 24 Utopian Days.
    # 
    # Penalties
    # -80% Magic Effectiveness (WPA) on Defense
    # Can not Run Libraries
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 10/0, 4.0nw
    # Defensive Specialist - 0/10, 5.0nw
    # Elite Unit - 11/5, 900gc, 6.5nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 3/0, 0.9nw
    #     
    if target.RACE == RACE_OPTS.HUMAN
        # Unique Ability: Interest

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 10 # Offensive Specialist - 10/0, 4.0nw
        definitions.DSPEC_DEF = 10 # Defensive Specialist - 0/10, 5.0nw
        definitions.ELITE_OFF = 11 # Elite Unit - 11/5, 900gc, 6.5nw
        definitions.ELITE_DEF = 5 # Elite Unit - 11/5, 900gc, 6.5nw
        definitions.ELITE_NW = 6.5 # Elite Unit - 11/5, 900gc, 6.5nw
        definitions.HORSE_OFF = 3 # War Horse - 3/0, 0.9nw

        # Bonuses
        definitions.SCIENCE_MULTIPLIER *= 1.30  # +30% Science Efficiency
        definitions.INCOME_MULTIPLIER *= 1.15  # +15% Income

        # Penalties
        # -80% Magic Effectiveness (WPA) on Defense
        # definitions.WPA_MODIFIER *= 0.20 this is no the asme
        definitions.LAND_EFFECT_MULTIPLIERS[BUILDING_OPTS.LIBRARIES] = 0  # Can not Run Libraries

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.AGGRESSION, SELF_SPELLS.FOUNTAIN_OF_KNOWLEDGE, SELF_SPELLS.GREATER_PROTECTION, SELF_SPELLS.REFLECT_MAGIC]) # Access to Aggression, Fountain of Knowledge, Greater Protection, Reflect Magic
    end

    # 
    # ORC
    # Bonuses
    # +30% Battle (Resource) Gains
    # -50% Draft Cost
    # Train Elites with Specialist Credits
    # 
    # Access to Aggression, Bloodlust
    # 
    # Unique Ability: Warlord’s Fury
    # Passive: Successful attacks plunder an additional 15% resources (gold, runes, food) based on traditional plunder attack, and return 25% of military casualties(soldiers specialists, and elites) home instantly.
    # (if you lost 100 troops when you attacked, 25 of those troops are home to attack with your next attack)
    #
    # Penalties
    # -20% Credits gained on successful attacks (Building and Specialist Credits)
    # -10% Building Effectiveness
    #
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 11/0, 4.4nw
    # Defensive Specialist - 0/9, 4.5nw
    # Elite Unit - 15/4, 950gc, 7.25nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.ORC
        # Unique Ability: Warlord’s Fury

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 11 # Offensive Specialist - 11/0, 4.4nw
        definitions.DSPEC_DEF = 9 # Defensive Specialist - 0/9, 4.5nw
        definitions.ELITE_OFF = 15 # Elite Unit - 15/4, 950gc, 7.25nw
        definitions.ELITE_DEF = 4 # Elite Unit - 15/4, 950gc, 7.25nw
        definitions.ELITE_NW = 7.25 # Elite Unit - 15/4, 950gc, 7.25nw
        definitions.HORSE_OFF = 2 # War Horse - 2/0, 0.6nw

        # Bonuses
        definitions.COMBAT_GAINS_MULTIPLIER *= 1.30  # +30% Battle (Resource) Gains
        # -50% Draft Cost
        # Train Elites with Specialist Credits

        # Penalties
        # -20% Credits gained on successful attacks (Building and Specialist Credits)
        definitions.BE_MULTIPLIER *= 0.90  # -10% Building Effectiveness

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.AGGRESSION, SELF_SPELLS.BLOODLUST]) # Access to Aggression, Bloodlust
    end
    
    # 
    # GNOMES
    # Bonuses
    # +25% Birth Rates
    # +25% Enemy Military Casualties
    # +20% Population
    # 
    # Access to Town Watch
    # 
    # Unique Ability: Natures Feast
    # Activated ability: When activated, the next successful attack depletes the target’s food reserves to zero. Cooldown: 24 Utopian Days.
    # .
    # 
    # Penalties
    # +60% damage from all magic sources (+ damage from instant, and + effect from fading spells)
    # 
    # Soldier - 3/0, 0.75nw
    # Offensive Specialist - 9/0, 3.6nw
    # Defensive Specialist - 0/9, 4.5nw
    # Elite Unit - 9/9, 800gc, 7.0nw
    # Mercenary - 8/0, 0.0nw
    # Prisoner - 8/0, 1.6nw
    # War Horse - 2/0, 0.6nw
    #     
    if target.RACE == RACE_OPTS.GNOME
        # Unique Ability: Natures Feast

        # Unit Specifications
        definitions.SOLDIER_OFF = 3 # Soldier - 3/0, 0.75nw
        definitions.OSPEC_OFF = 9 # Offensive Specialist - 9/0, 3.6nw
        definitions.DSPEC_DEF = 9 # Defensive Specialist - 0/9, 4.5nw
        definitions.ELITE_OFF = 9 # Elite Unit - 9/9, 800gc, 7.0nw
        definitions.ELITE_DEF = 9 # Elite Unit - 9/9, 800gc, 7.0nw
        definitions.ELITE_NW = 7.0 # Elite Unit - 9/9, 800gc, 7.0nw
        definitions.HORSE_OFF = 2 # War Horse - 2/0, 0.6nw

        # Bonuses
        # +25% Birth Rates
        # +25% Enemy Military Casualties
        definitions.POP_MULTIPLIER *= 1.20  # +20% Population

        # Penalties
        # +60% damage from all magic sources (+ damage from instant, and + effect from fading spells)

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.TOWN_WATCH]) # Access to Town Watch
    end

    # UNDEAD
    # // UNDEAD REMOVED IN NEW AGE
    ## UNDEAD
    # # Undead
    # # Bonuses
    # # (Undead race removed in the new age)
    # 
    # # Penalties
    # # (Undead race removed in the new age)
    #
    # if target.RACE == RACE_OPTS.UNDEAD
    #     # Code related to Undead race is removed/commented out
    # end

    ### PERSONALITIES ###

    # 
    # The Artisan
    # +50% Building Capacity (Homes, Stables, Dungeons)
    # +50% Building Production (Banks, Farms, Stables, Towers)
    # +50% Building Credits gained in Combat
    # +100% Successful Espionage Operations for double stealth cost
    # -20% Building Damage taken from Arson, Greater Arson, Tornado, Raze
    # +20% Artisan Science Efficiency
    # 
    # Starts with +600 Soldiers, +600 Specialist Credits and +200 Building Credits
    # 
    # Unique Passive: Masterful Craftsmanship Artisans have a 25% chance to receive 15% of a building's cost back as building credits upon construction
    #     
    if target.PERS == PERS_OPTS.ARTISAN
        # Unique Passive: Masterful Craftsmanship

        # Bonuses
        definitions.BUILD_CAP_MULTIPLIER *= 1.50  # +50% Building Capacity
        definitions.BUILD_PROD_MULTIPLIER *= 1.50  # +50% Building Production
        # +50% Building Credits gained in Combat
        # +100% Successful Espionage Operations for double stealth cost
        # -20% Building Damage taken from Arson, Greater Arson, Tornado, Raze
        definitions.SCIENCE_BONUS_MODS[SCIENCE_OPTS.ARTISAN] = 1.20  # +20% Artisan Science Efficiency
    end

    #     
    # The Necromancer
    # +15% Wizard effectiveness (WPA)
    # Access to Nightmares
    # 
    # Starting Bonus: +500 Wizards, +500 Specialist Credits
    # 
    # Unique Passive: Dark pact After each successful attack, The Necromancer converts:
    # 
    #     5% of the enemy's casualties into Wizards for the Necromancers province. Minimum gain of 1 wizard per a successful attack.
    #     10% of the enemy's casualties into Soldiers for the Necromancers province. Minimum gain of 1 soldier per a successful attack.
    #     20% of the enemy's casualties into Peasants for the Necromancers province. Minimum gain of 1 Peasant per a successful attack.
    #     
    if target.PERS == PERS_OPTS.NECROMANCER
        # Unique Passive: Dark pact
        
        # Bonuses
        definitions.WPA_MODIFIER *= 1.15 # +15% Wizard effectiveness (WPA)

        # Access to spells
        # Acess to nightmares
    end

    # 
    # The Cleric
    # +1 Elite Defensive Value
    # +1 Defensive Specialist Strength
    # - 1 Self Spell Mana Cost
    # -25% Damage from Instant Spells
    # Access To Salvation, Revelation, Divine Shield, Illuminate Shadows
    # 
    # Starts with +800 Soldiers and +800 Specialist Credits
    # 
    # Unique Passive: Divine Favou: When casting self-spells, Clerics have a 50% chance to double the spell's duration, symbolizing their deep connection to divine powers
    #     
    if target.PERS == PERS_OPTS.CLERIC
        # Unique Passive: Divine Favou

        # Bonuses
        definitions.ELITE_DEF += 1  # +1 Elite Defensive Value
        definitions.DSPEC_DEF += 1  # +1 Defensive Specialist Strength
        #- 1 Self Spell Mana Cost
        #definitions.OP_DAMAGE_RECEIVED_MULTIPLIER *= 0.75  # -25% Damage from Instant Spells

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.SALVATION, SELF_SPELLS.REVELATION, SELF_SPELLS.DIVINE_SHIELD, SELF_SPELLS.ILLUMINATE_SHADOWS]) # Access To Salvation, Revelation, Divine Shield, Illuminate Shadows
    end

    # 
    # The Heretic
    # +50% Wizard Production
    # -50% Thieves lost on Thievery Operations
    # +25% Thievery Efficiency (TPA)
    # +10% Magic Efficiency (WPA)
    # Access to Meteor Showers, Greater Arson
    # 
    # Starts with +400 Wizards and +400 Thieves
    # 
    # Unique Passive: Blasphemous Might After a failed offensive spell cast, the heretic recovers 5% Stealth.
    #     
    if target.PERS == PERS_OPTS.HERETIC
        # Unique Passive: Blasphemous Might

        # Bonuses
        # +50% Wizard Production
        definitions.THIEF_LOSSES_FACTOR *= 0.50  # -50% Thieves lost on Thievery Operations
        definitions.TPA_MODIFIER *= 1.25 # +25% Thievery Efficiency (TPA)
        definitions.WPA_MODIFIER *= 1.1 # +10% Magic Efficiency (WPA)

        # Access to Spells
        # Access to Meteor Showers, Greater Arson
    end

    # 
    # The Mystic
    # +75% Guilds Effectiveness
    # +1 Mana Recovery per Tick
    # Access to Magic Ward, Meteor Showers, Mage’s Fury, Mind Focus, Chastity, Fools Gold,
    # +25% Channeling Science Efficiency
    # 
    # Starts with +800 Wizards
    # 
    # Unique Passive: Righteousness Fury:Gains access to the Spells Righteous Aggressor and Righteous Defender
    # These are 1-2 hour duration spells that were first introduced in genesis and when cast and active have the following effects:
    # Righteous aggressor will convert offensive specialist to elites on successful attack similar how offensive conversions have worked in past ages.
    # Righteous defender will convert defensive specialist to elites when you are attacked at roughly the same rate offensive conversions occur.
    #     
    if target.PERS == PERS_OPTS.MYSTIC
        # Unique Passive: Righteousness Fury

        # Bonuses
        definitions.LAND_EFFECT_MULTIPLIERS[BUILDING_OPTS.GUILDS] = get(definitions.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.GUILDS, 1) * 1.75  # +75% Guilds Effectiveness
        # +1 Mana Recovery per Tick
        definitions.SCIENCE_BONUS_MODS[SCIENCE_OPTS.CHANNELING] = 1.25  # +25% Channeling Science Efficiency

        # Access to Spells
        append!(definitions.SELF_SPELLS, [SELF_SPELLS.MAGES_FURY, SELF_SPELLS.MIND_FOCUS]) # Access to Magic Ward, Meteor Showers, Mage’s Fury, Mind Focus, Chastity, Fools Gold,
    end

    # 
    # The Rogue
    # +80% Thieves' Dens Effectiveness
    # +1 Stealth Recovery per Tick
    # Access to All Thievery Operations
    # +25% Crime Science Efficiency
    # 
    # Starts with +800 Thieves
    # 
    # Unique Passive: Shadows in the Night: Rogues deal +25% Sabotage damage if the operation is performed when a Rogue has 50%
    # or more Stealth
    #     
    if target.PERS == PERS_OPTS.ROGUE
        # Unique Passive: Shadows in the Night

        # Bonuses
        definitions.LAND_EFFECT_MULTIPLIERS[BUILDING_OPTS.THIEVES_DENS] = get(definitions.LAND_EFFECT_MULTIPLIERS, BUILDING_OPTS.THIEVES_DENS, 1) * 1.8  # +80% Thieves' Dens Effectiveness
        # +1 Stealth Recovery per Tick
        definitions.SCIENCE_BONUS_MODS[SCIENCE_OPTS.CRIME] = 1.25  # +25% Crime Science Efficiency

        # Access to Spells
        # Access to All Thievery Operations
    end

    # 
    # The Tactician
    # -15% Attack Time
    # No Thieves lost on intel
    # +25% Credits gained in Combat
    # +25% Siege Science Efficiency
    # 
    # Starts with +800 Soldiers and +800 Specialist Credits
    # 
    # Unique Passive: Dragons Wrath When attacking, 10% of your raw offense will also deal damage to the dragon as well as attack your target
    # (i.e. if you attack someone with 10,000 soldiers the dragon will also take 3000 points of damage) .
    #     
    if target.PERS == PERS_OPTS.TACTICIAN
        # Unique Passive: Dragons Wrath

        # Bonuses
        definitions.ATTACK_TIME_MODIFIER *= 0.85  # -15% Attack Time
        # +25% Credits gained in Combat
        definitions.SCIENCE_BONUS_MODS[SCIENCE_OPTS.SIEGE] = 1.25  # +25% Siege Science Efficiency

        # No Thieves lost on intel - Not directly implementable

        # Access to Spells
        # No new spells
    end

    # 
    # The War Hero
    # +30% Honor Gains
    # - 30% Honor Losses
    # +5% Military Efficiency in War
    # Converts Specialist to Elites upon Successful Traditional Marches
    # 
    # Starts with +800 Soldiers and +800 Specialist Credits
    # 
    # Unique Active: War Trophies: Once activated, War Hero grants double honour bonuses for 12 ticks. Cooldown: 24 Utopian Days.
    #     
    if target.PERS == PERS_OPTS.WARHERO
        # Unique Active: War Trophies

        # Bonuses
        # +30% Honor Gains
        # -30% Honor Losses
        # +5% Military Efficiency in War
        definitions.OME_MODIFIER *= 1.05
        definitions.DME_MODIFIER *= 1.05

        # Converts Specialist to Elites upon Successful Traditional Marches

        # Access to Spells
        # No new spells
    end

    # 
    # The Warrior
    # +1 General
    # +4 Mercenary & Prisoner Strength
    # -50% Mercenary Cost
    # +25% Tactics Science Efficiency
    # 
    # Starts with +800 Soldiers and +800 Specialist Credits
    # 
    # Unique activated: Indomitable Spirit: Once activated Warriors gain 5% Military Efficiency for 6 ticks. Cooldown: 24 Utopian Days.
    #     
    if target.PERS == PERS_OPTS.WARRIOR
        # Unique activated: Indomitable Spirit

        # Bonuses
        # +1 General
        definitions.PRISONER_OFF += 4  # +4 Mercenary & Prisoner Strength
        definitions.SCIENCE_BONUS_MODS[SCIENCE_OPTS.TACTICS] = 1.25  # +25% Tactics Science Efficiency

        # -50% Mercenary Cost
    end
    # Spell stuf

    spells_to_take_into_account = filter(x -> !(x in target.SELF_SPELLS_EXCLUDE),  definitions.SELF_SPELLS)

    if SELF_SPELLS.CLEAR_SIGHT in spells_to_take_into_account
        definitions.OP_DAMAGE_RECEIVED_MULTIPLIER *= 1.25
    end

    if SELF_SPELLS.TOWN_WATCH in spells_to_take_into_account
        # meh, let's ignore for now
    end

    if SELF_SPELLS.ILLUMINATE_SHADOWS in spells_to_take_into_account
        definitions.OP_DAMAGE_RECEIVED_MULTIPLIER *= 1.2
    end

    if SELF_SPELLS.QUICKFEET in spells_to_take_into_account
        definitions.ATTACK_TIME_MODIFIER *= 0.85
    end

    if SELF_SPELLS.REVELATION in spells_to_take_into_account
        #definitions.TOTAL_SCIENCE_MULTIPLIER *= 1.2
        definitions.SCIENCE_GENERATION_MODS.scientist_gen_mod *= 1.25
    end

    if SELF_SPELLS.FOUNTAIN_OF_KNOWLEDGE in spells_to_take_into_account
        #definitions.TOTAL_SCIENCE_MULTIPLIER *= 1.05
        definitions.SCIENCE_GENERATION_MODS.science_gen_mod *= 1.1
    end

    if SELF_SPELLS.SCIENTIFIC_INSIGHTS in spells_to_take_into_account
        definitions.SCIENCE_MULTIPLIER *= 1.1
    end

    if SELF_SPELLS.FANATICISM in spells_to_take_into_account
        definitions.OME_MODIFIER *= 1.05
    end

    if SELF_SPELLS.BLOODLUST in spells_to_take_into_account
        definitions.OME_MODIFIER *= 1.1
    end

    if SELF_SPELLS.INVISIBILITY in spells_to_take_into_account
        definitions.TPA_MODIFIER *= 1.1
        definitions.THIEF_LOSSES_FACTOR *= 0.8
    end

    if SELF_SPELLS.MAGES_FURY in spells_to_take_into_account
        definitions.WPA_MODIFIER *= 1.25
    end

    if SELF_SPELLS.GHOST_WORKERS in spells_to_take_into_account
        definitions.GHOST_WORKER_EFFECT *= 0.75
    end

    if SELF_SPELLS.MINERS_MYSTIQUE in spells_to_take_into_account
        definitions.MINERS_MYSTIQUE_EFFECT = 0.3
    end

    if SELF_SPELLS.GREATER_PROTECTION in spells_to_take_into_account
        definitions.DME_MODIFIER *= 1.1
    end

    if SELF_SPELLS.MINOR_PROTECTION in spells_to_take_into_account
        definitions.DME_MODIFIER *= 1.1
    end

    if SELF_SPELLS.SALVATION in spells_to_take_into_account
        definitions.ATTACK_LOSSES_MODIFIER *= 0.9
    end

    if SELF_SPELLS.MIST in spells_to_take_into_account
        definitions.ATTACK_DAMAGE_MODIFIER *= 0.9
    end

    if SELF_SPELLS.GUILE in spells_to_take_into_account
        definitions.MAGE_DAMAGE_MULTIPLIER *= 1.1
        definitions.THIEF_DAMAGE_MULTIPLIER *= 1.1
    end

    return definitions
end

end
