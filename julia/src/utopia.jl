
include("data-structs.jl")
include("game-definitions.jl")
include("science.jl")


module Utopia
using JuMP, Cbc, Ipopt, Juniper, DataFrames, PrettyTables, Alpine, NamedArrays

using ..DataStructs
using ..DataStructs.RACE_OPTS
using ..DataStructs.PERS_OPTS
using ..DataStructs.HONOR_OPTS
using ..DataStructs.SELF_SPELLS
using ..GameDefinitions
using ..Science

export optimizeProvince

function optimizeProvince(target::OptimizationTarget, silent::Bool=false)

    definitions = generate_definitions(target)

    #old stuff, must be updated to actually calculate science
    target.RUNES *= definitions.RUNES_WANTED_MOD

    SELECTED_HONOR = deepcopy(HONOR_BONUSSES[target.HONOR])

    TOTAL_SCIENCE_BOOKS = target.SCIENCE

    if TOTAL_SCIENCE_BOOKS === nothing
        science_state = target.SCIENCE_STATE
        science_mods = definitions.SCIENCE_GENERATION_MODS
        process_science(science_state, science_mods, target.SCIENCE_DAYS)
        TOTAL_SCIENCE_BOOKS = total_books(science_state)
    end

    #target.SCIENCE *= definitions.TOTAL_SCIENCE_MULTIPLIER

    optimizer = Juniper.Optimizer

    nl_solver = optimizer_with_attributes(Ipopt.Optimizer, "print_level" => 0, "print_user_options" => if silent
        "yes"
    else
        "no"
    end)
    mip_solver = optimizer_with_attributes(Cbc.Optimizer, "logLevel" => 0)
    model = Model(
        optimizer_with_attributes(optimizer, "nl_solver" => nl_solver, "mip_solver" => mip_solver, "branch_strategy" => :MostInfeasible, "feasibility_pump" => true);
        add_bridges=false
    )
 
    # model = JuMP.Model(COSMO.Optimizer);
    #alpine = optimizer_with_attributes(Alpine.Optimizer,
    #"nlp_solver" => nl_solver,
    #"mip_solver" => mip_solver)


    #model = Model(alpine)
    #model = Model(optimizer_with_attributes(optimizer, "nlp_solver" => nl_solver, "mip_solver" => mip_solver))

    merged_building_data = deepcopy(BUILDING_DATA)
    for (building_idx, pair) in enumerate(definitions.WICKED_BUILDING_DATA)
        building = pair.first
        for (k, effect) in pair.second

            println(building, ": ", k, ": ", effect)

            if haskey(merged_building_data, building)
                println("haskey")
                push!(merged_building_data[building], k => effect)
            else
                merged_building_data[building] = [k => effect]
            end

        end
    end
    #model = Model(EAGO.Optimizer)

    building_keys = collect(keys(merged_building_data))
    building_keys_lookup = Dict([v => idx for (idx, v) in enumerate(building_keys)])
    building_count = length(building_keys)
    building_effects_by_effect = Dict()

    #building_effects_by_effect = Dict([
    #    effect.first => merge(effect.second, (; building = building)) for (building, effects) in deepcopy(merged_building_data) for effect in effects
    #])
    for (building, effects) in deepcopy(merged_building_data)
        for effect in effects
            effect_key = effect.first
            if haskey(building_effects_by_effect, effect_key)
                push!(building_effects_by_effect[effect_key][:buildings], building)
            else
                building_effects_by_effect[effect_key] = merge(effect.second, (; buildings=[building]))
            end
        end
    end


    @variable(model, 0 <= buildings[1:building_count] <= 50, start = 5)

    # population stuff

    p_soldiers = 0
    p_ospecs = 1
    p_dspecs = 2
    p_elites_off = 3
    p_elites_def = 4
    p_wizards = 5
    p_thieves = 6

    population_names = ["soldiers", "ospec", "dspec", "elites_off", "elites_def", "wizards", "thieves"]

    @variable(model, population[0:6] >= 0, start = 0)


    s_effects = deepcopy(SCIENCE_DATA.cats)

    s_effects[SCIENCE_OPTS.CRIME] = merge(s_effects[SCIENCE_OPTS.CRIME], (; mul=s_effects[SCIENCE_OPTS.CRIME].mul * definitions.CRIME_SCIENCE_MULTIPLIER))
    s_effects[SCIENCE_OPTS.CHANNELING] = merge(s_effects[SCIENCE_OPTS.CHANNELING], (; mul=s_effects[SCIENCE_OPTS.CHANNELING].mul * definitions.CHANNELING_SCIENCE_MULTIPLIER))
    s_effects[SCIENCE_OPTS.CUNNING] = merge(s_effects[SCIENCE_OPTS.CUNNING], (; mul=s_effects[SCIENCE_OPTS.CUNNING].mul * definitions.CUNNING_SCIENCE_MULTIPLIER))

    for (k, effect) in definitions.SCIENCE_BONUS_MODS
        s_effects[k] = merge(s_effects[k], (; mul=s_effects[k].mul * effect))
    end

    books_keys = collect(keys(s_effects))
    books_keys_lookup = Dict([v => idx for (idx, v) in enumerate(books_keys)])

    @variable(model, books[1:length(books_keys)] >= 1, start = TOTAL_SCIENCE_BOOKS / 14)

    building_effects = Dict{Symbol,NonlinearExpression}()

    # ==
    # Split Building effects in two, as some non-BE affected buildings (building effect -> science -> BE) affect BE
    # ==

    # merge BUILDING_DATA and definitions.WICKED_BUILDING_DATA to create a new DataStructures merged_building_data


    for (building_idx, pair) in enumerate(merged_building_data)
        for (k, effect) in pair.second
            if !effect[:be]
                if :flat_base in keys(effect)
                    mul = 1
                    if :flat_mul in keys(effect)
                        mul *= effect.flat_mul(definitions)
                    end
                    if :flat_cap_mul in keys(effect)
                        mul *= effect.flat_cap_mul(definitions)
                    end
                    if haskey(building_effects, k)
                        old_expr = building_effects[k]
                        building_effects[k] = @NLexpression(model, old_expr + (mul * effect.flat_base * buildings[building_idx] * target.ACRES / 100))
                    else
                        push!(building_effects, k => @NLexpression(model, mul * effect.flat_base * buildings[building_idx] * target.ACRES / 100))
                    end
                else
                    # Base Effect * BE * MIN(50%, % of building) * (100% - MIN(50%, % of building))
                    if :mul in keys(effect)
                        mul = effect.mul(definitions)
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + (mul * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        else
                            push!(building_effects, k => @NLexpression(model, mul * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        end
                    elseif :land_mul in keys(effect)
                        mul = effect.land_mul(definitions)
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + (mul * effect.base * buildings[building_idx] * (100 - mul * buildings[building_idx]) / 10000))
                        else
                            push!(building_effects, k => @NLexpression(model, mul * effect.base * buildings[building_idx] * (100 - mul * buildings[building_idx]) / 10000))
                        end
                    else
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + (effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        else
                            push!(building_effects, k => @NLexpression(model, effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        end
                    end
                end
            end
        end
    end

    @NLconstraint(model, sum(x for x in buildings) == 100)
    @NLconstraint(model, sum(x for x in books) <= TOTAL_SCIENCE_BOOKS)



    science_effects = Dict(
        k => @NLexpression(
            model,
            (books[books_keys_lookup[k]]^(1 / 2.125)) * v.mul / 100 * (1 + building_effects[:science_effect_increase]) * definitions.SCIENCE_MULTIPLIER
            # 1/100 * books[v.id]^0.470588 * (1 + building_effects[:science_effect_increase]) * v.mul
        )
        for (k, v) in s_effects)

    #science_effects = Dict(
    #k => @NLexpression(
    #    model,
    #    1 / 100 * (1 + building_effects[:science_effect_increase]) * definitions.SCIENCE_MULTIPLIER
    #    # 1/100 * books[v.id]^0.470588 * (1 + building_effects[:science_effect_increase]) * v.mul
    #)
    #for (k, v) in s_effects)

    #for s in SCIENCE_OPTS.SCIENCE
    #    if s in keys(target.SCIENCE_BASE)
    #        @NLconstraint(model,  books[books_keys_lookup[s]] >= target.SCIENCE_BASE[s])
    #    else
    #        @NLconstraint(model,  books[books_keys_lookup[s]] > 0)
    #    end
    #end

    for (k, v) in target.SCIENCE_BASE
        @NLconstraint(model, books[books_keys_lookup[k]] >= v)
    end

    pop_space = @NLexpression(model, ((1 + science_effects[SCIENCE_OPTS.HOUSING]) * definitions.POP_MULTIPLIER * (((SELECTED_HONOR.pop - 1) * definitions.HONOR_EFFECTS) + 1) * ((buildings[building_keys_lookup[BUILDING_OPTS.HOMES]] * target.ACRES / 100 * (10 + definitions.HOME_POP_INCREASE) * definitions.HOME_POP_MULTIPLIER) + (sum(x for x in buildings) * target.ACRES / 100 * 25))))
    jobs = @NLexpression(model, sum(x for x in buildings) * target.ACRES / 100 * 25 - buildings[building_keys_lookup[BUILDING_OPTS.HOMES]] * target.ACRES / 100 * 25)
    peasants = @NLexpression(model, pop_space - sum(x for x in population))
    @NLconstraint(model, peasants >= 0)
    # jobs can't be over 100% performed 
    pct_jobs_performed = @NLexpression(model, (3 * peasants + building_effects[:prisoner_holding] / 2) / (2 * (jobs * definitions.GHOST_WORKER_EFFECT)))
    # we assume no unemployed peasants, should be good in practice, having unemplayoed peasants is far from optimal
    @NLconstraint(model, pct_jobs_performed <= 1.0)
    # Building Efficiency       =  (0.5 * (1 + % Jobs Performed)) * Race * Personality * Tools Science * Dragon * Blizzard
    be = @NLexpression(model, (0.5 * (1.0 + pct_jobs_performed)) * (1 + science_effects[SCIENCE_OPTS.TOOLS]) * definitions.BE_MULTIPLIER)

    for (building_idx, pair) in enumerate(merged_building_data)
        for (k, effect) in pair.second
            if effect[:be]
                if :flat_base in keys(effect)
                    mul = 1
                    if :flat_mul in keys(effect)
                        mul *= effect.flat_mul(definitions)
                    end
                    if :flat_cap_mul in keys(effect)
                        mul *= effect.flat_cap_mul(definitions)
                    end
                    if haskey(building_effects, k)
                        old_expr = building_effects[k]
                        building_effects[k] = @NLexpression(model, old_expr + (be * mul * effect.flat_base * buildings[building_idx] * target.ACRES / 100))
                    else
                        push!(building_effects, k => @NLexpression(model, be * mul * effect.flat_base * buildings[building_idx] * target.ACRES / 100))
                    end
                else
                    # Base Effect * BE * MIN(50%, % of building) * (100% - MIN(50%, % of building))
                    if :mul in keys(effect)
                        mul = effect.mul(definitions)
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + (mul * be * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        else
                            push!(building_effects, k => @NLexpression(model, mul * be * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        end
                        @NLconstraint(model, building_effects[k] <= 0.25 * effect.base)
                    elseif :land_mul in keys(effect)
                        mul = effect.land_mul(definitions)
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + (mul * be * effect.base * buildings[building_idx] * (100 - mul * buildings[building_idx]) / 10000))
                        else
                            push!(building_effects, k => @NLexpression(model, mul * be * effect.base * buildings[building_idx] * (100 - mul * buildings[building_idx]) / 10000))
                        end
                        @NLconstraint(model, building_effects[k] <= 0.25 * effect.base)
                        @NLconstraint(model, buildings[building_idx] * mul <= 50)
                    else
                        if haskey(building_effects, k)
                            old_expr = building_effects[k]
                            building_effects[k] = @NLexpression(model, old_expr + (be * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        else
                            push!(building_effects, k => @NLexpression(model, be * effect.base * buildings[building_idx] * (100 - buildings[building_idx]) / 10000))
                        end
                    end
                end
            end
        end
    end

    """
    for (effect, expression) in building_effects
        println(effect, ": ", expression)
    end

    return
    """

    # Raw Income = (3 * Employed Peasants) + (1 * Unemployed Peasants) + (0.625 * Prisoners) + (Banks * 25 * BE)
    # Modified Income = Raw Income * Plague * Riots * Bank % Bonus * Income Sci * Honor Income Mod * Race Mod * Personality Mod * Dragon * War Stance * Ritual
    income = @NLexpression(model, ((3 + definitions.MINERS_MYSTIQUE_EFFECT) * peasants + building_effects[:gc_production]) * (1 + building_effects[:income_increase]) * (1 + science_effects[SCIENCE_OPTS.ALCHEMY]) * definitions.INCOME_MULTIPLIER * (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1))

    @variable(model, 20 <= wages <= 200, start = 100)

    # (((Def specs + Off specs )*0.5) + Elites * 0.7) * Wage Rate * Armouries Bonus * Race Mod * Personality Mod * max(Inspire Army , Hero's Inspiration) * Greed * Bookkeeping Science Effect
    expenses = @NLexpression(model,
        ((population[p_ospecs] + population[p_dspecs]) * 0.5 + (population[p_elites_off] + population[p_elites_def]) * 0.75)
        * (1 - building_effects[:wages_decrease]) * (1 - science_effects[SCIENCE_OPTS.BOOKKEEPING])
        * (wages / 100) * definitions.WAGES_MULTIPLIER
    )

    base_me = @NLexpression(model, 33 + 67 * (wages / 100)^0.25)

    # OME = (Base Military Efficiency + Training Ground Bonus + Honor Bonus) * Science Bonus * Race Bonus * Personality Bonus * Fanaticism * Bloodlust * Onslaught Ritual
    ome = @NLexpression(model, (base_me / 100 + building_effects[:ome_increase]) * (1 + science_effects[SCIENCE_OPTS.TACTICS]) * definitions.OME_MODIFIER * (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1))
    dme = @NLexpression(model, (base_me / 100 + building_effects[:dme_increase]) * (1 + science_effects[SCIENCE_OPTS.STRATEGY]) * definitions.DME_MODIFIER * (((SELECTED_HONOR.me - 1) * definitions.HONOR_EFFECTS) + 1))

    prisoners = @NLexpression(model, building_effects[:prisoner_holding])

    if definitions.PRISONER_PER_ACRE > 0
        prisoners = @NLexpression(model, (population[p_ospecs] + population[p_elites_off]) / 5)
        @NLconstraint(model, prisoners <= target.ACRES * definitions.PRISONER_PER_ACRE)
    end

    if definitions.HORSE_PER_ACRE > 0
        horses = @NLexpression(model, population[p_ospecs] + population[p_elites_off])
    else
        horses = @NLexpression(model, building_effects[:horse_holding])
        @NLconstraint(model, horses <= population[p_ospecs] + population[p_elites_off])
    end


    offense = @NLexpression(model, (population[p_ospecs] * definitions.OSPEC_OFF + population[p_elites_off] * definitions.ELITE_OFF +
                                    prisoners * definitions.PRISONER_OFF + horses * definitions.HORSE_OFF) * ome)


    defense = @NLexpression(model, (population[p_dspecs] * definitions.DSPEC_DEF + population[p_elites_def] * definitions.ELITE_DEF) * dme)
    turtle_defense = @NLexpression(model, (population[p_dspecs] * definitions.DSPEC_DEF + (population[p_elites_off] + population[p_elites_def]) * definitions.ELITE_DEF) * dme)

    # horse are only good when they have a rider
    #@NLconstraint(model, horses <= population[p_ospecs] + population[p_elites_off])

    #prisoners are only offense when they ar 1/5 of military_losses_decrease
    @NLconstraint(model, building_effects[:prisoner_holding] <= (population[p_ospecs] + population[p_elites_off]) / 5)


    tpa = @NLexpression(model, definitions.TPA_MODIFIER * (population[p_thieves] * (1 + building_effects[:tpa_increase]) * (1 + science_effects[SCIENCE_OPTS.CRIME])) * (((SELECTED_HONOR.tpa - 1) * definitions.HONOR_EFFECTS) + 1) / target.ACRES)
    wpa = @NLexpression(model, definitions.WPA_MODIFIER * (population[p_wizards] * (1 + science_effects[SCIENCE_OPTS.CHANNELING])) * (((SELECTED_HONOR.wpa - 1) * definitions.HONOR_EFFECTS) + 1) / target.ACRES)

    food_needed = @NLexpression(model, (peasants + sum(x for x in population)) * 0.25 * definitions.FOOD_NEEDED_MULTIPLIER)
    runes_produced = @NLexpression(model, (
                                              definitions.ELF_GUILD_RUNES * buildings[building_keys_lookup[BUILDING_OPTS.GUILDS]] * target.ACRES
                                              + definitions.DRYAD_LAND_FOOD_RUNES_MULTIPLIER * target.ACRES * 0.5
                                              + building_effects[:rune_production]
                                          ) * (1 + science_effects[SCIENCE_OPTS.PRODUCTION]) * definitions.RUNE_PROD_MULTIPLIER)


    nw = @NLexpression(model, target.ACRES * 60 + peasants * 0.25 + population[p_ospecs] * definitions.OSPEC_OFF * 0.4 + population[p_dspecs] * definitions.DSPEC_DEF * 0.5
                              + (population[p_elites_off] + population[p_elites_def]) * definitions.ELITE_NW
                              + horses * definitions.HORSE_NW_BASE
                              + building_effects[:prisoner_holding] * definitions.PRISONER_OFF * 0.2
                              + population[p_thieves] * 5 + population[p_wizards] * 7 + target.ACRES * 0.000007 * TOTAL_SCIENCE_BOOKS)

    # don't starve
    @NLconstraint(model, food_needed <= (building_effects[:food_production] + definitions.LAND_GENERATES_FOOD * target.ACRES + definitions.DRYAD_LAND_FOOD_RUNES_MULTIPLIER * target.ACRES * 3) * (1 + science_effects[SCIENCE_OPTS.PRODUCTION]))

    if target.OPT_RAW_TPA_AND_WPA
        @NLconstraint(model, population[p_thieves] / target.ACRES >= target.MOD_TPA)
        @NLconstraint(model, population[p_wizards] / target.ACRES >= target.MOD_WPA)
    else
        # WPA/TPA
        @NLconstraint(model, tpa >= target.MOD_TPA)
        @NLconstraint(model, wpa >= target.MOD_WPA)
    end

    @NLconstraint(model, income - expenses >= target.INCOME)
    @NLconstraint(model, runes_produced >= target.RUNES)

    # BELOW ARE SITUATIONAL CONSTRAINS, ADJUST AS YOU LIKE
    attack_time_factor = @NLexpression(model, definitions.ATTACK_TIME_MODIFIER * (1 - building_effects[:attack_time_decrease]))
    losses_factor = @NLexpression(model, definitions.ATTACK_LOSSES_MODIFIER * (1 - building_effects[:military_losses_decrease]) * (1 - science_effects[SCIENCE_OPTS.RESILIENCE]))
    landloss_factor = @NLexpression(model, definitions.ATTACK_DAMAGE_MODIFIER * (1 - building_effects[:land_loss_decrease]))
    thief_op_damage_receive_factor = @NLexpression(model, definitions.OP_DAMAGE_RECEIVED_MULTIPLIER * (1 - building_effects[:thief_damage_decrease]) * (1 - science_effects[SCIENCE_OPTS.SHIELDING]))
    combat_gains_multiplier = @NLexpression(model, definitions.COMBAT_GAINS_MULTIPLIER * (1 + science_effects[SCIENCE_OPTS.SIEGE]))
    thief_damage_multiplier = @NLexpression(model, definitions.THIEF_DAMAGE_MULTIPLIER * (1 + science_effects[SCIENCE_OPTS.CUNNING]))
    mage_damage_multiplier = @NLexpression(model, definitions.MAGE_DAMAGE_MULTIPLIER * (1 + science_effects[SCIENCE_OPTS.SORCERY]))
    thief_losses_factor = @NLexpression(model, definitions.THIEF_LOSSES_FACTOR * (1 - building_effects[:thief_losses_decrease]) * (1 - science_effects[SCIENCE_OPTS.FINESSE]))

    # Building  preferences
    for (building, building_target) in target.BUILDING_TARGETS
        land_effect_multiplier = 1
        if :as_effective in keys(building_target) && building_target[:as_effective]
            if building in keys(definitions.LAND_EFFECT_MULTIPLIERS)
                land_effect_multiplier = definitions.LAND_EFFECT_MULTIPLIERS[building]
            end
        end
        if :min in keys(building_target)
            @NLconstraint(model, buildings[building_keys_lookup[building]] * land_effect_multiplier >= building_target[:min])
        end
        if :max in keys(building_target)
            @NLconstraint(model, buildings[building_keys_lookup[building]] * land_effect_multiplier <= building_target[:max])
        end
        if :eq in keys(building_target)
            @NLconstraint(model, buildings[building_keys_lookup[building]] * land_effect_multiplier == building_target[:eq])
        end
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.MAGE_DAMAGE_MULTIPLIER)
        @NLconstraint(model, mage_damage_multiplier >= target.TARGETS[EXTRA_TARGETS.MAGE_DAMAGE_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.THIEF_DAMAGE_MULTIPLIER)
        @NLconstraint(model, thief_damage_multiplier >= target.TARGETS[EXTRA_TARGETS.THIEF_DAMAGE_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.COMBAT_GAINS_MULTIPLIER)
        @NLconstraint(model, combat_gains_multiplier >= target.TARGETS[EXTRA_TARGETS.COMBAT_GAINS_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.ATTACK_TIME_MULTIPLIER)
        @NLconstraint(model, attack_time_factor <= target.TARGETS[EXTRA_TARGETS.ATTACK_TIME_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.LOSSES_MULTIPLIER)
        @NLconstraint(model, losses_factor <= target.TARGETS[EXTRA_TARGETS.LOSSES_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.LANDLOSS_MULTIPLIER)
        @NLconstraint(model, landloss_factor <= target.TARGETS[EXTRA_TARGETS.LANDLOSS_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER)
        @NLconstraint(model, thief_op_damage_receive_factor <= target.TARGETS[EXTRA_TARGETS.THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.THIEF_LOSSES_MULTIPLIER)
        @NLconstraint(model, thief_losses_factor <= target.TARGETS[EXTRA_TARGETS.THIEF_LOSSES_MULTIPLIER])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.MAX_PRISONERS)
        if (definitions.PRISONER_PER_ACRE == 0)
            @NLconstraint(model, building_effects[:prisoner_holding] <= target.TARGETS[EXTRA_TARGETS.MAX_PRISONERS])
        end
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.MAX_ELITE_OSPEC_RATIO)
        @NLconstraint(model, (population[p_elites_def] + population[p_elites_off]) / population[p_ospecs] <= target.TARGETS[EXTRA_TARGETS.MAX_ELITE_OSPEC_RATIO])
    end

    if haskey(target.TARGETS, EXTRA_TARGETS.MAX_ELITE_DSPEC_RATIO)
        @NLconstraint(model, (population[p_elites_def] + population[p_elites_off]) / population[p_dspecs] <= target.TARGETS[EXTRA_TARGETS.MAX_ELITE_DSPEC_RATIO])
    end





    if (target.OPA !== nothing)
        @NLconstraint(model, offense >= target.ACRES * target.OPA)
        if target.OPT_FOR_TURTLE_DEF
            defense = turtle_defense
        end
        if target.OPT_FOR_NW
            @NLobjective(model, Max, defense / nw)
        else
            @NLobjective(model, Max, defense)
        end
    end

    if (target.DPA !== nothing)
        @NLconstraint(model, defense >= target.ACRES * target.DPA)
        if target.OPT_FOR_NW
            @NLobjective(model, Max, offense / nw)
        else
            @NLobjective(model, Max, offense)
        end
    end

    if (target.OPA_DPA_RATIO !== nothing)
        if target.OPT_FOR_TURTLE_DEF
            @NLconstraint(model, defense * target.OPA_DPA_RATIO <= offense)
            opt_for = turtle_defense
        else
            @NLconstraint(model, defense * target.OPA_DPA_RATIO >= offense)
            opt_for = offense
        end
        if target.OPT_FOR_NW
            @NLobjective(model, Max, opt_for / nw)
        else
            @NLobjective(model, Max, opt_for)
        end
    end
    # @NLobjective(model, Max, offense)

    #print(model)
    JuMP.optimize!(model)
    if (!silent)
        println("** Optimal objective function value = ", JuMP.objective_value(model))
        # println("|| $(JuMP.value(be)) = @NLexpression(model,  ((0.5 * (1.0 + $(JuMP.value(pct_jobs_performed))) * $(JuMP.value(science_effects[SCIENCE_OPTS.TOOLS])))")

        #println("** wages = ", JuMP.value(wages), " me = ", JuMP.value(base_me))
        #println("** peasants = ", JuMP.value(peasants))
        #println("** pop_space = ", JuMP.value(pop_space))
        #println("** income = ", JuMP.value(income), " exp = ", JuMP.value(expenses), " = ", JuMP.value(income) - JuMP.value(expenses))
        #println("** Optimal pct_jobs_performed = ", JuMP.value(pct_jobs_performed))
        # println("** Optimal building = ", JuMP.value.(buildings))
        # println("** Optimal books = ", JuMP.value.(books))
        # println("** Optimal pop = ", JuMP.value.(population))
        #for (k, v) in science_effects
        #    println("** Science ", k, " (", JuMP.value(books[s_effects[k].id]), ") ", JuMP.value(v))
        #end
        #for (k, v) in building_effects
        #    building = buildings[b_effects[k].id]
        #    println("** Building ", k, " (", JuMP.value(building), ") ", JuMP.value(v))
        #end
        println(JuMP.termination_status(model))
    end



    df_scy = DataFrame(Science_Effect=SCIENCE_OPTS.SCIENCE[], Books=Number[], Effect=Number[], Group=SCIENCE_GROUP_OPTS.SCIENCE_GROUP[])
    for (k, v) in science_effects
        ct = JuMP.value(books[books_keys_lookup[k]])
        effect = JuMP.value(v)
        if ct > 10
            push!(df_scy, (k, ct, effect * 100, s_effects[k].group))
        end
        # println("** Building ", k, " (",  JuMP.value(building) ,") ", JuMP.value(v))
    end
    sort!(df_scy, [:Group, :Science_Effect])

    if (!silent)
        pretty_table(df_scy, crop=:none, formatters=ft_printf("%d"))
    end

    groups = groupby(df_scy, :Group)

    def_scy_groups = combine(groups, :Books => sum, nrow, renamecols=false)

    if (!silent)
        pretty_table(def_scy_groups, crop=:none, formatters=ft_printf("%d"))
    end

    df_build = DataFrame(Building_Effect=Symbol[], Building_Name=BUILDING_OPTS.BUILDING[], Building_Pct=Number[], Effect=Number[])
    for (k, v) in building_effects
        building_keys = building_effects_by_effect[k][:buildings]

        for building_key in building_keys

            building = buildings[building_keys_lookup[building_key]]
            effect = JuMP.value(v)
            if effect > 0.01
                push!(df_build, (k, building_key, JuMP.value(building), effect > 2 ? effect : effect * 100))
            end
        end
        # println("** Building ", k, " (",  JuMP.value(building) ,") ", JuMP.value(v))
    end
    sort!(df_build, [:Building_Name, :Building_Effect])

    if (!silent)
        pretty_table(df_build, crop=:none, formatters=ft_printf("%.0f"))
    end

    df_pop = DataFrame(Troop=String[], Amount=Number[])
    push!(df_pop, ("peasants", JuMP.value(peasants)))

    for (i, v) in enumerate(population)
        push!(df_pop, (population_names[i], JuMP.value(v)))
    end

    push!(df_pop, ("horses", JuMP.value(horses)))
    if definitions.PRISONER_PER_ACRE > 0
        push!(df_pop, ("prisoners", JuMP.value(prisoners)))
    end
    if (!silent)
        pretty_table(df_pop, crop=:none, formatters=ft_printf("%d"))
    end

    df_core = DataFrame(Core=String[], Amount=Number[])
    push!(df_core, ("NW", JuMP.value(nw)))
    push!(df_core, ("OPNW", 100 * JuMP.value(offense) / JuMP.value(nw)))
    push!(df_core, ("DPNW", 100 * JuMP.value(defense) / JuMP.value(nw)))
    push!(df_core, ("ACRES", target.ACRES))
    push!(df_core, ("OPA", JuMP.value(offense) / target.ACRES))
    push!(df_core, ("DPA", JuMP.value(defense) / target.ACRES))
    push!(df_core, ("TURTLE DPA", JuMP.value(turtle_defense) / target.ACRES))
    push!(df_core, ("TPA (raw)", JuMP.value(population[p_thieves]) / target.ACRES))
    push!(df_core, ("WPA (raw)", JuMP.value(population[p_wizards]) / target.ACRES))
    push!(df_core, ("TPA (mod)", JuMP.value(tpa)))
    push!(df_core, ("WPA (mod)", JuMP.value(wpa)))
    push!(df_core, ("OME", 100 * JuMP.value(ome)))
    push!(df_core, ("DME", 100 * JuMP.value(dme)))
    push!(df_core, ("Total Books", TOTAL_SCIENCE_BOOKS))
    push!(df_core, ("Income", JuMP.value(income)))
    push!(df_core, ("Wage Rate", JuMP.value(wages)))
    push!(df_core, ("Expenses", JuMP.value(expenses)))
    push!(df_core, ("BE", 100 * JuMP.value(be)))
    push!(df_core, ("AttackTime", 100 * JuMP.value(attack_time_factor)))
    push!(df_core, ("Thief Losses", 100 * JuMP.value(thief_losses_factor)))
    push!(df_core, ("Thief Damage Multiplier", 100 * JuMP.value(thief_damage_multiplier)))


    if (!silent)
        pretty_table(df_core, crop=:none, formatters=ft_printf("%.d"))
    end

    frames = (df_scy, def_scy_groups, df_build, df_pop, df_core)
    values = (target.ACRES, JuMP.value(nw), JuMP.value(offense), JuMP.value(defense), JuMP.value(turtle_defense))

    return (frames, values, definitions)
end

end