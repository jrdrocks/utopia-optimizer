module DataStructs

using Parameters

export OptimizationTarget, ExtraOptimizationTarget, OptimizationDefinitions, RACE_OPTS, PERS_OPTS, HONOR_OPTS, POPULATION_OPTS, BUILDING_OPTS, SCIENCE_OPTS, SCIENCE_GROUP_OPTS, SELF_SPELLS, EXTRA_TARGETS, Scientist, ScienceState, ScienceGenMods
 
baremodule RACE_OPTS
using Base: @enum
@enum RACE AVIAN DARKELF DWARF DRYAD ELF FAERY GNOME HALFLING HUMAN ORC UNDEAD
end

baremodule PERS_OPTS
using Base: @enum
@enum PERS NONE ARTISAN CLERIC HERETIC MERCHANT MYSTIC NECROMANCER RAIDER PALADIN ROGUE SAGE SHEPHERD TACTICIAN WARHERO WARRIOR
end

baremodule HONOR_OPTS
using Base: @enum
@enum HONOR PEASANT KNIGHT LORD BARON VISCOUNT COUNT MARQUIS DUKE PRINCE
end

baremodule POPULATION_OPTS
using Base: @enum
@enum POPULATION PEASANTS SOLDIERS OSPECS DSPECS ELITES_OFF ELITES_DEF WIZARDS THIEVES
end


baremodule BUILDING_OPTS
using Base: @enum
@enum BUILDING begin
    HOMES
    FARMS
    MILLS
    BANKS
    TRAINING_GROUNDS
    ARMOURIES
    MILITARY_BARRACKS
    FORTS
    CASTLES
    HOSPITALS
    GUILDS
    TOWERS
    THIEVES_DENS
    WATCH_TOWERS
    UNIVERSITIES
    LIBRARIES
    STABLES
    DUNGEONS
end
end


baremodule SCIENCE_OPTS
using Base: @enum
@enum SCIENCE begin
    ALCHEMY 
    TOOLS 
    HOUSING 
    PRODUCTION 
    BOOKKEEPING 
    ARTISAN 
    STRATEGY 
    SIEGE 
    TACTICS 
    VALOR 
    RESILIENCE 
    HEROISM 
    CRIME 
    CHANNELING 
    SHIELDING 
    CUNNING 
    SORCERY 
    FINESSE
end
end

baremodule SCIENCE_GROUP_OPTS
using Base: @enum
@enum SCIENCE_GROUP begin
    ECONOMY
    WAR
    ARCANE
end
end

baremodule SELF_SPELLS
using Base: @enum
@enum SPELL begin
    CLEAR_SIGHT
    TOWN_WATCH
    GREATER_PROTECTION
    ILLUMINATE_SHADOWS
    DIVINE_SHIELD
    PATRIOTISM
    HEROS_INSPIRATION
    ANIMATE_DEAD
    MYSTIC_AURA
    GHOST_WORKERS
    MIST
    INVISIBILITY
    MAGES_WARD
    MAGES_FURY
    TREE_OF_GOLD
    AGGRESSION
    FOUNTAIN_OF_KNOWLEDGE
    BLOODLUST
    MINOR_PROTECTION
    INSPIRE_ARMY
    FANATICISM
    MINERS_MYSTIQUE
    SALVATION
    REVELATION
    QUICKFEET
    REFLECT_MAGIC
    MIND_FOCUS
    SCIENTIFIC_INSIGHTS 
    WRATH
    GUILE
end
end


baremodule EXTRA_TARGETS
using Base: @enum
@enum EXTRA_TARGET begin
    MAGE_DAMAGE_MULTIPLIER
    THIEF_DAMAGE_MULTIPLIER
    COMBAT_GAINS_MULTIPLIER
    ATTACK_TIME_MULTIPLIER    
    LOSSES_MULTIPLIER
    LANDLOSS_MULTIPLIER
    THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER
    THIEF_LOSSES_MULTIPLIER
    MAX_PRISONERS
    MAX_ELITE_OSPEC_RATIO
    MAX_ELITE_DSPEC_RATIO
end
end

@with_kw mutable struct Scientist
    books = 0
    cat::Union{SCIENCE_GROUP_OPTS.SCIENCE_GROUP, Nothing} = nothing
end

@with_kw mutable struct ScienceState 
    scientists::Array{Scientist} = []
    training_done = 0
end

@with_kw mutable struct ScienceGenMods 
    scientist_gen_mod = 1
    science_gen_mod = 1
    starting_scientists_mod = 0
end

@with_kw mutable struct OptimizationTarget
    RACE::RACE_OPTS.RACE
    PERS::PERS_OPTS.PERS
    HONOR::HONOR_OPTS.HONOR
    ACRES::Number
    SCIENCE::Union{Number, Nothing} = nothing    
    SCIENCE_DAYS::Union{Number, Nothing} = nothing
    SCIENCE_STATE::Union{ScienceState, Nothing} = ScienceState(scientists = [Scientist() for x in 0:5])
    SELF_SPELLS_EXCLUDE::AbstractArray{SELF_SPELLS.SPELL} = []
    INCOME::Number
    RUNES::Number
    MOD_WPA::Number
    MOD_TPA::Number
    DPA::Union{Number, Nothing} = nothing
    OPA::Union{Number, Nothing} = nothing
    OPA_DPA_RATIO::Union{Number, Nothing} = nothing
    OPT_FOR_NW::Bool = false
    OPT_FOR_TURTLE_DEF::Bool = false
    OPT_RAW_TPA_AND_WPA::Bool = false
    BUILDING_TARGETS::Dict{BUILDING_OPTS.BUILDING, NamedTuple} = Dict()
    TARGETS::Dict{EXTRA_TARGETS.EXTRA_TARGET,Number} = Dict()
    SCIENCE_BASE::Dict{SCIENCE_OPTS.SCIENCE,Number} = Dict()
end

#@with_kw mutable struct ExtraOptimizationTarget
#    RACE::Union{RACE_OPTS.RACE, Nothing} = nothing
#    PERS::Union{PERS_OPTS.PERS, Nothing} = nothing
#    HONOR::Union{HONOR_OPTS.HONOR, Nothing} = nothing
#    ACRES::Union{Number, Nothing} = nothing
#    SCIENCE::Union{Number, Nothing} = nothing    
#    SCIENCE_DAYS::Union{Number, Nothing} = nothing
#    SCIENCE_STATE::Union{ScienceState, Nothing} = nothing
#    SELF_SPELLS_EXCLUDE::Union{AbstractArray{SELF_SPELLS.SPELL}, Nothing} = []
#    INCOME::Union{Number, Nothing} = nothing
#    RUNES::Union{Number, Nothing} = nothing
#    MOD_WPA::Union{Number, Nothing} = nothing
#    MOD_TPA::Union{Number, Nothing} = nothing
#    DPA::Union{Number, Nothing} = nothing
#    OPA::Union{Number, Nothing} = nothing
#    OPA_DPA_RATIO::Union{Number, Nothing} = nothing
#    OPT_FOR_NW::Union{Bool, Nothing} = nothing
#    OPT_FOR_TURTLE_DEF::Union{Bool, Nothing} = nothing
#    OPT_RAW_TPA_AND_WPA::Union{Bool, Nothing} = nothing
#    BUILDING_TARGETS::Union{Dict{BUILDING_OPTS.BUILDING, NamedTuple}, Nothing} = Dict()
#    TARGETS::Union{Dict{EXTRA_TARGETS.EXTRA_TARGET,Number}, Nothing} = Dict()
#    SCIENCE_BASE::Union{Dict{SCIENCE_OPTS.SCIENCE,Number}, Nothing} = Dict()
#end

macro generate_nullable_struct(struct_name, base_struct)
    # Get the actual type of the base struct
    base_type = eval(base_struct)
    base_fields = fieldnames(base_type)
    
    # Create the struct expression
    struct_expr = quote
        Base.@kwdef mutable struct $(esc(struct_name))
            $(map(base_fields) do field
                field_type = fieldtype(base_type, field)
                
                # Handle Dict types specially
                if field_type <: Dict
                    # Extract key and value types from the Dict
                    key_type = keytype(field_type)
                    val_type = valtype(field_type)
                    
                    # Make both the Dict itself and its values nullable
                    if string(val_type) |> contains("Nothing")
                        :($(field)::Union{Dict{$(key_type), $(val_type)}, Nothing} = nothing)
                    else
                        :($(field)::Union{Dict{$(key_type), Union{$(val_type), Nothing}}, Nothing} = nothing)
                    end
                # Handle other types
                elseif string(field_type) |> contains("Nothing")
                    :($(field)::$(field_type) = nothing)
                else
                    :($(field)::Union{$(field_type), Nothing} = nothing)
                end
            end...)
        end
    end
    
    return struct_expr
end

# Generate the ExtraOptimizationTarget
@generate_nullable_struct ExtraOptimizationTarget OptimizationTarget


@with_kw mutable struct OptimizationDefinitions
    SOLDIER_OFF = 3
    OSPEC_OFF = 10
    DSPEC_DEF = 10
    ELITE_OFF = 0
    ELITE_DEF = 0
    HORSE_OFF = 2
    PRISONER_OFF = 8
    ELITE_NW = 8.5
    SELF_SPELLS::Vector{SELF_SPELLS.SPELL} = [SELF_SPELLS.MINOR_PROTECTION, SELF_SPELLS.INSPIRE_ARMY, SELF_SPELLS.FANATICISM]    
    BE_MULTIPLIER = 1
    POP_MULTIPLIER = 1
    INCOME_MULTIPLIER = 1
    FOOD_NEEDED_MULTIPLIER = 1
    WPA_MODIFIER = 1
    WAGES_MULTIPLIER = 1
    RUNE_PROD_MULTIPLIER = 1
    TPA_MODIFIER = 1
    ATTACK_TIME_MODIFIER = 1
    ATTACK_LOSSES_MODIFIER = 1
    OME_MODIFIER = 1
    DME_MODIFIER = 1
    HOME_POP_MULTIPLIER = 1
    BUILD_PROD_MULTIPLIER = 1
    BUILD_CAP_MULTIPLIER = 1
    WT_EFFECT_MULTIPLIER = 1
    LAND_GENERATES_FOOD = 0
    COMBAT_GAINS_MULTIPLIER = 1
    RUNES_WANTED_MOD = 1
    BARRACKS_EFFECT_MULTIPLIER = 1
    RUNE_NEEDED_MULTIPLIER = 1
    GUILD_NEEDED_MULTIPLIER = 1
    OP_DAMAGE_RECEIVED_MULTIPLIER = 1
    HOME_POP_INCREASE = 0
    DUNGEON_CAPACITY_MODIFIER = 1
    THIEF_DAMAGE_MULTIPLIER = 1
    MAGE_DAMAGE_MULTIPLIER = 1
    THIEF_LOSSES_FACTOR = 1
    HONOR_EFFECTS = 1 
    ATTACK_DAMAGE_MODIFIER = 1
    DRYAD_LAND_FOOD_RUNES_MULTIPLIER = 0
    ELF_GUILD_RUNES = 0
    SCIENCE_MULTIPLIER = 1
    CUNNING_SCIENCE_MULTIPLIER = 1
    CHANNELING_SCIENCE_MULTIPLIER = 1
    VALOR_SCIENCE_MULTIPLIER = 1
    HEROISM_SCIENCE_MULTIPLIER = 1
    CRIME_SCIENCE_MULTIPLIER = 1
    GHOST_WORKER_EFFECT = 1    
    MINERS_MYSTIQUE_EFFECT = 0
    PRISONER_PER_ACRE = 0
    HORSE_PER_ACRE = 0
    HORSE_NW_BASE = 0.3
    SCIENCE_GENERATION_MODS = ScienceGenMods()
    SCIENCE_BONUS_MODS = Dict{SCIENCE_OPTS.SCIENCE, Number}()  
    LAND_EFFECT_MULTIPLIERS = Dict{BUILDING_OPTS.BUILDING, Number}()        
    WICKED_BUILDING_DATA = Dict{BUILDING_OPTS.BUILDING, Vector{Pair{Symbol, NamedTuple}}}()
end

#@with_kw mutable struct UtopiaBuild
#
#end

end
