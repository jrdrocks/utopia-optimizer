

module Science

using Parameters

using ..DataStructs
using ..GameDefinitions


export process_science, total_books, assign_scientist

function assign_scientist(scientists::Array{Scientist}, target_science::Array{Float64})
    # Calculate current science generation per category
    current_science = Dict(
        SCIENCE_GROUP_OPTS.ECONOMY => 0.0,
        SCIENCE_GROUP_OPTS.WAR => 0.0,
        SCIENCE_GROUP_OPTS.ARCANE => 0.0
    )
    
    # Get current scientist levels and sum their contributions
    for s in scientists
        if s.cat !== nothing
            index = findlast(books -> s.books >= books, SCIENCE_DATA.rank_up)
            index = index === nothing ? 1 : index + 1
            base_gen = SCIENCE_DATA.levels[index]
            
            # Find multiplier for this mscientist's category
            for (science, data) in SCIENCE_DATA.cats
                if data.group == s.cat
                    current_science[s.cat] += base_gen * data.mul
                    break
                end
            end
        end
    end
    
    # Calculate target science points
    total_target = sum(target_science)
    target_points = Dict(
        SCIENCE_GROUP_OPTS.ECONOMY => target_science[1] / total_target,
        SCIENCE_GROUP_OPTS.WAR => target_science[2] / total_target,
        SCIENCE_GROUP_OPTS.ARCANE => target_science[3] / total_target
    )
    
    # Find category with largest deficit
    max_deficit = -Inf
    best_cat = SCIENCE_GROUP_OPTS.ECONOMY
    for cat in keys(current_science)
        deficit = target_points[cat] - (current_science[cat] / sum(values(current_science)))
        if deficit > max_deficit
            max_deficit = deficit
            best_cat = cat
        end
    end
    
    return best_cat
end



function generate(self::Scientist, mod)
    index = findlast(books -> self.books >= books, SCIENCE_DATA.rank_up)
    index = index === nothing ? 1 : index + 1
    self.books += SCIENCE_DATA.levels[index] * mod
end


function total_books(self::ScienceState)
    mapreduce(x -> x.books, +, self.scientists)
end



function process_science(state::ScienceState, mods::ScienceGenMods, days)
    if mods.starting_scientists_mod > 0
        push!(state.scientists, Scientist())
    elseif mods.starting_scientists_mod < 0
        pop!(state.scientists)
    end

    for day in 1:days
        for hr in 1:24
            state.training_done += SCIENCE_DATA.training_speed * mods.scientist_gen_mod
            if state.training_done >= 100
                push!(state.scientists, Scientist())
                state.training_done = 0
            end

            for s in state.scientists
                generate(s, mods.science_gen_mod)
            end
        end
    end
end


end
