include("src/utopia.jl")

target = OptimizationTarget(
    RACE_OPTS.ORC, # PREF_FIRST_RAC = 
    RACE_OPTS.UNDEAD, # PREF_SECOND_RACE = 
    HONOR_OPTS.BARON,
    900, # PREF_ACRES = 
    400_000, # PREF_SCIENCE = 
    4_000, # PREF_INCOME = 
    1_250, # PREF_RUNES = 
    0.75, # PREF_MOD_WPA = 
    2.5, # PREF_MOD_TPA = 
    75, # for attackers, PREF_DPA = 
    -1, # for tms, PREF_OPA = 
    -1, # OPA_DPA_RATIO
    true, # opt for nw
    false, #target turtle def
    true, #OPT_RAW_TPA_AND_WPA instead of mod
    Dict(
        :effective_guilds_pct => 10,
        #:min_homes_pct => 0,
        :max_homes_pct => 30,
        #:thief_losses_factor => 0.5, #min
        :thief_op_damage_recieve_factor => 0.7, #max x*100 pct of damage
        :landloss_factor => 0.7, #max
        :losses_factor => 0.5, #max
        :attack_time_factor => 0.9, #max
        #:combat_gains_multiplier => 1.2, #min
        #:thief_damage_multiplier => 1.4, #min
        #:mage_damage_multiplier => 1.3, #min
        :max_prisoners => 500
    )
)



(frames, values) = optimizeProvince(target)
 
target = Args.OptimizationTarget(
    RACE_OPTS.ORC, # PREF_FIRST_RAC = 
    RACE_OPTS.GNOME, # PREF_SECOND_RACE = 
    1200, # PREF_ACRES = 
    400_000, # PREF_SCIENCE = 
    4_000, # PREF_INCOME = 
    1_250, # PREF_RUNES = 
    0.75, # PREF_MOD_WPA = 
    7.5, # PREF_MOD_TPA = 
    80, # for attackers, PREF_DPA = 
    -1, # for tms, PREF_OPA = 
    -1, # OPA_DPA_RATIOfalse,
    true, # opt for nw
    false, #target turtle def
    true, #OPT_RAW_TPA_AND_WPA instead of mod
    Dict(
        :effective_guilds_pct => 10,
        #:min_homes_pct => 0,
        :max_homes_pct => 30,
        :thief_losses_factor => 0.5, #min
        #:thief_op_damage_recieve_factor => 0.3, #max x*100 pct of damage
        :landloss_factor => 0.7, #max
        :losses_factor => 0.75, #max
        :attack_time_factor => 0.9, #max
        #:combat_gains_multiplier => 1.2, #min
        #:thief_damage_multiplier => 1.4, #min
        #:mage_damage_multiplier => 1.3, #min
        :max_prisoners => 500
    )
)

(frames, values) = optimizeProvince(target)

"""

target = Args.OptimizationTarget(
    RACE_OPTS.ELF, #PREF_FIRST_RAC = 
    RACE_OPTS.FAERY, #PREF_SECOND_RACE = 
    1000, #PREF_ACRES = 
    1_000_000, #PREF_SCIENCE = 
    5_000, #PREF_INCOME = 
    4_000, #PREF_RUNES = 
    5, #PREF_MOD_WPA = 
    14, #PREF_MOD_TPA = 
    -1, # for attackers, PREF_DPA = 
    25, # for tms, PREF_OPA = 
    -1 # OPA_DPA_RATIO
)


(frames, values) = optimizeProvince(target)
0
"""
