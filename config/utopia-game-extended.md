Okay, here's a new markdown document focused specifically on the additions and improvements we discussed, building upon the previous comprehensive document. This document will highlight the areas where we need more information and provide a structure for incorporating those details as they become available.

# Utopia Game Mechanics: Additions and Refinements

This document outlines further details, clarifications, and missing information needed to enhance the Utopia game model.

## 1. Formulas and Calculations

### 1.1 Missing/Incomplete Formulas

We need to find and add the following formulas:

*   **`BUILDING_EFFICIENCY`:**
    *   Current formula is incomplete. We know it's affected by `SCIENCE_TYPES[TOOLS]`, `BUILDING_AMOUNT[MILLS]`, `RACE` (Dwarf), `PERSONALITY` (Artisan), `SPELLS` (Blizzard), and `RITUAL`(Expedient).
    *   Need to determine the exact formula, including how changes in building efficiency phase in over time.
*   **`OME` (Offensive Military Efficiency):**
    *   Current Formula: `OME = (Base Military Efficiency + Training Grounds Bonus + Honor Bonus) * Science Bonus * Race Bonus * Personality Bonus * Fanaticism * Bloodlust * Ritual`
    *   We need the specific values for:
        *   `Base Military Efficiency` (how is this calculated initially?)
        *   `Training Grounds Bonus` (how much bonus per building/percentage of land?)
        *   `Honor Bonus` (how much bonus per point of honor?)
        *   `Science Bonus` (specific values for each relevant science)
        *   `Race/Personality/Spell/Ritual Bonuses` (specific values)
*   **`DME` (Defensive Military Efficiency):**
    *   Current Formula: `DME = (Base Military Efficiency + Forts Bonus + Honor Bonus) * Science Bonus * Race Bonus * Personality Bonus * Minor Protection * Greater Protection * Fanaticism * Plague * Ritual`
    *   We need the specific values for:
        *   `Forts Bonus`
        *   `Honor Bonus`
        *   `Science Bonus`
        *   `Race/Personality/Spell/Ritual Bonuses`
*   **`POPULATION_GROWTH_MODIFIER`:**
    *   Need a formula that combines the effects of:
        *   `BUILDING_AMOUNT[HOMES]`
        *   `BUILDING_AMOUNT[HOSPITALS]`
        *   `RACE` (Halfling, Faerie)
        *   `PERSONALITY` (Shepherd)
        *   `SPELLS` (Love & Peace, Chastity)
        *   `RITUAL`(Affluent, Benediction)
        *   How do these stack (additive, multiplicative)? Are there diminishing returns?
*   **`SCIENCE_PRODUCTION_MODIFIER`:**
    *   Need a formula that combines the effects of:
        *   `BUILDING_AMOUNT[UNIVERSITIES]`
        *   `BUILDING_AMOUNT[LIBRARIES]`
        *   `RACE` (Human)
        *   `PERSONALITY` (Shepherd)
        *   `SPELLS` (Fountain of Knowledge, Scientific Insights)
        *   `RITUAL`(Affluent)
*   **`THIEVERY_OPERATIONS`:**
    *   Need detailed formulas for each operation, including:
        *   Success rate calculation (based on `TPA`, modifiers, etc.)
        *   Gains/losses calculation (specific amounts for each resource/item stolen/lost)
        *   Factors that affect success rate and gains/losses (e.g., `THIEVERY_RESISTANCE_MODIFIER`, `SPELL_DAMAGE_MODIFIER`, etc.)
*   **`SPELL` Success Rates:**
    *   Need a general formula for spell success rate.
    *   Factors to consider: `WPA`, spell difficulty, `SPELL_SUCCESS_MODIFIER`, target's `SPELL_RESISTANCE_MODIFIER`.
*   **`DRAGON` Slaying:**
    *   Need a formula for damage inflicted on dragons per unit type.
*   **`RITUAL` Strength:**
    *   Need a formula for how much each successful `CAST_RITUAL` increases ritual strength.
    *   Need the formula for how ritual strength diminishes over time.
    *   Need the formula that modifies the effects based on Ritual strength
*   **Attack Time Calculation:**
    *   Need the formula for how the Net Worth difference between attacker and defender modifies the base attack time.

### 1.2 Loss/Casualty Formulas

*   **`MILITARY_CASUALTY_MODIFIER`:**
    *   Need a complete formula that combines the base casualty rate with all modifiers (Hospitals, Resilience, race, personality, spells, rituals, etc.).
*   **`THIEF_LOSS_MODIFIER`:**
    *   Need a formula for calculating thief losses during thievery operations.
*   **Resource Losses When Attacked:**
    *   Need a formula for calculating resource losses, considering `CASTLES`, spells, and other relevant modifiers.
*   **Building Losses from Arson/Greater Arson/Tornadoes:**
    *   Need formulas for calculating building losses from these specific attacks/spells.

### 1.3 Base Values

*   **`INCOME_RATE`:** What is the base income rate before any modifiers are applied?
*   **`BIRTH_RATE`:** What is the base birth rate before any modifiers are applied?
*   **`SPELL_DAMAGE_MODIFIER`:** What are the base damage values for spells before modifiers?
*   **`WIZARD_PRODUCTION_MODIFIER`:** What is the base production of wizards?

## 2. Missing Game Elements and Interactions

### 2.1 Espionage Operations

| Operation          | Description                                                 | Cost | Risks | Effects |
| :----------------- | :---------------------------------------------------------- | :--- | :---- |:---- |
| `SPY_ON_THRONE`    | Reveals target's throne information (resources, army, etc.) | ?    | ?     | Provides detailed information about the target province's current state. |
| `SPY_ON_DEFENSE`   | Reveals target's defensive information (DME, Forts, etc.)    | ?    | ?     | Provides information on the target's defensive capabilities, including `DME_MODIFIER`.|
| `SNATCH_NEWS`      | Copies latest news from target kingdom                      | ?    | ?     | Allows you to see the target kingdom's recent activities and events. |
| `INFILTRATE`       | Attempts to steal gold, food, or runes                      | ?    | ?     |  Potentially steal `GOLD`, `FOOD`, or `RUNES` from the target. |
| `SURVEY`           | Reveals target's land and building information             | ?    | ?     | Provides detailed information about the target's land and buildings. |
| `SPY_ON_MILITARY`  | Reveals target's military information                       | ?    | ?     | Provides detailed information about the target's military units and strength.|
| `SPY_ON_SCIENCES`  | Reveals target's science information                       | ?    | ?     | Provides information about the target's current science levels.|

### 2.2 Thievery Operations

| Operation          | Effects                                                                                                         |
| :----------------- | :-------------------------------------------------------------------------------------------------------------- |
| `DESTABILIZE_GUILDS` | Reduces target's `SPELL_DURATION_MODIFIER` (Need formula and duration)                                         |
| `INCITE_RIOTS`       | Reduces target's `POPULATION_GROWTH_MODIFIER` (Need formula and duration)                                       |
| `BRIBE_THIEVES`      | Reduces target's `STEALTH` (Need formula)                                                                       |
| `BRIBE_GENERALS`      | Increases target's `MILITARY_CASUALTY_MODIFIER` (20% chance for +15% to all Military Casualties according to doc) |
| `FREE_PRISONERS`      | Frees some of your captured `PRISONERS` (Need formula)                                                          |
| `ASSASSINATE_WIZARDS`  | Kills a number of target's `WIZARDS` (Need formula)                                                              |

### 2.3 Dragons

| Dragon Type    | Effects                                                                                     |
| :------------- | :------------------------------------------------------------------------------------------ |
| `EMERALD_DRAGON` | -40% Building and Specialist Credits Gained in Combat, -20% Gold, Food, Runes gain in combat |
| `RUBY_DRAGON`    | -15% Military Efficiency, +30% Military Wages, Lose 30% of new drafted Soldiers             |
| `SAPPHIRE_DRAGON` | -30% Magic Effectiveness (WPA), -30% Thievery Effectiveness (TPA)                          |
| `TOPAZ_DRAGON`   | -30% Building Efficiency, -25% Income, Destroys 4% of Buildings Instantly and Every 6 Ticks |

*   **Dragon-Slaying:**
    *   Need a formula for damage inflicted on dragons per unit type.
    *   Specific bonuses for races/personalities?

### 2.4 Rituals

*   **Mana Cost:** What is the base mana cost for casting a ritual?
*   **Completion:** How much does each successful cast contribute to the ritual's completion?
*   **Strength:** How is `RITUAL_STRENGTH` calculated, and how does it affect the ritual's effects?

### 2.5 Multi-Attack Protection (MAP)

*   Need a formula or more details on how `MAP` works.
*   How much does it reduce casualties?
*   How many attacks trigger it?
*   How long does it last?

### 2.6 Honor

*   Need formulas for gaining and losing honor.
*   Need specific values for how honor affects `OME` and `DME`.

### 2.7 Relations

*   Need a table specifying how different relation levels (Unfriendly, Hostile, War) modify:
    *   Attack gains
    *   Spell effects
    *   Thievery operation effects
    *   Other interactions

### 2.8 Stances

*   **Limitations:** Are there any limitations on changing `STANCE` (e.g., cooldown period)?

### 2.9 Kingdom Networth Factor

*   Clarify whether the average province size refers to **land size** or another metric.

### 2.10 War

*   Apart from the already mentioned effects, how does being in a `WAR` affect:
    *   Attack gains?
    *   Other game mechanics?

### 2.11 Building/Specialist Credits from Attacks

*   Need formulas for calculating `BUILDING_CREDITS` and `SPECIALIST_CREDITS` gained from attacks.

## 3. Clarifications and Details

*   **Building Production:**
    *   Specify base production rates for each building that produces resources.
*   **Unit Costs:**
    *   Provide base costs (gold, resources) for training each military unit type.
*   **Spell Costs:**
    *   Specify the rune cost for each spell.
*   **Spell Duration:**
    *   Provide the base duration for each spell before modifiers.
*   **Spell and Thievery Operation Success Rates:**
    *   Explain how success rates are calculated. What factors influence them?
*   **Interaction Effects:**
    *   Describe in more detail how certain spells interact with each other (e.g., Droughts canceling Storms).
*   **Rounding:**
    *   Specify how rounding is applied in calculations (e.g., always round down, round to nearest integer, etc.).
*   **Limits:**
    *   Clarify any limits on modifiers (e.g., maximum `BUILDING_EFFICIENCY_MODIFIER`, minimum `ATTACK_TIME_MODIFIER`, etc.).

## 4. Structure and Organization

*   **Grouping Modifiers:** Consider grouping modifiers under the property they affect. For example, under `POPULATION_GROWTH_MODIFIER`, list all the things that affect it (races, personalities, buildings, spells, sciences, etc.).
*   **Tables:** Use more tables to clearly present information, especially for spell effects, thievery operation effects, race/personality modifiers, etc.
*   **Examples:** Add concrete examples to illustrate how formulas and calculations work in practice.

## 5. Additional Considerations

*   **Retired Mechanics:** Briefly mention any retired mechanics that might still be relevant for understanding the current game, like the removed stances.
*   **Game Version:** Specify the game version or age that this documentation applies to, as mechanics might change with updates.

## 6. Updated effects

### Spells

| Spell                    | Target    | Effects                                                                                                                              |
| :----------------------- | :-------- | :----------------------------------------------------------------------------------------------------------------------------------- |
| MINOR_PROTECTION         | SELF      | `DME_MODIFIER`: + 5%                                                                                                       |
| GREATER_PROTECTION       | SELF      | `DME_MODIFIER`: + 5%                                                                                                       |
| MAGIC_SHIELD             | SELF      | Increases defensive `WPA_MODIFIER` by 20%                                                                                            |
| MYSTIC_AURA              | SELF      | Next incoming spell fails                                                                                                             |
| FERTILE_LANDS            | SELF      | `FOOD_PRODUCTION_MODIFIER`: + 25%                                                                                          |
| NATURES_BLESSING         | SELF      | Immunity to `SPELLS`[Storms, Droughts], 33% chance to cure the Plague                                                                |
| LOVE_AND_PEACE           | SELF      | `POPULATION_GROWTH_MODIFIER`: +0.85% and `HORSE_PRODUCTION_RATE`: +40%                                                                    |
| DIVINE_SHIELD            | SELF      | `INSTANT_SPELL_DAMAGE_TAKEN_MODIFIER`: -25%                                                                                           |
| QUICK_FEET              | SELF      | `ATTACK_TIME_MODIFIER`: - 10%                                                                                               |
| BUILDERS_BOON           | SELF      | `CONSTRUCTION_TIME_MODIFIER`: - 25%                                                                                        |
| INSPIRE_ARMY             | SELF      | `WAGE_MODIFIER`: -15%, `TRAINING_TIME_MODIFIER`: -20%                                                                     |
| HEROS_INSPIRATION        | SELF      | `WAGE_MODIFIER`: -30% and `TRAINING_TIME_MODIFIER`: -30%                                                                     |
| SCIENTIFIC_INSIGHTS      | SELF      | `SCIENCE_PRODUCTION_MODIFIER`: +10%                                                                                      |
| ANONYMITY                | SELF      | Hides province name in attacks, no honor gains, `GOLD_GAINS_MODIFIER` and `BATTLE_RESOURCE_GAINS_MODIFIER`: -15%, ambush immunity                                                    |
| ILLUMINATE_SHADOWS       | SELF      | Decreases incoming `THIEVERY_DAMAGE_MODIFIER` by -20%                                                                                    |
| SALVATION                | SELF      | `MILITARY_CASUALTY_MODIFIER`: -15%                                                                                       |
| WRATH                    | SELF      | `ENEMY_MILITARY_CASUALTY_MODIFIER`: +20%                                                                                  |
| INVISIBILITY             | SELF      | `TPA_MODIFIER`: +10%, `THIEF_LOSS_MODIFIER`: -20%                                                      |
| CLEAR_SIGHT              | SELF      | 25% chance to catch enemy thieves, `THIEVERY_RESISTANCE_MODIFIER`: +25%                                                                                                    |
| MAGES_FURY               | SELF      | Increases offensive `WPA_MODIFIER` by +25%, decreases defensive `WPA_MODIFIER` by -25%                                                     |
| WAR_SPOILS               | SELF      | Take control of land gained on attacks                                                                                                |
| MIND_FOCUS               | SELF      | Increases `WIZARD_PRODUCTION_MODIFIER` by +25%                                                                                       |
| FANATICISM               | SELF      | `OME_MODIFIER`: +5%, `DME_MODIFIER`: -5%                                                                       |
| GUILE                    | SELF      | `SPELL_DAMAGE_MODIFIER`: +10%, `SABOTAGE_DAMAGE_MODIFIER`: +10%                                                                              |
| REVELATION               | SELF      | Increases `SCIENTIST_PRODUCTION_MODIFIER` by +20%                                                                                     |
| FOUNTAIN_OF_KNOWLEDGE    | SELF      | `SCIENCE_PRODUCTION_MODIFIER`: +10%                                                                                      |
| TREE_OF_GOLD             | SELF      | Instantly generates 26.66% to 53.33% of daily income                                                                                   |
| TOWN_WATCH               | SELF      | Every 5 peasants defend with 1 strength, high casualties                                                                                |
| AGGRESSION               | SELF      | Soldiers are +2/-2 strength                                                                                                          |
| MINERS_MYSTIQUE          | SELF      | Peasants generate an extra 0.3 gold per tick                                                                                           |
| GHOST_WORKERS            | SELF      | Decreases jobs required for maximum `BUILDING_EFFICIENCY` by -25%                                                                      |
| ANIMATE_DEAD             | SELF      | 50% of defensive military casualties return as soldiers in next defense                                                                 |
| MIST                     | SELF      | `BATTLE_RESOURCE_GAINS_MODIFIER`: -10% in battle                                                                                                  |
| REFLECT_MAGIC            | SELF      | 20% chance to reflect offensive spells back at casters                                                                                |
| SHADOWLIGHT              | SELF      | Reveals origin province of next thievery op, next sabotage fails automatically                                                           |
| BLOODLUST                | SELF      | `OME_MODIFIER`: +10%, `ENEMY_MILITARY_CASUALTY_MODIFIER`: +15%, own `MILITARY_CASUALTY_MODIFIER`: +15%                  |
| PATRIOTISM               | SELF      | Increases draft speed by +30%, decreases propaganda damage taken by -30%                                                                  |
| PARADISE                 | SELF      | Creates a few acres of land, consumes explore pool, not available during war or protection                                               |
| CAST_RITUAL              | SELF      | Increases progress towards the chosen `RITUAL`                                                                                     |
| STORMS                   | OFFENSIVE | Kills 1.5% of target's `POPULATION`, cancels `SPELLS`[Droughts], increases `SPELLS`[Tornadoes] damage taken by +15%                       |
| DROUGHTS                | OFFENSIVE | `FOOD_PRODUCTION_MODIFIER`: -25%, draft speed - 15%, kills some warhorses, cancels `SPELLS`[Storms], increases `THIEVERY_OPERATIONS`[Arson] damage taken by +15% |
| MAGIC_WARD               | OFFENSIVE | (Unfriendly, Hostile, War) Increases target's rune costs by 100%                                                                         |
| GLUTTINY                 | OFFENSIVE | `FOOD_CONSUMPTION_MODIFIER`: +25%                                                                                          |
| VERMIN                   | OFFENSIVE | Destroys on average 50% of target's `FOOD`                                                                                            |
| GREED                    | OFFENSIVE | `WAGE_MODIFIER`: +25%, `DRAFT_COST_MODIFIER`: +25%                                                                             |
| EXPOSE_THIEVES           | OFFENSIVE | (Unfriendly, Hostile, War) Reduces target's `STEALTH` by -5% each tick                                                                   |
| CHASTITY                 | OFFENSIVE | `POPULATION_GROWTH_MODIFIER`: -50%                                                                                          |
| SLOTH                    | OFFENSIVE | (Unfriendly, Hostile, War) Reduces draft rate by -50%, `DRAFT_COST_MODIFIER`: +100%                                           |
| FIREBALL                 | OFFENSIVE | (Unfriendly, Hostile, War) Kills ~6% of opponent's `PEASANTS`                                                                           |
| FOOLS_GOLD               | OFFENSIVE | (Unfriendly, Hostile, War) Destroys up to 25% of target's `GOLD`                                                                        |
| LIGHTNING_STRIKE         | OFFENSIVE | (Unfriendly, Hostile, War) Destroys up to 65% of target's `RUNES`                                                                        |
| EXPLOSIONS               | OFFENSIVE | When sending aid, 50% chance to reduce shipment to 55-80% if either sender or receiver is affected                                       |
| BLIZZARD                 | OFFENSIVE | `BUILDING_EFFICIENCY_MODIFIER`: -10%                                                                                     |
| PITFALLS                 | OFFENSIVE | Increases opponent's defensive `MILITARY_CASUALTY_MODIFIER` by +15%                                                                      |
| NIGHTMARES               | OFFENSIVE | (Unfriendly, Hostile, War) ~1.5% of target's troops are put back in training over 8 ticks                                               |
| TORNADOES                | OFFENSIVE | (Unfriendly, Hostile, War) Destroys several of opponent's `BUILDINGS`                                                                   |
| MYSTIC_VORTEX            | OFFENSIVE | (Unfriendly, Hostile, War) 50% chance to remove each active spell on target                                                              |
| AMNESIA                  | OFFENSIVE | (War) Temporarily removes ~5% of target's allocated `SCIENCE_BOOKS`, returns over 48 ticks                                               |
| METEOR_SHOWERS           | OFFENSIVE | (Hostile, War) Kills peasants and troops every tick                                                                                      |
| ABOLISH_RITUAL           | OFFENSIVE | (Unfriendly, Hostile, War) Reduces enemy `RITUAL` strength by -2%, limited to 10 casts per target                                         |
| LAND_LUST                | OFFENSIVE | (Unfriendly, Hostile, War) Captures 1-1.25% of opponent's acres                                                                        |

## 12. Updated effects with Modifiers

### Buildings

| Building         | Effects                                                                                                       |
| :--------------- | :------------------------------------------------------------------------------------------------------------ |
| **HOMES**        | Increases: `POPULATION` (capacity), `POPULATION_GROWTH_MODIFIER`                                       |
| **FARMS**        | Increases: `FOOD`, `FOOD_PRODUCTION_MODIFIER`                                                                 |
| **MILLS**        | Increases: `BUILDING_EFFICIENCY_MODIFIER`, Decreases: `CONSTRUCTION_TIME_MODIFIER`, `EXPLORATION_COST_MODIFIER` |
| **BANKS**        | Increases: `GOLD`, `INCOME_MODIFIER`                                                                           |
| **TRAINING_GROUNDS** | Increases: `OME_MODIFIER`, Decreases: `TRAINING_TIME_MODIFIER`                                                |
| **ARMORIES**     | Decreases: `DRAFT_COST_MODIFIER`, `WAGE_MODIFIER`, `MILITARY_UNIT_COST`                                    |
| **BARRACKS**     | Decreases: `ATTACK_TIME_MODIFIER`                                                                              |
| **FORTS**        | Increases: `DME_MODIFIER`                                                                                      |
| **CASTLES**      | Decreases: Resource losses when attacked                                                                       |
| **HOSPITALS**    | Decreases: `MILITARY_CASUALTY_MODIFIER`, Increases: `POPULATION_GROWTH_MODIFIER`                               |
| **GUILDS**       | Trains: `WIZARDS`, Increases: `SPELL_DURATION_MODIFIER`                                                          |
| **TOWERS**       | Increases: `RUNE_PRODUCTION_MODIFIER`                                                                         |
| **THIEVES_DENS** | Increases: `TPA_MODIFIER`, `STEALTH`, Decreases: `THIEF_LOSS_MODIFIER`                                         |
| **WATCH_TOWERS** | Increases: `THIEVERY_RESISTANCE_MODIFIER`, Decreases: `THIEVERY_DAMAGE_MODIFIER`                                |
| **UNIVERSITIES** | Increases: `SCIENTIST_PRODUCTION_RATE`, `SCIENCE_PRODUCTION_MODIFIER`                                         |
| **LIBRARIES**    | Increases: `SCIENCE_PRODUCTION_MODIFIER`                                                                       |
| **STABLES**      | Produces: `HORSES`, Increases: `HORSE_PRODUCTION_RATE`                                                         |
| **DUNGEONS**      | Houses: `PRISONERS`                                                                                           |

### Sciences

| Science       | Category     | Effects                                                                             |
| :------------ | :----------- | :---------------------------------------------------------------------------------- |
| ALCHEMY       | ECONOMY      | Increases `INCOME_MODIFIER`                                                          |
| TOOLS         | ECONOMY      | Increases `BUILDING_EFFICIENCY_MODIFIER`                                             |
| HOUSING       | ECONOMY      | Increases `POPULATION` capacity                                                      |
| PRODUCTION    | ECONOMY      | Increases `FOOD_PRODUCTION_MODIFIER`, `RUNE_PRODUCTION_MODIFIER`                    |
| BOOKKEEPING   | ECONOMY      | Decreases `WAGE_MODIFIER`                                                             |
| ARTISAN       | ECONOMY      | Decreases `CONSTRUCTION_TIME_MODIFIER`, `CONSTRUCTION_COST_MODIFIER`, `RAZE_COST_MODIFIER` |
| STRATEGY      | MILITARY     | Increases `DME_MODIFIER`                                                             |
| SIEGE         | MILITARY     | Increases `BATTLE_RESOURCE_GAINS_MODIFIER`                                         |
| TACTICS       | MILITARY     | Increases `OME_MODIFIER`                                                             |
| VALOR         | MILITARY     | Decreases `MILITARY_UNIT_TRAINING_TIME`                                             |
| HEROISM       | MILITARY     | Decreases `DRAFT_COST_MODIFIER`, increases `DRAFT_RATE`                                |
| RESILIENCE     | MILITARY     | Decreases `MILITARY_CASUALTY_MODIFIER`                                                |
| CRIME         | ARCANE_ARTS  | Increases `TPA_MODIFIER`, `STEALTH`                                                   |
| CHANNELING    | ARCANE_ARTS  | Increases `WPA_MODIFIER`                                                             |
| SHIELDING     | ARCANE_ARTS  | Decreases damage from enemy thievery (`THIEVERY_RESISTANCE_MODIFIER`) and instant magic (`SPELL_RESISTANCE_MODIFIER`) |
| SORCERY       | ARCANE_ARTS  | Increases instant `SPELL_DAMAGE_MODIFIER`                                            |
| CUNNING       | ARCANE_ARTS  | Increases `THIEVERY_DAMAGE_MODIFIER`                                                 |
| FINESSE       | ARCANE_ARTS  | Decreases `THIEF_LOSS_MODIFIER`, `WIZARD_LOSS_MODIFIER` (on failed ops/spells)         |

### Rituals

| Ritual     | Effects                                                                                                       |
| :--------- | :------------------------------------------------------------------------------------------------------------ |
| BARRIER    | `SPELL_RESISTANCE_MODIFIER` to Instant Magic : -20%, `THIEVERY_RESISTANCE_MODIFIER`: -20% , -10% Battle (Resource) Losses                       |
| EXPEDIENT  | `BUILDING_EFFICIENCY_MODIFIER`: +10%, `WAGE_MODIFIER`: -20%, `CONSTRUCTION_COST_MODIFIER`: -20%                                           |
| HASTE      | `ATTACK_TIME_MODIFIER`: -10%, `TRAINING_TIME_MODIFIER`: -25%, `CONSTRUCTION_TIME_MODIFIER`: -25%                                                    |
| HAVOC      | `WPA_MODIFIER`: +20% (offensive) , `TPA_MODIFIER`: +20% (offensive), `SPELL_DAMAGE_MODIFIER`: +10%, `SABOTAGE_DAMAGE_MODIFIER`: +10%                                  |
| ONSLAUGHT  | `OME_MODIFIER`: +10%, `ENEMY_MILITARY_CASUALTY_MODIFIER`: +15%                                  |
| STALWART   | `DME_MODIFIER`: +5%, `MILITARY_CASUALTY_MODIFIER`: -20%                                                    |

### Stances

| Stance      | Effects                                                                          |
| :---------- | :------------------------------------------------------------------------------- |
| NORMAL      | No Effects                                                                       |
| AGGRESSIVE  | `BATTLE_RESOURCE_GAINS_MODIFIER`: +10%,  `SPECIALIST_CREDITS` gain in combat : +30%, `MILITARY_CASUALTY_MODIFIER`: +15% (self, offensive), `SPELL_DAMAGE_MODIFIER`: +10% (instant, offensive), `SABOTAGE_DAMAGE_MODIFIER`: +10% (offensive), `WAGE_MODIFIER`: +20% |
| PEACEFUL    | `INCOME_MODIFIER`: +15%, `FOOD_PRODUCTION_MODIFIER`: +20%, `RUNE_PRODUCTION_MODIFIER`: +20%, `POPULATION_GROWTH_MODIFIER`: +20%, `OME_MODIFIER`: -15%,  `SPELL_DAMAGE_MODIFIER`: -20% (outgoing, instant), `SABOTAGE_DAMAGE_MODIFIER`: -20% (outgoing) |

##

## 13. Thievery Operations

Here is a more detailed look at each thievery operation, including potential costs, risks, and effects. The exact mechanics of success rate calculation are still to be determined, but we know they involve factors like `TPA_MODIFIER`, relative `STEALTH` levels, and possibly other modifiers.

| Thievery Operation | Description                                                                                             | Cost in Thieves/Stealth                            | Risks                                                              | Effects                                                                                                                                                             |
| :----------------- | :------------------------------------------------------------------------------------------------------ | :-------------------------------------------------- | :----------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `SPY_ON_THRONE`    | Reveals target's throne information (resources, army, etc.)                                              |  ?                                                  | Potential for detection and loss of thieves                       | Provides detailed information about the target province's current state.                                                                                   |
| `SPY_ON_DEFENSE`   | Reveals target's defensive information (DME, Forts, etc.)                                               |  ?                                                  | Potential for detection and loss of thieves                       | Provides information on the target's defensive capabilities, including `DME_MODIFIER`.                                                                  |
| `SNATCH_NEWS`      | Copies latest news from target kingdom                                                                  |  ?                                                  | Potential for detection and loss of thieves                       | Allows you to see the target kingdom's recent activities and events.                                                                                   |
| `INFILTRATE`       | Attempts to steal gold, food, or runes                                                                  |  ?                                                  | Potential for detection, loss of thieves, and resource loss     | Potentially steal `GOLD`, `FOOD`, or `RUNES` from the target.                                                                                             |
| `SURVEY`           | Reveals target's land and building information                                                          |  ?                                                  | Potential for detection and loss of thieves                       | Provides detailed information about the target's land and buildings.                                                                                      |
| `SPY_ON_MILITARY`  | Reveals target's military information                                                                    |  ?                                                  | Potential for detection and loss of thieves                       | Provides detailed information about the target's military units and strength.                                                                               |
| `SPY_ON_SCIENCES`  | Reveals target's science information                                                                     |  ?                                                  | Potential for detection and loss of thieves                       | Provides information about the target's current science levels.                                                                                            |
| `ROB_THE_GRANARIES` | Steals `FOOD` from the target                                                                           |  ?                                                  | Potential for detection, loss of thieves, and triggering defenses | Steals a portion of the target's `FOOD` reserves.                                                                                                           |
| `ROB_THE_VAULTS`    | Steals `GOLD` from the target                                                                            |  ?                                                  | Potential for detection, loss of thieves, and triggering defenses | Steals a portion of the target's `GOLD` reserves.                                                                                                           |
| `ROB_THE_TOWERS`    | Steals `RUNES` from the target                                                                           |  ?                                                  | Potential for detection, loss of thieves, and triggering defenses | Steals a portion of the target's `RUNES` reserves.                                                                                                           |
| `KIDNAPPING`       | Captures `PEASANTS` from the target                                                                       |  ?                                                  | Potential for detection, loss of thieves, and triggering defenses | Captures a number of `PEASANTS` from the target, reducing their `POPULATION`.                                                                                |
| `STEAL_HORSES`     | Steals `HORSES` from the target                                                                          |  ?                                                  | Potential for detection, loss of thieves, and triggering defenses | Steals a portion of the target's `HORSES`.                                                                                                                  |
| `PROPAGANDA`       | Reduces target's honor and loyalty                                                                       |  ?                                                  | Potential for detection and loss of thieves                       | Lowers the target province's honor and potentially causes unrest or rebellion.                                                                              |
| `SABOTAGE_WIZARDS` | Kills a number of target's `WIZARDS`                                                                       |  ?                                                  | Potential for detection and loss of thieves                       | Eliminates a portion of the target's `WIZARDS`, reducing their `WPA` and potentially disrupting spellcasting.                                                  |
| `DESTABILIZE_GUILDS`| Reduces target's `SPELL_DURATION_MODIFIER`                                                                |  ?                                                  | Potential for detection and loss of thieves                       | Decreases the duration of spells cast by the target.                                                                                                      |
| `ARSON`            | Destroys `BUILDINGS` on the target                                                                        |  ?                                                  | Potential for detection and loss of thieves                       | Destroys a number of random buildings on the target, reducing their production and potentially other benefits.                                                  |
| `GREATER_ARSON`    | Destroys more `BUILDINGS` than Arson                                                                     |  ?                                                  | Potential for detection and loss of thieves                       | Destroys a larger number of random buildings compared to `ARSON`.                                                                                         |
| `NIGHT_STRIKE`     | Kills a percentage of target's troops                                                                        |  ?                                                  | Potential for detection and loss of thieves                       | Eliminates a portion of the target's military units.                                                                                                       |
| `INCITE_RIOTS`     | Reduces target's `POPULATION_GROWTH_MODIFIER`                                                               |  ?                                                  | Potential for detection and loss of thieves                       | Decreases the target's population growth rate, potentially hindering their development.                                                                       |
| `BRIBE_THIEVES`    | Reduces target's `STEALTH`                                                                                  |  ?                                                  | Potential for detection and loss of thieves                       | Decreases the target's `STEALTH`, making them more vulnerable to further thievery operations.                                                                |
| `BRIBE_GENERALS`    | Increases target's `MILITARY_CASUALTY_MODIFIER` (20% chance for +15% to all Military Casualties according to doc) |  ?                                                  | Potential for detection and loss of thieves                       | Makes the target's military units more vulnerable in combat.                                                                                             |
| `FREE_PRISONERS`    | Frees some of your captured `PRISONERS`                                                                  |  ?                                                  | Potential for detection and loss of thieves                       | Recovers some of your `PRISONERS` that were previously captured by the target.                                                                          |
| `ASSASSINATE_WIZARDS` | Kills a number of target's `WIZARDS`                                                                       |  ?                                                  | Potential for detection and loss of thieves                       | Eliminates a portion of the target's `WIZARDS`, reducing their `WPA` and potentially disrupting spellcasting.                                                  |

**Note:** The exact costs in thieves and stealth, as well as the risks and specific effects (e.g., the number of `WIZARDS` killed in `ASSASSINATE_WIZARDS`) likely depend on factors like relative `TPA`, `SCIENCE_POINTS[CRIME]`, and potentially other modifiers. These formulas need to be determined for a complete model.

## 14. Further Research and Refinements

This section summarizes the areas where we still need more information to complete the Utopia game model:

### 14.1 Formulas and Calculations

*   **Complete Formulas:**
    *   `BUILDING_EFFICIENCY`
    *   `OME` (Offensive Military Efficiency)
    *   `DME` (Defensive Military Efficiency)
    *   `POPULATION_GROWTH_MODIFIER`
    *   `SCIENCE_PRODUCTION_MODIFIER`
    *   `THIEVERY_OPERATIONS` (success rates, gains/losses)
    *   `SPELL` success rates
    *   `DRAGON` slaying
    *   `RITUAL` strength increase/decrease
    *   Attack Time calculation (NW difference modifier)
*   **Loss/Casualty Formulas:**
    *   `MILITARY_CASUALTY_MODIFIER`
    *   `THIEF_LOSS_MODIFIER`
    *   Resource losses when attacked
    *   Building losses from Arson/Greater Arson/Tornadoes
*   **Define "Base Values":**
    *   Base `INCOME_RATE`
    *   Base `BIRTH_RATE`
    *   Base `SPELL_DAMAGE_MODIFIER` values
    *   Base `WIZARD_PRODUCTION_MODIFIER`

### 14.2 Missing Game Elements and Interactions

*   **Espionage Operations:**
    *   Costs, risks, and specific effects of each espionage operation.
*   **Thievery Operations:**
    *   Formulas for the effects of `DESTABILIZE_GUILDS`, `INCITE_RIOTS`, `BRIBE_THIEVES`, `BRIBE_GENERALS`, and `FREE_PRISONERS`.
    *   Formulas for success rates and gains/losses of each thievery operation.
*   **Dragons:**
    *   Specific effects of each dragon type on the target kingdom.
    *   Dragon-slaying strength calculation per unit type.
*   **Rituals:**
    *   Base mana cost for casting a ritual.
    *   Contribution of each successful cast to ritual completion.
    *   Formula for ritual strength increase/decrease.
*   **Multi-Attack Protection (MAP):**
    *   Formula or detailed mechanics of `MAP`.
    *   Casualty reduction amount.
    *   Trigger conditions.
    *   Duration.
*   **Honor:**
    *   Formulas for gaining and losing honor.
    *   Specific values for how honor affects `OME` and `DME`.
*   **Relations:**
    *   Table specifying how different relation levels modify attack gains, spell/thievery effects, etc.
*   **Stances:**
    *   Limitations on changing stances (e.g., cooldown period).
*   **Kingdom Networth Factor:**
    *   Clarify if the average province size refers to land size or another metric.
*   **War:**
    *   Effects of being in a `WAR` on attack gains and other mechanics (beyond those already mentioned).
*   **Building/Specialist Credits from Attacks:**
    *   Formulas for calculating `BUILDING_CREDITS` and `SPECIALIST_CREDITS` gained from attacks.

### 14.3 Clarifications and Details

*   **Building Production:**
    *   Specify base production rates for each building that produces resources.
*   **Unit Costs:**
    *   Provide base costs (gold, resources) for training each military unit type.
*   **Spell Costs:**
    *   Specify the rune cost for each spell.
*   **Spell Duration:**
    *   Provide the base duration for each spell before modifiers.
*   **Spell and Thievery Operation Success Rates:**
    *   Explain how success rates are calculated. What factors influence them?
*   **Interaction Effects:**
    *   Describe in more detail how certain spells interact with each other (e.g., Droughts canceling Storms).
*   **Rounding:**
    *   Specify how rounding is applied in calculations (e.g., always round down, round to nearest integer, etc.).
*   **Limits:**
    *   Clarify any limits on modifiers (e.g., maximum `BUILDING_EFFICIENCY_MODIFIER`, minimum `ATTACK_TIME_MODIFIER`, etc.).

### 14.4 Additional Considerations

*   **Retired Mechanics:** Briefly mention any retired mechanics that might still be relevant for understanding the current game, like the removed stances.
*   **Game Version:** Specify the game version or age that this documentation applies to, as mechanics might change with updates.

By continuing to gather this missing information and refine the formulas, we can build an increasingly accurate and comprehensive model of the Utopia game. Remember to always cross-reference your information with in-game observations, official announcements, and community discussions.
