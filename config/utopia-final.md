**Utopia Game Mechanics**

*   **Provinces:** Basic game unit. Each has a race, personality, population, buildings, resources, science, and military.
*   **Race & Personality:** Each province has one of each. They modify base values, available spells, unit stats, and provide unique abilities (passive or active).
*   **Population & Units:** Population consists of peasants, soldiers, specialists, elites, thieves, and wizards. Units consume population space except for horses, prisoners, and mercenaries.
*   **Buildings:** Provide resource capacity, special effects, or both. Building efficiency affects production.
*   **Resources:** Gold, runes, food, peasants, and science points/books.
*   **Science:** Allocate science points to categories (Economy, Military, Arcane Arts) to modify province properties.
*   **Magic:** Spells (race/personality dependent) modify values temporarily. Cast on self or others.
*   **Thievery:** Actions against other provinces, similar to spells.
*   **Attacking/Defending:** Provinces interact through attacks, modified by offense, defense, protection, and the new Hostility Meter.
*   **Dragons:** Kingdom-wide, sent to disrupt other kingdoms for 48 ticks unless slain.
*   **Rituals:** Kingdom-wide bonuses, activated by casting spells.
*   **Stances:** Kingdom-wide declarations affecting gains, production, and military stats.
*   **Abilities:** Unique racial and personality abilities, either passive (always active) or active (activated by the player, with a cooldown).


**Races (Age of Ember and Frost)**

| Race        | Bonuses                                                                                                    | Penalties                                         | Soldier | Off. Spec | Def. Spec | Elite   | Merc | Prisoner | Horse | Unique Ability                                                                                                                                                                                                |
| :---------- | :--------------------------------------------------------------------------------------------------------- | :------------------------------------------------ | :------ | :-------- | :-------- | :------ | :--- | :------- | :---- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Avian       | -25% Attack Time, +60% Birth Rate, Immunity to Ambush, Access to Greater Protection, Clear Sight           | +15% Mil. Casualties, No Stables/Horses            | 3/0     | 11/0      | 0/10      | 14/3    | 8/0  | 8/0      | n/a   | **Skybound Strike (Active):** Next attack auto-succeeds, no off. losses. Gains reduced based on offense vs. defense (e.g., 75% break = 75% gains). Cooldown: 24 Utopian Days.                            |
| Dark Elf    | No Rune Cost (excl. Ritual), +15% Magic Effectiveness, Can Train Thieves w/ Spec. Credits, Access to Blizzard, Guile, Mage's Fury, Fools Gold, Magic Ward, Invisibility | +30% Exploration Costs, +30% Mil. Wages          | 3/0     | 11/0      | 0/10      | 10/10   | 8/0  | 8/0      | 2/0   | **Shadow Surge (Active):** Offensive spells have 20% chance to recast for free for 6 Utopian Days. Cooldown: 24 Utopian Days.                                                                           |
| Dwarf       | +30% Building Efficiency, -50% Construction Time, Access to Miner’s Mystique                              | Cannot use Accelerated Construction, +100% Food Consumption | 3/0     | 10/0      | 0/10      | 13/6    | 8/0  | 8/0      | 2/0   | **Earthshaker (Active):** Next successful attack razes all Forts and 5% of other buildings. Cooldown: 24 Utopian Days.                                                                                     |
| Elf         | +25% Magic Effectiveness, -20% Mil. Casualties, Access to Chastity, Mist, Pitfalls, Wrath                    | +20% Mil. Wages, No Dungeons/Prisoners            | 3/0     | 10/0      | 0/11      | 12/4    | 8/0  | 8/0      | 2/0   | **Mana Well (Active):** Restores 50% of caster's mana. Cooldown: 24 Utopian Days.                                                                                                                          |
| Faery       | +25% Off. Spell Duration, +25% Self Spell Duration, +1 Mana/Tick in War, Access to a lot of spells           | -10% Population                                  | 3/0     | 10/0      | 0/10      | 4/12    | 8/0  | 8/0      | 2/0   | **Ethereal Mirage (Passive):** Mystic Vortex removes all active spells from the target.                                                                                                                      |
| Halfling    | +15% Population, +25% Thievery Effectiveness, -10% Mil. Training Time, Access to Quick Feet, Town Watch, Vermin | -25% Birth Rate, +15% Military Wages               | 3/0     | 9/0       | 0/9       | 11/5    | 8/0  | 8/0      | 2/0   | **Sneak Attack (Active):** Thievery operations incur no losses for 1 Utopian Day. Cooldown: 24 Utopian Days.                                                                                                |
| Human       | +30% Science Efficiency, +15% Income, Access to Aggression, Fountain of Knowledge, Greater Protection, Reflect Magic | -80% Magic Effectiveness on Defense, No Libraries | 3/0     | 10/0      | 0/10      | 11/5    | 8/0  | 8/0      | 3/0   | **Interest (Active):** Provides an additional extreme activity bonus. Cooldown: 24 Utopian Days.                                                                                                            |
| Orc         | +30% Battle Gains, -50% Draft Cost, Train Elites w/ Spec. Credits, Access to Aggression, Bloodlust             | -20% Credits on attacks, -10% Building Efficiency | 3/0     | 11/0      | 0/9       | 15/4    | 8/0  | 8/0      | 2/0   | **Warlord's Fury (Passive):** Successful attacks plunder +15% resources (gold, runes, food), return 25% of mil. casualties.                                                                                  |
| Dryad       | +10% Off. Mil. Efficiency, +10% Def. Mil. Efficiency, Wages are "always paid"                               | -30% Thievery effectiveness (TPA), -15% income    | 3/0     | 10/0      | 0/10      | 13/5    | 8/0  | 8/0      | 2/0   | **Roots of Ruin (Passive):** Successful attacks destroy 1% of target's entire population (peasants, thieves, wizards, military).                                                                                |
| Gnome       | +25% Birth Rates, +25% Enemy Mil. Casualties, +20% Population, Access to Town Watch                           | +60% damage from all magic sources                | 3/0     | 9/0       | 0/9       | 9/9     | 8/0  | 8/0      | 2/0   | **Natures Feast (Active):** Next successful attack depletes target's food reserves to zero. Cooldown: 24 Utopian Days. |

**Personalities (Age of Ember and Frost)**

| Personality   | Bonuses                                                                                                                                                                                  | Starts With                                      | Unique Ability                                                                                                                                                                            |
| :------------ | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----------------------------------------------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Artisan       | +50% Building Capacity (Homes, Stables, Dungeons), +50% Building Production (Banks, Farms, Stables, Towers), +50% Building Credits in Combat, +100% Espionage (double cost), -20% Building Damage (Arson, Greater Arson, Tornado, Raze), +20% Artisan Science | +600 Soldiers, +600 Spec. Credits, +200 Building Credits | **Masterful Craftsmanship (Passive):** 25% chance to receive 50% of a building's cost back as building credits upon construction.                                                              |
| The Necromancer        | +15% Wizard effectiveness (WPA), Access to Nightmares                                                 | +500 Wizards, +500 Specialist Credits                  |      **Dark pact (Passive):** After each successful attack, The Necromancer converts: 10% of the enemy's casualties into Wizards, 10% into soldiers, 20% into Peasants. Minimum gain of 1 of each per a successful attack.                                                                                                                                                            |
| Cleric        | +1 Elite Def, +1 Def. Spec. Strength, -1 Self Spell Mana Cost, -25% Damage from Instant Spells, Access to Salvation, Revelation, Divine Shield, Illuminate Shadows                         | +800 Soldiers, +800 Spec. Credits             | **Divine Favour (Passive):** 50% chance to double self-spell duration.                                                                                                                     |
| Heretic       | +50% Wizard Production, -50% Thieves lost on ops, +25% Thievery Efficiency, +10% Magic Efficiency, Access to Meteor Showers, Greater Arson                                                     | +400 Wizards, +400 Thieves                    | **Blasphemous Might (Passive):** After a failed offensive spell cast, recover 5% Stealth.                                                                                                     |
| Mystic        | +75% Guilds, +1 Mana Recovery/Tick, Access to Magic Ward, Meteor Showers, Mage’s Fury, Mind Focus, Chastity, Fools Gold, +25% Channeling Science                                               | +800 Wizards                                  | **Righteousness Fury (Passive):** Gains access to the Spells Righteous Aggressor and Righteous Defender, which convert off/def spec to elites on successful attacks/defense, respectively. |
| Rogue         | +80% Thieves' Dens, +1 Stealth Recovery/Tick, Access to All Thievery, +25% Crime Science                                                                                                   | +800 Thieves                                  | **Shadows in the Night (Passive):** +25% Sabotage damage if the operation is performed with 50% or more Stealth.                                                                              |
| Tactician     | -15% Attack Time, No Thief losses on intel, +25% Credits in Combat, +25% Siege Science                                                                                                       | +800 Soldiers, +800 Spec. Credits             | **Dragons Wrath (Passive):** When attacking, 10% of your raw offense also deals damage to the dragon.                                                                                         |
| War Hero      | +30% Honor Gains, -30% Honor Losses, +5% Mil. Efficiency in War, Converts Spec. to Elites on Trad. March                                                                                     | +800 Soldiers, +800 Spec. Credits             | **War Trophies (Active):** Double honor bonuses for 12 ticks. Cooldown: 24 Utopian Days.                                                                                                       |
| Warrior       | +1 General, +4 Merc/Prisoner Strength, -50% Merc Cost, +25% Tactics Science                                                                                                               | +800 Soldiers, +800 Spec. Credits             | **Indomitable Spirit (Active):** +5% Military Efficiency for 6 ticks. Cooldown: 24 Utopian Days.                                                                                             |

**Units**

| Unit                  | Offense | Defense | Cost     | Notes                                                                                                                                        |
| :-------------------- | :------ | :------ | :------- | :------------------------------------------------------------------------------------------------------------------------------------------- |
| Peasants              | 0       | 0       |          | Work and generate income.                                                                                                                   |
| Soldiers              | 3       | 0       | Varies   | Basic unit, can be trained into Specialists, Elites, or Thieves.                                                                            |
| Wizards               | 0       | 0       |          | Required to cast spells.                                                                                                                    |
| Thieves               | 0       | 0       | 500gc    | Conduct Thievery operations.                                                                                                                |
| Horses                | 2       | 0       |          | Add raw offensive points.                                                                                                                   |
| Prisoners             | 8       | 0       |          | Taken from enemy casualties, fill jobs, produce gold.                                                                                       |
| Mercenaries           | 8       | 0       | 300gc    |                                                                                                                                             |
| Defensive Specialist | 0       | 10      | 350gc    | Defend only.                                                                                                                                 |
| Offensive Specialist | 10      | 0       | 350gc    | Attack only.                                                                                                                                 |
| Elites                | Varies  | Varies  | Varies   | Vary by race.                                                                                                                                |

**Unit Formulas**

*   **Soldiers Drafted/Tick:** `Peasants * Draft Speed * Race Bonus * Personality Bonus * Patriotism * Affluent Ritual * Sloth * Dragon * Heroism Science Effect`
*   **Draft Cost:** `Current Draft Level Factor * Draft Rate * Race Bonus * Personality Bonus * Armouries Mod * Heroism Science Effect * Sloth Effect`
*   **Training Cost:** `Unit Cost * Race Bonus * Armouries Bonus`
*   **Full Training Time:** `24 * Race Bonus * Personality Bonus * MAX(Inspire Army, Hero's Inspiration) * Valor Science Effect * Haste Ritual * Training Grounds Bonus`

**Buildings**

| Building         | Capacity        | Flat Rate Base                 | Percent Base Effect                      | Percent Effect Max | Living Space | Jobs | Description                                                                                                |
| :--------------- | :-------------- | :----------------------------- | :--------------------------------------- | :----------------- | :----------- | :--- | :--------------------------------------------------------------------------------------------------------- |
| Barren Land      | 15 People       | 2 Bushels                      |                                          |                    |              |      | Houses 15 people, produces 2 bushels/hour                                                                 |
| Homes            | 10 People       | 0.3 Peasants†                  |                                          |                    |              |      | Houses 10 people, generates peasants/day                                                                   |
| Farms            | 60 Bushels      |                                | 25                                       | 25                 |              |      | Produces 60 bushels/day                                                                                   |
| Mills            |                 |                                | 4%, 3%, 2%                               | 100%, 75%, 50%     |              |      | Decreases build/exploration costs                                                                          |
| Banks            | 25gc            |                                | 1.5%                                     | 37.5%              |              |      | Produces 25gc/day, increases income                                                                        |
| Training Grounds |                 |                                | 1.5%, 1%                                 | 37.5%, 25%         |              |      | Increases off. mil. efficiency, reduces training time                                                      |
| Armouries        |                 |                                | 2%, 2%, 1.5%                             | 50%, 50%, 37.5%    |              |      | Decreases draft/wage/training costs                                                                         |
| Military Barracks |                 |                                | 1.5%, 2%                                 | 37.5%, 50%         |              |      | Lowers attack time, reduces merc costs                                                                     |
| Forts            |                 |                                | 1.5%                                     | 37.5%              |              |      | Increases def. mil. efficiency                                                                            |
| Castles          |                 |                                | 2%, 2%                                   | 50%, 50%           |              |      | Decreases resource/honor losses when attacked                                                              |
| Hospitals        |                 |                                | 3%, 3%, 2%                               | 75%, 75%, 50%      |              |      | Decreases mil. losses, chance of curing plague, increases birth rate                                        |
| Guilds           |                 | 0.02 Wizards†                  |                                          |                    |              |      | Trains wizards/day, increases spell duration                                                               |
| Towers           | 12 Runes        |                                |                                          |                    |              |      | Produces 12 runes/day                                                                                     |
| Thieves Dens     |                 |                                | 3.6%, 3%                                 | 90%, 75%           |              |      | Lowers thievery losses, bonus to TPA                                                                       |
| Watch Towers     |                 |                                | 1.5%, 2.5%                               | 37.5%, 62.5%       |              |      | Chance of catching enemy thieves, reduces damage                                                            |
| Universities     |                 |                                | 1.5%, 1%                                 | 37.5%, 25%         |              |      | Increases scientist spawn rate/book generation                                                              |
| Libraries        |                 |                                | 1%                                       | 25%                |              |      | Increases science efficiency                                                                               |
| Stables          | 80 Horses       | 2 Horses                       |                                          |                    |              |      | Houses war horses, increases production rate                                                                |
| Dungeons         | 30 Prisoners    |                                |                                          |                    |              |      | Houses prisoners of war                                                                                    |

**Building Formulas**

*   **Construction Time:** `16 * Racial Mod * Personality Mod * Builders Boon * Double Speed * War Bonus * Ritual Mod * Artisan Science Mod`
*   **Construction Costs:** `0.05 * (land + 10000) * Race Mod * Personality Mod * Mills Mod * Double Speed * Ritual Mod * Artisan Science Mod`
*   **Raze Costs:** `(300 + (0.05 * land)) * Artisan Science Mod * Race Mod * Personality Mod`
*   **Building Efficiency:** `(0.5 * (1 + % Jobs Performed)) * Race * Personality * Tools Science * Dragon * Blizzard`

**Science**

| Science Category | Type                  | Effect                                                      | Multiplier |
| :--------------- | :-------------------- | :---------------------------------------------------------- | :--------- |
| Economy          | Alchemy               | Income                                                      | ~0.0724    |
|                  | Tools                 | Building Effectiveness                                      | ~0.0524    |
|                  | Housing               | Population Limits                                           | ~0.0262    |
|                  | Production            | Food & Rune Production                                      | ~0.2172    |
|                  | Bookkeeping           | Wage Reduction                                              | ~0.068     |
|                  | Artisan               | Construction Time/Cost/Raze Cost Reduction                   | ~0.0478    |
| Military         | Strategy              | Defensive Military Efficiency                               | ~0.0367    |
|                  | Siege                 | Battle Gains                                                | ~0.0262    |
|                  | Tactics               | Offensive Military Efficiency                               | ~0.0367    |
|                  | Valor                 | Reduced Mil. Train Time, Increased Dragon Slaying Strength | ~0.0582    |
|                  | Heroism               | Draft Speed & Draft Costs Reduction                         | ~0.0418    |
|                  | Resilience            | Reduced Military Casualties                                 | ~0.0489    |
| Arcane Arts      | Crime                 | Thievery Effectiveness                                      | ~0.1557    |
|                  | Channeling            | Magic Effectiveness                                         | ~0.1875    |
|                  | Shielding             | Reduced Damage from Enemy Thievery/Magic Instant Ops        | ~0.0314    |
|                  | Sorcery               | Increased Magic Instant Damage                              | ~0.0314    |
|                  | Cunning               | Increased Thievery Operation Damage                         | ~0.0314    |
|                  | Finesse               | Reduced Wizards/Thieves lost on Failed Spells/Ops          | ~0.0965    |

*   **Science Bonus:** `(# of Books in Type)^(1/2.125) * Science Multiplier * Race Mod * Personality Mod * Amnesia Effect * Scientific Insights Mod * Libraries Mod`
*   **Scientists:** Start with a set number, assigned to categories. Produce books based on rank.
*   **Scientist Spawn Rate:** `2 * Race Mod * Universities Effect * Revelation Mod`
*   **Scientist Ranks:** Recruit (0-1439 exp, 60 books), Novice (1440-5279 exp, 80 books), Graduate (5280-12479 exp, 100 books), Professor (12480+ exp, 120 books)

**Magic**

*   **% Guilds:** `Total Number of Guilds / Total Acres`
*   **Spell Duration:** Determined by % Guilds, up to a maximum (usually 20% Guilds).
*   **Raw WPA:** `Number of Wizards / Acres`
*   **Offensive Modified WPA:** `Raw WPA * Channeling Science * Race Mod * Honor Mod * Mage's Fury Mod * Dragon Mod`
*   **Defensive Modified WPA:** `Raw WPA * Channeling Science * Race Mod * Honor Mod * Mage's Fury Mod * Magic Shield Mod * Dragon Mod`
*   **Runes Cost:** `ROUNDDOWN((0.6 * Size + 200) * Spell Cost Multiplier * 1.5)`

**Magic - Self Spells**

| Spell               | Description                                                                                               | Spell Bonus                                         | Difficulty | Cost Multiplier | Duration (Avg) |
| :------------------ | :-------------------------------------------------------------------------------------------------------- | :-------------------------------------------------- | :--------- | :-------------- | :------------- |
| Minor Protection    | Increases Def. Mil. Efficiency by 5% (multiplicative). Stacks with Greater Protection.                   | 105%                                                | 0.017      | 0.35            | 12             |
| Greater Protection  | Increases Def. Mil. Efficiency by 5%. Stacks (multiplicative) with Minor Protection.                      | 105%                                                | 0.019      | 0.45            | 24             |
| Magic Shield        | Increases Def. Magic Effectiveness by 20%.                                                              | 120%                                                | 0.022      | 0.5             | 14             |
| Mystic Aura         | Makes the next spell cast on you fail automatically.                                                      |                                                     | 0.022      | 0.5             | N/A            |
| Fertile Lands       | Increases Food production by 25%                                                                         | 125%                                                | 0.022      | 0.5             | 16             |
| Nature's Blessing   | Grants immunity to Storms/Droughts. 33% chance to cure Plague on cast.                                   | 100%                                                | 0.023      | 0.6             | 16             |
| Love & Peace        | Increases Birth Rate by +0.85%, War Horse production by 40%.                                              | +0.85%                                              | 0.024      | 0.7             | 14             |
| Divine Shield       | Decreases Instant Spell Damage taken by 20%.                                                              | 80%                                                 | 0.024      | 0.8             | 12             |
| Quick Feet          | Decreases Attack Time by 10%.                                                                              | 90%                                                 | 0.026      | 0.8             | 2              |
| Builder's Boon      | Reduces Construction Time by 25%.                                                                         | 75%                                                 | 0.027      | 1               | 12             |
| Inspire Army        | Reduces wages by 15%, training time by 20%.                                                               | 85%/80%                                             | 0.028      | 1.1             | 12             |
| Hero's Inspiration  | Reduces Mil. Wages by 30%, Training Time by 30%.                                                         | 70%/70%                                             | 0.017      | 1.1             | 14             |
| Scientific Insights | Increases Science Efficiency by 10%.                                                                      | 110%                                                | 0.019      | 1.25            | 9              |
| Anonymity           | Hides province name in next attack, reveals Kingdom location. No Honor, -15% attack gains, Ambush Immunity. |                                                     | 0.04       | 1.3             | N/A            |
| Illuminate Shadows  | Reduces Damage from Thievery Ops by 20%.                                                                  | 80%                                                 | 0.02       | 1.3             | 8              |
| Salvation           | Reduces All Mil. Casualties by 15%.                                                                       | 85%                                                 | 0.02       | 1.3             | 8              |
| Wrath               | Increases Mil. Casualties of attacking provinces by 20%.                                                  | 120%                                                | 0.021      | 1.35            | 8              |
| Invisibility        | Increases Off. Thievery Effectiveness by 10%. Reduce Thieves lost during ops by 20%.                       | 110%/80%                                            | 0.032      | 1.35            | 12             |
| Clear Sight         | 25% chance to catch enemy thieves.                                                                        | 75%                                                 | 0.033      | 1.4             | 16             |
| Mage's Fury         | Magic Effectiveness +25% Offense, -25% Defense.                                                            | 125%/75%                                            | 0.033      | 1.4             | 6              |
| War Spoils          | Take control of Land gained on Attacks.                                                                   |                                                     | 0.034      | 1.45            | 4              |
| Mind Focus          | Increases Wizard production by 25%.                                                                         | 125%                                                | 0.033      | 1.475           | 12             |
| Fanaticism          | Increases Off. Mil. Efficiency by 5%. Decreases Def. Mil. Efficiency by 5%.                               | 105%/95%                                            | 0.034      | 1.5             | 6              |
| Guile               | Increases Instant Spell Damage and Sabotage Damage by 10%.                                                 | 110%                                                | 0.039      | 1.5             | 8              |
| Revelation          | Increases Scientist Spawn Rate by 20%.                                                                    | 120%                                                | 0.035      | 1.55            | 10             |
| Fountain of Knowledge | Increases Science Book production by 10%.                                                                 | 110%                                                | 0.033      | 1.55            | 10             |
| Tree of Gold        | Instantly generates 26.66% to 53.33% of daily Income.                                                     |                                                     | 0.025      | 1.6             | N/A            |
| Town Watch          | Every 5 Peasants defend with 1 strength, high Casualties.                                                  |                                                     | 0.037      | 1.6             | 10             |
| Aggression          | Soldiers are +2/-2 strength.                                                                               | +2/-2                                               | 0.037      | 1.625           | 12             |
| Miner's Mystique    | Peasants generate extra 0.3gc/tick.                                                                        | +0.3                                                | 0.038      | 1.65            | 14             |
| Ghost Workers       | Lowers Jobs required for max efficiency by 25%.                                                            | 75%                                                 | 0.038      | 1.65            | 14             |
| Animate Dead        | In next defense, 50% Mil. Casualties returned as Soldiers.                                                | 50%                                                 | 0.038      | 1.7             | N/A            |
| Mist                | 10% lower resource losses in battle.                                                                       | 90%                                                 | 0.038      | 1.7             | 4              |
| Reflect Magic       | 20% chance to reflect Off. Spells back.                                                                    | 20%                                                 | 0.4        | 1.8             | 12             |
| Shadowlight         | Reveals origin Province for next Thievery Op on you. Next Sabotage fails.                                 |                                                     | 0.042      | 1.9             | N/A            |
| Bloodlust           | Increases Off. Mil. Efficiency by 10%, Enemy Mil. Casualties by 15%, but suffer +15% Casualties.            | 110% 115%/115% | 0.04       | 2.0             | 6              |
| Patriotism          | Increases draft speed by 30%. Lowers Propaganda Damage by 30%.                                            | 130%/70%                                            | 0.045      | 2.0             | 12             |
| Paradise            | Creates a few acres of land. Consumes explore pool. Not in War/Protection.                               |                                                     | 0.5        | 3.0             | N/A            |
| Cast Ritual         | Increases progress towards chosen ritual. Only on Ritual tab.                                             | N/A                                                 | 6.0        | N/A             | N/A            |
| Righteous Aggressor | Converts off spec to elites on successful attack                                                          |                                                     |            |                 |                 |
| Righteous Defender | Converts def spec to elites when attacked                                                                 |                                                     |            |                 |                 |

**Magic - Offensive Spells**

| Spell           | Description                                                                                | Spell Bonus | Difficulty | Cost Multiplier | Duration (Avg) |
| :-------------- | :----------------------------------------------------------------------------------------- | :---------- | :--------- | :-------------- | :------------- |
| Crystal Ball    | Reveals target's Throne, Troops, Resources. Accurate. Costs 1 Mana.                       |             | 0.004      | 0.35            | N/A            |
| Storms          | Kills Peasants (1.5% of Pop). Cancels Drought. +15% Tornado damage.                         |             | 0.009      | 0.8             | 12             |
| Drought         | Food production -25%. Draft Speed -15%. Kills Horses. Cancels Storms. +15% Arson damage.    | 75%/85%     | 0.01       | 1.0             | 12             |
| Magic Ward      | Unfriendly/Hostile/War: Increases target's Rune Costs by 100%.                             | 200%        | 0.02       | 1.15            | 6              |
| Gluttony        | Increases Food required by 25%.                                                            | 125%        | 0.014      | 1.2             | 12             |
| Vermin          | Destroys ~50% of target's Food.                                                             | 50%         | 0.014      | 1.275           | N/A            |
| Greed           | Increases Mil. Wages/Draft Costs by 25%.                                                   | 125%        | 0.016      | 1.3             | 12             |
| Expose Thieves  | Unfriendly/Hostile/War: Reduces target's Stealth by 5%/Tick.                               | 0.95        | 0.024      | 1.4             | 6              |
| Chastity        | Reduces Birth Rate by 50%.                                                                 | 50%         | 0.024      | 1.4             | 6              |
| Sloth           | Unfriendly/Hostile/War: Reduces target's Draft Rate by 50%. Increases Draft Cost by 100%. | 50%/200%    | 0.024      | 1.4             | 6              |
| Fireball        | Unfriendly/Hostile/War: Kills ~6% of Peasants.                                              | 94%         | 0.02       | 1.5             | N/A            |
| Fool's Gold     | Unfriendly/Hostile/War: Destroys up to 25% of target's Gold.                               | 75%         | 0.02       | 1.5             | N/A            |
| Lightning Strike | Unfriendly/Hostile/War: Destroys up to 65% of Runes.                                       |             | 0.02       | 1.55            | N/A            |
| Explosions      | 50% chance that aid is reduced to 55-80% if either sender or receiver has explosions.       |             | 0.02       | 1.8             | 12             |
| Blizzard        | Reduces building effectiveness by 10%.                                                     | 90%         | 0.02       | 1.8             | 6              |
| Pitfalls        | Causes opponent to suffer +15% Def. Mil. Casualties.                                        | 115%        | 0.024      | 1.8             | 12             |
| Nightmares      | Unfriendly/Hostile/War: ~1.5% of Troops put back in Training over 8 Ticks.                  | 98.5%       | 0.039      | 2.2             | N/A            |
| Tornadoes       | Unfriendly/Hostile/War: Destroys several Buildings.                                         |             | 0.03       | 2.5             | N/A            |
| Mystic Vortex   | Unfriendly/Hostile/War: 50% chance to remove each active spell.                             | 50%         | 0.031      | 2.8             | N/A            |
| Amnesia         | War: Temporarily removes ~5% of allocated Science Books. Return over 48 ticks.              | 0.95        | 0.034      | 2.8             | N/A            |
| Meteor Showers  | Hostile/War: Kills Peasants/Troops every Tick.                                               |             | 0.04       | 3.5             | 8              |
| Abolish Ritual  | Unfriendly/Hostile/War: Reduces ritual strength by 2%. Max 10 casts on a province.           | -2%         | 0.032      | 2.8             | N/A            |
| Land Lust       | Unfriendly/Hostile/War: Captures 1-1.25% of Acres.                                           |             | 0.04       | 4               | N/A            |

**Thievery**

*   **Raw TPA:** `Number of Thieves / Acres`
*   **Mod TPA:** `Raw TPA * Invisibility * Crime Science * Racial Mod * Thieves Dens Bonus * Honor Bonus * Ritual Bonus`
*   **Stealth:** Varies by operation and relations, minimum 5% to send thieves. Increases by 3% per hour.
*   **Optimal Thieves to Send:** `(Opponents' Resources * Max % Of Total) / Gains Per Thief / Racial Mod`
*   **Thievery Yield:** `Thieves Sent * Relative Size * Gains Per Thief * (1 - Resources Lost) * Racial Mod * Personality Mod * Science Mod * Guile`
*   **Relative Size:**  `MIN(Target Networth / Self Networth, Self Networth / Target Networth) + War Bonus`

**Thievery Table**

| Operation        | Max % of Total (Out of War) | Gains Per Thief (Out of War) | Resources Lost (Out of War) | Max % of Total (War) | Gains Per Thief (War) | War Bonus | Resources Lost (War) |
| :--------------- | :-------------------------- | :--------------------------- | :-------------------------- | :------------------- | :-------------------- | :-------- | :------------------- |
| Rob Granaries    | 37.5%                       | 53.55                        | 10%                         | 40%                  | 96                    | 25%       | 10%                  |
| Rob Vaults       | 5.20%                       | 40                           | 10%                         | 13.6%                | 64                    | 25%       | 10%                  |
| Rob Towers       | 21.3%                       | 17.5                         | 10%                         | 28%                  | 16.8                  | 25%       | 10%                  |
| Kidnap Peasants  | 1.85%                       | 0.132                        | 20%                         | 4%                   | 0.285                 | 25%       | 20%                  |
| Steal Horses     | 12.75%                      | 9.4%                         | 50%                         | 13.6%                | 10%                   | 25%       | 50%                  |
| Night Strike     | 18%                         | 90%                          | 0                           | 16%                  | 80%                   | 25%       | 0                    |

**Attacking & Defending**

*   **Base Military Efficiency:** `(33 + 67 * (Effective Wage Rate / 100)^0.25) * Ruby Dragon * Multi-Attack Protection Bonus`
*   **Offensive Military Efficiency (OME):** `(Base Mil. Efficiency + Training Grounds Bonus + Honor Bonus) * Science Bonus * Race Bonus * Personality Bonus * Fanaticism * Bloodlust * Ritual`
*   **Defensive Military Efficiency (DME):** `(Base Mil. Efficiency + Forts Bonus + Honor Bonus) * Science Bonus * Race Bonus * Personality Bonus * Minor Protection * Greater Protection * Fanaticism * Plague * Ritual`
*   **Raw Offense:** `(Soldiers * (Soldier Off + Aggression)) + (Off. Specs * Off Spec Attack) + (Elites * Elite Attack) + (Horses * War Horse Attack) + [(Mercs * Attack Value + Prisoners * Attack Value)]`
*   **Modified Offense:** `Raw Offense * (OME + General Bonus)`
    *   `General Bonus`: +5% per additional general over 1
*   **Attack Time:** `Base Attack Time * Race Bonus * Personality Bonus * Barracks Bonus * Quick Feet * Attack Type * War * NW Mod * Ritual Bonus`
    *   Base Attack Time: 14 hours (7 intra-kingdom)
    *   War Attack Speed: -15% after 12 hours of war
    *   Net Worth (NW) Mod: Longer attack time if target NW differs from yours (except intra-kingdom or in war)
*   **Minimum Offense to Win:** `Mod Offense > Mod Defense` (or 51% of opponent's defense for Conquest)
*   **Attack Gains:** `Target Resource * Attack Type * RPNW * RKNW * Multi-Attack Protection * Race Mod * Personality Mod * Castles Protection * Relations Mod * Stance Mod * Siege Science * Emerald Dragon * Attack Time Adjustment Factor * Ritual Bonus * Anonymity * Mist`
    *   Land attacks also reward Military/Building Credits.
    *   Raze, Massacre, Ambush have specific gain rules.
*   **Province Networth Factor:**
    *   `Relative Province Networth (rpnw) = Targets Networth / Self Networth`
    *   `Province Networth Factor = DEPEND (rpnw):`
        *   `rpnw < 0.567 = 0`
        *   `0.567 < rpnw < 0.9 = 3 * rpnw - 1.7`
        *   `0.9 < rpnw < 1.1 = 1`
        *   `1.1 < rpnw < 1.6 = -2 * rpnw + 3.2`
        *   `rpnw > 1.6 = 0`
*   **Kingdom Networth Factor:**
    *   `Relative Kingdom Networth (rknw) = Target Kingdom Avg. Prov. Networth / Self Kingdom Avg. Prov. Networth`
    *   `Kingdom Networth Factor = DEPEND (rknw):`
        *   `rknw < 0.5 = 0.8`
        *   `0.5 < rknw < 0.9 = rknw / 2 + 0.55`
        *   `rknw > 0.9 = 1`
*   **Attack Time Adjustment Factor:**

| Hours | Gains Modifier %        |
| :---- | :---------------------- |
| -2    | (-2 / base attack time) * 160% |
| -1    | (-1 / base attack time) * 150% |
| +1    | (1 / base attack time)  * 80%  |
| +2    | (2 / base attack time)  * 70%  |
| +3    | (3 / base attack time)  * 60%  |
| +4    | (4 / base attack time)  * 50%  |

*   **Attack Types:**
    *   **Traditional March:** Base 12% gains (capped at 20% of your/opponent's acres).
    *   **Ambush:** Returns 50% of acres lost, +15% mil. casualties, unaffected by gains modifiers.
    *   **Plunder:** Base 50% gold, 60% food/runes (max 1.75x base), -50% def. mil. casualties.
    *   **Learn:** Gains ~2% target's allocated books, ~30% unallocated, -50% def. mil. casualties. In war, temporarily removes ~35% allocated books.
    *   **Raze:** Destroys ~5% target's land (~30% buildings in war).
    *   **Conquest:** Base 6.8% land (full hit), decreased by relative offense vs. defense. Only within 5% NW range outside Hostile/War (unless attacker has Enhanced Conquest).
    *   **Massacre:** Base 9.5% peasants, 7.5% thieves, 5% wizards. ~3x efficiency in war.
*   **Raw Defense:** `(Defense Specs * Def Spec Points) + (Elites at Home * Elite's Defense) + (Soldiers * Sold Def Points * Aggression) + Townwatch`
*   **Mod Defense:** `MAX(Raw Defense * DME, Land)`
*   **Minimum Defense:** 1 defense per acre (except intra-kingdom).
*   **Military Casualties:** Base 6.5-8.5% offense, 5-6.5% defense. Modified by buildings, science, multi-attack protection, attack type, race, rituals, spells, operations, and dragons.

**Dragons**

*   Sent by kingdom Monarch/Stewards to disrupt other kingdoms.
*   Last 48 ticks or until slain.
*   Troops slay dragons with their offense value.
*   Thieves, wizards, and prisoners cannot be used to slay dragons.
*   Races, personalities, and Valor science can boost dragon slaying strength.
*   Send range: 20% smaller to 25% larger than kingdom's net worth (removed in war).
*   Cannot be funded/slain in protection.
*   Cancelled on EOWCF, regular/forced ceacefires.

**Dragon Types & Effects**

| Dragon Type | Effects                                                                                                                                                                    |
| :---------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Emerald     | +25% Military Losses in Combat, -20% Combat Gains, -40% Building/Specialist Credits Gained in Combat                                                                       |
| Ruby        | -15% Military Efficiency, +30% Military Wages, Lose 30% of new drafted Soldiers                                                                                           |
| Sapphire    | -30% Magic Effectiveness (WPA), -30% Thievery Effectiveness (TPA), +12.5% Instant Spell/Sabotage Damage Taken, -12.5% Instant/Sabotage Damage Dealt                        |
| Topaz       | -30% Building Efficiency, -25% Income, Destroys 4% of Buildings Instantly and Every 6 Ticks thereafter                                                                    |

**Dragon Costs & HP**

*   **Gold Cost:** `Dragon Type Cost Mod * Cost Metric`
*   **Food Cost:** `Dragon Type Cost Mod * Cost Metric * 0.2`
*   **Cost Metric:** `Target Kingdom NW * 0.656`

| Dragon Type | Cost Mod |
| :---------- | :------- |
| Sapphire    | 2        |
| Topaz       | 2        |
| Emerald     | 2.4      |
| Ruby        | 2.4      |

*   **Dragon HP:** `Dragon Type HP Mod * Relations Modifier * (Receiving Kingdom NW / 132)`

| Dragon Type | HP Mod  |
| :---------- | :------ |
| Sapphire    | 7.0125  |
| Topaz       | 7.0125  |
| Emerald     | 8.415   |
| Ruby        | 8.415   |

| Relations   | Relations Modifier |
| :---------- | :----------------- |
| None        | 0.5                |
| Unfriendly  | 0.5                |
| Hostile     | 0.75               |
| War         | 1                  |

**Rituals**

*   Kingdom-wide bonuses.
*   Started by Monarch/Steward.
*   Requires casting ritual spell 3x the number of provinces in the kingdom (min 15 provinces) within 48 hours.
*   Can be overcast for increased effects.
*   **Ritual Effectiveness:** `1 + (2 * SQRT(X - MAX(45, KD_provs * 3))) / 100` (where X = number of ritual casts, when X ≥ 3 * KD\_provs)
*   Strength diminishes by 0.25% per tick.
*   Costs 2% Mana per cast.
*   Lasts 48-120 hours.
*   Can be destroyed by Abolish Ritual spell (-2% strength per cast, max 10 casts per province).

**Ritual Types**

| Ritual    | Description                                                                                                  |
| :-------- | :----------------------------------------------------------------------------------------------------------- |
| Barrier   | -20% Damage from Enemy Instant Magic/Thievery, -10% Battle (Resource) Losses                                |
| Expedient | +10% Building Efficiency, -20% Military Wages, -20% Construction Cost                                       |
| Haste     | -10% Attack Time, -25% Training Time, -25% Construction Time                                                  |
| Havoc     | +20% Off. WPA, +20% Off. TPA, +10% Spell Damage, +10% Sabotage Damage                                       |
| Onslaught | +10% Off. Mil. Efficiency, +15% Enemy Mil. Casualties on Attacks                                           |
| Stalwart  | +5% Def. Mil. Efficiency, -20% Mil. Casualties                                                              |

**Stances**

*   Kingdom-wide declarations.
*   Controlled by Monarch/Steward.
*   Cannot be changed within 48 hours of last change.
*   Revert to Normal after 48 ticks (Aggressive/Peaceful).
*   Negated in war.
*   Cannot be changed during protection.

**Stance Types**

| Stance     | Effects                                                                                                                            |
| :--------- | :--------------------------------------------------------------------------------------------------------------------------------- |
| Normal     | No Effects                                                                                                                         |
| Aggressive | +10% Battle (Resource) Gains, +30% Credits in Combat, +15% Self Off. Mil. Casualties, +10% Instant Spell/Sabotage Damage, +20% Mil. Wages |
| Peaceful   | +15% Income, +20% Food/Rune Production, +20% Birth Rate, -15% Off. Mil. Efficiency, -20% Outgoing Instant Spell/Sabotage Damage   |
