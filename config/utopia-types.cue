package utopia

#BASE_RESOURCE:
	"GOLD" |
	"FOOD" |
	"RUNES" |
	"LAND" |
	"BUILDING_CREDITS" |
	"SPECIALIST_CREDITS" |
	"COMBAT_CREDITS" |
	"SCIENTISTS" |
	"MANA" |
	"STEALTH" |
	"POPULATION_SPACE" |
	"HORSES" |
	"PRISONERS" |
	"MERCENARIES"

#POPULATION_RESOURCE:
	"PEASANTS" |
	"SOLDIERS" |
	"OFFSPECS" |
	"DEFSPECS" |
	"ELITES" |
	"THIEVES" |
	"WIZARDS"

#RESOURCE: #BASE_RESOURCE | #POPULATION_RESOURCE

#SCIENCE: {
	ECONOMY:
		"ALCHEMY" |
		"TOOLS" |
		"HOUSING" |
		"PRODUCTION" |
		"BOOKKEEPING" |
		"ARTISAN"
	MILITARY:
		"STRATEGY" |
		"SIEGE" |
		"TACTICS" |
		"VALOR" |
		"HEROISM" |
		"RESILIENCE"
	ARCANE:
		"CRIME" |
		"CHANNELING" |
		"SHIELDING" |
		"SORCERY" |
		"CUNNING" |
		"FINESSE"
}

#BUILDING:
	"NONE" |
	"HOMES" |
	"FARMS" |
	"MILLS" |
	"BANKS" |
	"TRAINING_GROUNDS" |
	"ARMORIES" |
	"BARRACKS" |
	"FORTS" |
	"CASTLES" |
	"HOSPITALS" |
	"GUILDS" |
	"TOWERS" |
	"THIEVES_DENS" |
	"WATCH_TOWERS" |
	"UNIVERSITIES" |
	"LIBRARIES" |
	"STABLES" |
	"DUNGEONS"

#BASE_PROPERTY:
	"RACE" |
	"PERSONALITY"

#UNIT_DATA: {
	off?:                int
	def?:                int
	networth?:           float | 0.0
	cost?:               int | 0
	specialist_credits?: bool | false
}

#UNIT: {
	SOLDIER: #UNIT_DATA & {off: int | *3, networth: float | *0.75}
	OFFSPEC: #UNIT_DATA & {off: int | *10, networth: float | *4.0, specialist_credits: bool | *true, cost: int | *350}
	DEFSPEC: #UNIT_DATA & {def: int | *10, networth: float | *5.0, specialist_credits: bool | *true, cost: int | *350}
	ELITE: #UNIT_DATA & {}
	MERC: #UNIT_DATA & {off: int | *8}
	PRISONER: #UNIT_DATA & {off: int | *8, networth: float | *1.6}
	HORSE: #UNIT_DATA & {off: int | *2, networth: float | *0.6}
	THIEF: #UNIT_DATA & {cost: int | *500}
}

#DERIVED_PROPERTY:
	"NETWORTH" |
	"BUILDING_EFFICIENCY" |
	"OFFENSIVE_MILITARY_EFFICIENCY" |
	"DEFENSIVE_MILITARY_EFFICIENCY" |
	"WIZARDS_PER_ACRE" |
	"THIEVES_PER_ACRE" |
	"DRAFT_RATE" |
	"TRAINING_TIME" |
	"ATTACK_TIME" |
	"INCOME_RATE" |
	"BIRTH_RATE" |
	"FOOD_CONSUMPTION_RATE" |
	"SCIENCE_PRODUCTION_RATE" |
	"SPELL_DURATION_MODIFIER" |
	"SPELL_SUCCESS_MODIFIER" |
	"HORSE_PRODUCTION_RATE"

#FEATURE_PROPERTY:
	"ACCELERATED_CONSTRUCTION" |
	"ACCELERATED_TRAINING"

#RACE: {
	MILITARY: [#UNIT]
}

AVIAN: {
	MILITARY: #UNIT & {
		SOLDIER: {off: 4} 
		THIEF: {specialist_credits: true}
	}
}
