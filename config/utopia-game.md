# Utopia Game Mechanics

This document outlines the core mechanics of the Utopia game, including province properties, modifiers, and the effects of various game elements.

## 1. Province Properties

These are the fundamental attributes of each province.

```
# Basic Province Properties
LAND: Total land acres (affects multiple calculations)
NETWORTH: Total value of the province
BUILDING_CREDITS: Currency for constructing buildings
SPECIALIST_CREDITS: Currency for training military specialists
POPULATION: Total population
RACE:  Province race, index into RACE_LIST
PERSONALITY: Province personality, index into PERSONALITY_LIST
GOLD: Amount of gold
FOOD: Amount of food
RUNES: Amount of runes
MANA: Amount of mana
CURRENT_ATTACK_TARGET: (x, y) location or 0 if no target
CURRENT_ATTACK_TYPE: index into ATTACK_TYPES
STEALTH: Used for thievery operations
```

### Resources

```
RESOURCES = [
    GOLD,
    FOOD,
    RUNES,
    BUILDING_CREDITS,
    SPECIALIST_CREDITS,
    PEASANTS,
    SOLDIERS,
    OFFSPECS,
    DEFSPECS,
    ELITES,
    HORSES,
    PRISONERS,
    THIEVES,
    WIZARDS,
    SCIENTISTS,
    MANA,
    STEALTH
]

RESOURCE_AMOUNT[RESOURCE in RESOURCES]: Current amount of each resource
```

### Buildings

```
BUILDINGS = [
    HOMES,
    FARMS,
    MILLS,
    BANKS,
    TRAINING_GROUNDS,
    ARMORIES,
    BARRACKS,
    FORTS,
    CASTLES,
    HOSPITALS,
    GUILDS,
    TOWERS,
    THIEVES_DENS,
    WATCH_TOWERS,
    UNIVERSITIES,
    LIBRARIES,
    STABLES,
    DUNGEONS
]

BUILDING_AMOUNT[BUILDING in BUILDINGS]: Number of each building type
BUILDING_CAPACITY[BUILDING in BUILDINGS]: Maximum capacity (if applicable)
BUILDING_PRODUCTION_RATE[BUILDING in BUILDINGS]: Production rate (if applicable)
```

### Military Units

```
MILITARY_UNITS = [
    SOLDIERS,
    OFFSPECS,
    DEFSPECS,
    ELITES,
    MERCENARIES,
    PRISONERS,
    HORSES
]

MILITARY_UNIT_AMOUNT[UNIT in MILITARY_UNITS]: Number of each unit type
MILITARY_UNIT_OFFENSE[UNIT in MILITARY_UNITS]: Offensive strength of each unit type
MILITARY_UNIT_DEFENSE[UNIT in MILITARY_UNITS]: Defensive strength of each unit type
MILITARY_UNIT_WAGE[UNIT in MILITARY_UNITS]: Wage cost of each unit type
MILITARY_UNIT_COST[UNIT in MILITARY_UNITS]: Training cost of each unit type
MILITARY_UNIT_TRAINING_TIME[UNIT in MILITARY_UNITS]: Training time of each unit type
```

### Science

```
SCIENCE_CATEGORIES = [
    ECONOMY,
    MILITARY,
    ARCANE_ARTS
]

SCIENCE_TYPES[ECONOMY] = [
    ALCHEMY,
    TOOLS,
    HOUSING,
    PRODUCTION,
    BOOKKEEPING,
    ARTISAN
]

SCIENCE_TYPES[MILITARY] = [
    STRATEGY,
    SIEGE,
    TACTICS,
    VALOR,
    HEROISM,
    RESILIENCE
]

SCIENCE_TYPES[ARCANE_ARTS] = [
    CRIME,
    CHANNELING,
    SHIELDING,
    SORCERY,
    CUNNING,
    FINESSE
]

SCIENCE_POINTS[SCIENCE_TYPE in SCIENCE_TYPES]: Current science level in each type
SCIENCE_BOOKS[SCIENCE_CATEGORY in SCIENCE_CATEGORIES]: Number of books in each category
SCIENTIST_AMOUNT[SCIENCE_CATEGORY in SCIENCE_CATEGORIES]: Number of scientists in each category
```

### Magic

```
SPELLS = [
    # ... (See detailed list below)
]

ACTIVE_SPELLS = []: List of spells currently active on the province
ACTIVE_SPELL_TARGET[SPELL in ACTIVE_SPELLS]: Target of the active spell (province or kingdom)
ACTIVE_SPELL_REMAINING_DURATION[SPELL in ACTIVE_SPELLS]: Remaining duration of the active spell
```

### Thievery

```
THIEVERY_OPERATIONS = [
    # ... (See detailed list below)
]
```

### Dragons

```
DRAGON_TYPES = [
    EMERALD_DRAGON,
    RUBY_DRAGON,
    SAPPHIRE_DRAGON,
    TOPAZ_DRAGON
]

ACTIVE_DRAGON: Current active dragon type (or NONE)
```

### Rituals

```
RITUAL_TYPES = [
    BARRIER,
    EXPEDIENT,
    HASTE,
    HAVOC,
    ONSLAUGHT,
    STALWART
]

ACTIVE_RITUAL: Current active ritual type (or NONE)
RITUAL_STRENGTH: Current strength of the active ritual
```

### Stances

```
STANCE_TYPES = [
    NORMAL_STANCE,
    AGGRESSIVE_STANCE,
    PEACEFUL_STANCE
]

CURRENT_STANCE: Current stance of the province
```

### Additional Calculated Properties

```
BUILDING_EFFICIENCY
OFFENSIVE_MILITARY_EFFICIENCY (OME)
DEFENSIVE_MILITARY_EFFICIENCY (DME)
THIEVES_PER_ACRE (TPA)
WIZARDS_PER_ACRE (WPA)
DRAFT_RATE
TRAINING_TIME
ATTACK_TIME
INCOME_RATE
BIRTH_RATE
FOOD_CONSUMPTION_RATE
SCIENCE_PRODUCTION_RATE
SPELL_DURATION_MODIFIER
SPELL_SUCCESS_MODIFIER
```

## 2. Modifiers

Modifiers are properties that affect other province properties, usually expressed as percentages or flat bonuses/penalties.

```
# General Modifiers
BUILDING_EFFICIENCY_MODIFIER
INCOME_MODIFIER
POPULATION_GROWTH_MODIFIER
FOOD_PRODUCTION_MODIFIER
RUNE_PRODUCTION_MODIFIER
SCIENCE_PRODUCTION_MODIFIER
WAGE_MODIFIER
ATTACK_TIME_MODIFIER
TRAINING_TIME_MODIFIER
CONSTRUCTION_TIME_MODIFIER
EXPLORATION_COST_MODIFIER
BUILDING_COST_MODIFIER
DRAFT_COST_MODIFIER
TRAINING_COST_MODIFIER
SPECIALIST_CREDIT_MODIFIER
STEALTH_MODIFIER
BATTLE_RESOURCE_GAINS_MODIFIER
GOLD_GAINS_MODIFIER
MANA_RECOVERY_MODIFIER
# Military-related Modifiers
OME_MODIFIER
DME_MODIFIER
MILITARY_CASUALTY_MODIFIER
ENEMY_MILITARY_CASUALTY_MODIFIER
# Magic-related Modifiers
SPELL_DURATION_MODIFIER
SPELL_SUCCESS_MODIFIER
SPELL_DAMAGE_MODIFIER
SPELL_RESISTANCE_MODIFIER
INSTANT_SPELL_DAMAGE_TAKEN_MODIFIER
WPA_MODIFIER
# Thievery-related Modifiers
TPA_MODIFIER
THIEF_LOSS_MODIFIER
THIEVERY_DAMAGE_MODIFIER
THIEVERY_RESISTANCE_MODIFIER
SABOTAGE_DAMAGE_MODIFIER

# Modifiers from Specific Sources
RACE_MODIFIERS[MODIFIER in MODIFIERS]
PERSONALITY_MODIFIERS[MODIFIER in MODIFIERS]
SCIENCE_MODIFIERS[MODIFIER in MODIFIERS, SCIENCE_TYPE in SCIENCE_TYPES]
SPELL_MODIFIERS[MODIFIER in MODIFIERS, SPELL in SPELLS]
THIEVERY_MODIFIERS[MODIFIER in MODIFIERS, THIEVERY_OPERATION in THIEVERY_OPERATIONS]
DRAGON_MODIFIERS[MODIFIER in MODIFIERS, DRAGON_TYPE in DRAGON_TYPES]
RITUAL_MODIFIERS[MODIFIER in MODIFIERS, RITUAL_TYPE in RITUAL_TYPES]
STANCE_MODIFIERS[MODIFIER in MODIFIERS, STANCE_TYPE in STANCE_TYPES]
```

## 3. Races

```
RACES = [
    AVIAN,
    DARK_ELF,
    DWARF,
    ELF,
    FAERIE,
    HALFLING,
    HUMAN,
    ORC,
    DRYAD
]
```

Here's a table summarizing the effects of each race based on the provided reference:

| Race       | Modifiers                                                                                                                                                                                                |
| :--------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Avian      | `ATTACK_TIME_MODIFIER`: -25%, `POPULATION_GROWTH_MODIFIER`: +50%, `THIEVERY_RESISTANCE_MODIFIER` (to Ambush): +100% (Immune), `MILITARY_CASUALTY_MODIFIER`: +15%, Cannot use Stables or Horses, Soldier(3/0, 0.75nw), OS(10/0, 4.0nw), DS(0/10, 5.0nw), Elite(13/4, 900gc, 6.5nw) |
| Dark Elf   | `RUNE_COST_MODIFIER` (except for rituals): 0%, `WPA_MODIFIER`: +10%, Can use `SPECIALIST_CREDITS` to train Thieves, `EXPLORATION_COST_MODIFIER`: +25%, `WAGE_MODIFIER`: +25%                |
| Dwarf      | `BUILDING_EFFICIENCY_MODIFIER`: +30%, `CONSTRUCTION_TIME_MODIFIER`: -50%, `FOOD_CONSUMPTION_MODIFIER`: +100%, Cannot use Accelerated Construction, Elite(13/6, 1000gc, 7.5nw)                                                              |
| Elf        | `WPA_MODIFIER`: +25%, `MILITARY_CASUALTY_MODIFIER`: -30%, `MANA_RECOVERY_MODIFIER`: +1 per tick, `WAGE_MODIFIER`: +30%, No access to Dungeons or Prisoners, Elite(12/5, 900gc, 6.5nw)                                       |
| Faerie     | `SPELL_DURATION_MODIFIER`: +25% (self), +25% (offensive), `MANA_RECOVERY_MODIFIER`: +1 per tick during war, `POPULATION_MODIFIER`: -10%, Elite(4/12, 850gc, 6.75nw)                                                                   |
| Halfling   | `POPULATION_MODIFIER`: +10%, `TPA_MODIFIER`: +25%, `POPULATION_GROWTH_MODIFIER`: -25%, Elite(11/5, 850gc, 6.25nw)                                                                                                      |
| Human      | `SCIENCE_PRODUCTION_MODIFIER`: +35%, `INCOME_MODIFIER`: +15%, `WPA_MODIFIER`: -30%, Elite(12/5, 900gc, 6.5nw)                                                                                                       |
| Orc        | `BATTLE_RESOURCE_GAINS_MODIFIER`: +30%, `DRAFT_COST_MODIFIER`: -25%, Can use `SPECIALIST_CREDITS` to train Elites, `SCIENCE_PRODUCTION_MODIFIER`: -30%, `MILITARY_CASUALTY_MODIFIER`: +15%, Elite(15/3, 950gc, 6.75nw)                                   |
| Dryad      | `ATTACK_TIME_MODIFIER`: +20%, `ENEMY_MILITARY_CASUALTY_MODIFIER`: -20%, Elite(18/2, 1050gc, 7.5nw)                                                                                                 |

## 4. Personalities

```
PERSONALITIES = [
    ARTISAN,
    CLERIC,
    HERETIC,
    MYSTIC,
    ROGUE,
    SHEPHERD,
    TACTICIAN,
    WAR_HERO,
    WARRIOR
]
```

Here's a table summarizing the effects of each personality based on the provided reference:

| Personality | Modifiers                                                                                                                                                                                                                                                                                       |
| :---------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Artisan     | `BUILDING_CAPACITY_MODIFIER` (Homes, Stables, Dungeons): +50%, `BUILDING_PRODUCTION_MODIFIER` (Banks, Farms, Stables, Towers): +50%, `BUILDING_CREDITS_GAINS_MODIFIER`: +50%, `BUILDING_COST_MODIFIER`: -20% (when destroyed by Arson/Tornado/Raze), 100% success on Espionage Ops (double `STEALTH` cost), `SCIENCE_MODIFIERS[ARTISAN]`: +20%, Starts with +600 Soldiers, +600 `SPECIALIST_CREDITS` and +200 `BUILDING_CREDITS` |
| Cleric      | `MILITARY_UNIT_DEFENSE`[ELITES]: +1, `MILITARY_UNIT_DEFENSE`[DEFSPECS]: +1, `INSTANT_SPELL_DAMAGE_TAKEN_MODIFIER`: -25%, Access to `SPELLS`[Divine Shield, Revelation, Salvation], Starts with +800 Soldiers and +800 `SPECIALIST_CREDITS`    |
| Heretic     | `SPELL_DAMAGE_MODIFIER`: +20%, `SABOTAGE_DAMAGE_MODIFIER`: +20%, `WIZARD_PRODUCTION_MODIFIER`: +50%, `THIEF_LOSS_MODIFIER`: -50%, `SCIENCE_MODIFIERS[CHANNELING]`: +15%, `SCIENCE_MODIFIERS[CRIME]`: +10%, Access to `SPELLS`[Fool's Gold, Nightmares], Starts with +400 Wizards and +400 Thieves                                                                                    |
| Mystic      | `GUILD_EFFECT_MODIFIER`: +75%, `MANA_RECOVERY_MODIFIER`: +1% per tick, `SCIENCE_MODIFIERS[CHANNELING]`: +25%, Access to `SPELLS`[Chastity, Mage's Fury, Magic Ward, Meteor Showers, Mind Focus], Starts with +800 Wizards          |
| Rogue       | `THIEVES_DENS_EFFECT_MODIFIER`: +80%, `STEALTH_MODIFIER`: +1 per tick, Access to all Thievery Operations, `SCIENCE_MODIFIERS[CRIME]`: +25%, Starts with +800 thieves                                          |
| Shepherd    | `FOOD_PRODUCTION_MODIFIER`: +4 per acre (flat), `SCIENTIST_PRODUCTION_MODIFIER`: +30%, `EXPLORATION_COST_MODIFIER`: -30%, `CONSTRUCTION_TIME_MODIFIER`: -30%, `TRAINING_TIME_MODIFIER`: -30%, Immune to `SPELLS`[Droughts, Gluttony, Vermin], Access to `SPELL`[Reflect Magic], `SCIENCE_MODIFIERS[RESILIENCE]`: +25%, Starts with +800 Soldiers and +800 `SPECIALIST_CREDITS`                                                       |
| Tactician   | `ATTACK_TIME_MODIFIER`: -15%, `COMBAT_CREDITS_GAINS_MODIFIER`: +25%, `THIEF_LOSS_MODIFIER` (on Espionage Operations): -100%, Immune to Dragons, `SCIENCE_MODIFIERS[SIEGE]`: +25%, Starts with +800 soldiers and +800 `SPECIALIST_CREDITS`     |
| War Hero    | `ENEMY_MILITARY_CASUALTY_MODIFIER`: +30%, Converts Offensive Specialists to Elites on Successful Trad March and Conquest, Access to `SPELLS`[Animate Dead, Mist], `SCIENCE_MODIFIERS[HOUSING]`: +25%, Starts with +800 soldiers and +800 `SPECIALIST_CREDITS`                                                                                     |
| Warrior     | `OME_MODIFIER`: +15%, `MERCENARY_COST_MODIFIER`: -50%, `MILITARY_UNIT_OFFENSE`[MERCENARIES]: +2, `MILITARY_UNIT_OFFENSE`[PRISONERS]: +2, `SCIENCE_MODIFIERS[TACTICS]`: +25%, Starts with +800 soldiers and +800 `SPECIALIST_CREDITS`                                                                                                 |

## 5. Spells

Each spell is tagged as `SELF` (targeting own province) or `OFFENSIVE` (targeting another province).

```
SPELLS = [
    # Self Spells
    MINOR_PROTECTION, # SELF: `DME_MODIFIER`: + 5%
    GREATER_PROTECTION, # SELF: `DME_MODIFIER`: + 5%
    MAGIC_SHIELD, # SELF: `WPA_MODIFIER`: + 20% (defensive)
    MYSTIC_AURA, # SELF: Next incoming spell fails
    FERTILE_LANDS, # SELF: `FOOD_PRODUCTION_MODIFIER`: + 25%
    NATURES_BLESSING, # SELF: Immunity to `SPELLS`[Storms, Droughts], 33% chance to cure the Plague
    LOVE_AND_PEACE, # SELF: `POPULATION_GROWTH_MODIFIER`: +0.85%, `HORSE_PRODUCTION_RATE`: +40%
    DIVINE_SHIELD, # SELF: `INSTANT_SPELL_DAMAGE_TAKEN_MODIFIER`: -25%
    QUICK_FEET, # SELF: `ATTACK_TIME_MODIFIER`: - 10%
    BUILDERS_BOON, # SELF: `CONSTRUCTION_TIME_MODIFIER`: - 25%
    INSPIRE_ARMY, # SELF: `WAGE_MODIFIER`: -15%, `TRAINING_TIME_MODIFIER`: -20%
    HEROS_INSPIRATION, # SELF: `WAGE_MODIFIER`: -30%, `TRAINING_TIME_MODIFIER`: -30%
    SCIENTIFIC_INSIGHTS, # SELF: `SCIENCE_PRODUCTION_MODIFIER`: +10%
    ANONYMITY, # SELF: Hides province name in attacks, no honor gains, `BATTLE_RESOURCE_GAINS_MODIFIER`: -15%, ambush immunity
    ILLUMINATE_SHADOWS, # SELF: `THIEVERY_DAMAGE_MODIFIER`: -20% (incoming)
    SALVATION, # SELF: `MILITARY_CASUALTY_MODIFIER`: -15%
    WRATH, # SELF: `ENEMY_MILITARY_CASUALTY_MODIFIER`: +20%
    INVISIBILITY, # SELF: `TPA_MODIFIER`: +10%, `THIEF_LOSS_MODIFIER`: -20%
    CLEAR_SIGHT, # SELF: 25% chance to catch enemy thieves, `THIEVERY_RESISTANCE_MODIFIER`: +25%
    MAGES_FURY, # SELF: `WPA_MODIFIER`: +25% (offensive), -25% (defensive)
    WAR_SPOILS, # SELF: Take control of land gained on attacks
    MIND_FOCUS, # SELF: Increases `WIZARD_PRODUCTION_MODIFIER` by 25%
    FANATICISM, # SELF: `OME_MODIFIER`: +5%, `DME_MODIFIER`: -5%
    GUILE, # SELF: `SPELL_DAMAGE_MODIFIER`: +10%, `SABOTAGE_DAMAGE_MODIFIER`: +10%
    REVELATION, # SELF: Increases `SCIENTIST_PRODUCTION_MODIFIER` by +20%
    FOUNTAIN_OF_KNOWLEDGE, # SELF: `SCIENCE_PRODUCTION_MODIFIER`: +10%
    TREE_OF_GOLD, # SELF: Instantly generates 26.66% to 53.33% of daily income
    TOWN_WATCH, # SELF: Every 5 peasants defend with 1 strength, high casualties
    AGGRESSION, # SELF: Soldiers are +2/-2 strength
    MINERS_MYSTIQUE, # SELF: Peasants generate an extra 0.3 gold per tick
    GHOST_WORKERS, # SELF: `BUILDING_EFFICIENCY_MODIFIER`: + 25% (less jobs needed)
    ANIMATE_DEAD, # SELF: 50% of defensive military casualties return as soldiers in next defense
    MIST, # SELF: `BATTLE_RESOURCE_GAINS_MODIFIER`: -10% in battle
    REFLECT_MAGIC, # SELF: 20% chance to reflect offensive spells back at casters
    SHADOWLIGHT, # SELF: Reveals origin province of next thievery op, next sabotage fails automatically
    BLOODLUST, # SELF: `OME_MODIFIER`: +10%, `ENEMY_MILITARY_CASUALTY_MODIFIER`: +15%, own `MILITARY_CASUALTY_MODIFIER`: +15%
    PATRIOTISM, # SELF: Increases draft speed by +30%, decreases propaganda damage taken by -30%
    PARADISE, # SELF: Creates a few acres of land, consumes explore pool, not available during war or protection
    CAST_RITUAL, # SELF: Increases progress towards the chosen `RITUAL`

    # Offensive Spells
    STORMS, # OFFENSIVE: Kills 1.5% of target's `POPULATION`, cancels `SPELLS`[Droughts], increases `SPELLS`[Tornadoes] damage taken by +15%
    DROUGHTS, # OFFENSIVE: `FOOD_PRODUCTION_MODIFIER`: -25%, draft speed - 15%, kills some warhorses, cancels `SPELLS`[Storms], increases `THIEVERY_OPERATIONS`[Arson] damage taken by +15%
    MAGIC_WARD, # OFFENSIVE: (Unfriendly, Hostile, War) Increases target's rune costs by 100%
    GLUTTINY, # OFFENSIVE: `FOOD_CONSUMPTION_MODIFIER`: +25%
    VERMIN, # OFFENSIVE: Destroys on average 50% of target's `FOOD`
    GREED, # OFFENSIVE: `WAGE_MODIFIER`: +25%, `DRAFT_COST_MODIFIER`: +25%
    EXPOSE_THIEVES, # OFFENSIVE: (Unfriendly, Hostile, War) Reduces target's `STEALTH` by -5% each tick
    CHASTITY, # OFFENSIVE: `POPULATION_GROWTH_MODIFIER`: -50%
    SLOTH, # OFFENSIVE: (Unfriendly, Hostile, War) Reduces draft rate by -50%, `DRAFT_COST_MODIFIER`: +100%
    FIREBALL, # OFFENSIVE: (Unfriendly, Hostile, War) Kills ~6% of opponent's `PEASANTS`
    FOOLS_GOLD, # OFFENSIVE: (Unfriendly, Hostile, War) Destroys up to 25% of target's `GOLD`
    LIGHTNING_STRIKE, # OFFENSIVE: (Unfriendly, Hostile, War) Destroys up to 65% of target's `RUNES`
    EXPLOSIONS, # OFFENSIVE: When sending aid, 50% chance to reduce shipment to 55-80% if either sender or receiver is affected
    BLIZZARD, # OFFENSIVE: `BUILDING_EFFICIENCY_MODIFIER`: -10%
    PITFALLS, # OFFENSIVE: Increases opponent's defensive `MILITARY_CASUALTY_MODIFIER` by +15%
    NIGHTMARES, # OFFENSIVE: (Unfriendly, Hostile, War) ~1.5% of target's troops are put back in training over 8 ticks
    TORNADOES, # OFFENSIVE: (Unfriendly, Hostile, War) Destroys several of opponent's `BUILDINGS`
    MYSTIC_VORTEX, # OFFENSIVE: (Unfriendly, Hostile, War) 50% chance to remove each active spell on target
    AMNESIA, # OFFENSIVE: (War) Temporarily removes ~5% of target's allocated `SCIENCE_BOOKS`, returns over 48 ticks
    METEOR_SHOWERS, # OFFENSIVE: (Hostile, War) Kills peasants and troops every tick
    ABOLISH_RITUAL, # OFFENSIVE: (Unfriendly, Hostile, War) Reduces enemy `RITUAL` strength by -2%, limited to 10 casts per target
    LAND_LUST, # OFFENSIVE: (Unfriendly, Hostile, War) Captures 1-1.25% of opponent's acres
]
```

## 6. Thievery Operations

```
THIEVERY_OPERATIONS = [
    SPY_ON_THRONE,       # Reveals target's throne information
    SPY_ON_DEFENSE,      # Reveals target's defensive information
    SNATCH_NEWS,         # Copies latest news from target kingdom
    INFILTRATE,          # Attempts to steal gold, food, or runes
    SURVEY,              # Reveals target's land and building information
    SPY_ON_MILITARY,     # Reveals target's military information
    SPY_ON_SCIENCES,     # Reveals target's science information
    ROB_THE_GRANARIES,   # Steals `FOOD` from the target
    ROB_THE_VAULTS,      # Steals `GOLD` from the target
    ROB_THE_TOWERS,      # Steals `RUNES` from the target
    KIDNAPPING,          # Captures `PEASANTS` from the target
    STEAL_HORSES,        # Steals `HORSES` from the target
    PROPAGANDA,          # Reduces target's honor and loyalty
    SABOTAGE_WIZARDS,    # Kills a number of target's `WIZARDS`
    DESTABILIZE_GUILDS,  # Reduces target's `SPELL_DURATION_MODIFIER`
    ARSON,               # Destroys `BUILDINGS` on the target
    GREATER_ARSON,       # Destroys more `BUILDINGS` than Arson
    NIGHT_STRIKE,        # Kills a percentage of target's troops
    INCITE_RIOTS,        # Reduces target's `POPULATION_GROWTH_MODIFIER`
    BRIBE_THIEVES,       # Reduces target's `STEALTH`
    BRIBE_GENERALS,      # Increases target's `MILITARY_CASUALTY_MODIFIER`
    FREE_PRISONERS,      # Frees some of your captured `PRISONERS`
    ASSASSINATE_WIZARDS  # Kills a number of target's `WIZARDS`
]
```

## 7. Sciences

```
SCIENCE_CATEGORIES = [
    ECONOMY,
    MILITARY,
    ARCANE_ARTS
]

SCIENCE_TYPES[ECONOMY] = [
    ALCHEMY,       # Increases `INCOME_MODIFIER`
    TOOLS,         # Increases `BUILDING_EFFICIENCY_MODIFIER`
    HOUSING,       # Increases `POPULATION` capacity
    PRODUCTION,    # Increases `FOOD_PRODUCTION_MODIFIER`, `RUNE_PRODUCTION_MODIFIER`
    BOOKKEEPING,   # Decreases `WAGE_MODIFIER`
    ARTISAN        # Decreases `CONSTRUCTION_TIME_MODIFIER`, `CONSTRUCTION_COST_MODIFIER`, `RAZE_COST_MODIFIER`
]

SCIENCE_TYPES[MILITARY] = [
    STRATEGY,      # Increases `DME_MODIFIER`
    SIEGE,         # Increases `BATTLE_RESOURCE_GAINS_MODIFIER`
    TACTICS,       # Increases `OME_MODIFIER`
    VALOR,         # Decreases `MILITARY_UNIT_TRAINING_TIME`
    HEROISM,       # Decreases `DRAFT_COST_MODIFIER`, increases `DRAFT_RATE`
    RESILIENCE     # Decreases `MILITARY_CASUALTY_MODIFIER`
]

SCIENCE_TYPES[ARCANE_ARTS] = [
    CRIME,         # Increases `TPA_MODIFIER`, `STEALTH`
    CHANNELING,    # Increases `WPA_MODIFIER`
    SHIELDING,     # Decreases damage from enemy thievery (`THIEVERY_RESISTANCE_MODIFIER`) and instant magic (`SPELL_RESISTANCE_MODIFIER`)
    SORCERY,       # Increases instant `SPELL_DAMAGE_MODIFIER`
    CUNNING,       # Increases `THIEVERY_DAMAGE_MODIFIER`
    FINESSE,       # Decreases `THIEF_LOSS_MODIFIER`, `WIZARD_LOSS_MODIFIER` (on failed ops/spells)
]
```

## 8. Buildings

```
BUILDINGS = [
    HOMES,            # Increases: `POPULATION` (capacity), `POPULATION_GROWTH_MODIFIER`
    FARMS,            # Increases: `FOOD`, `FOOD_PRODUCTION_MODIFIER`
    MILLS,            # Increases: `BUILDING_EFFICIENCY_MODIFIER`, Decreases: `CONSTRUCTION_TIME_MODIFIER`, `EXPLORATION_COST_MODIFIER`
    BANKS,            # Increases: `GOLD`, `INCOME_MODIFIER`
    TRAINING_GROUNDS, # Increases: `OME_MODIFIER`, Decreases: `TRAINING_TIME_MODIFIER`
    ARMORIES,         # Decreases: `DRAFT_COST_MODIFIER`, `WAGE_MODIFIER`, `MILITARY_UNIT_COST`
    BARRACKS,         # Decreases: `ATTACK_TIME_MODIFIER`
    FORTS,            # Increases: `DME_MODIFIER`
    CASTLES,          # Decreases: Resource losses when attacked
    HOSPITALS,        # Decreases: `MILITARY_CASUALTY_MODIFIER`, Increases: `POPULATION_GROWTH_MODIFIER`
    GUILDS,           # Trains: `WIZARDS`, Increases: `SPELL_DURATION_MODIFIER`
    TOWERS,           # Increases: `RUNE_PRODUCTION_MODIFIER`
    THIEVES_DENS,     # Increases: `TPA_MODIFIER`, `STEALTH`, Decreases: `THIEF_LOSS_MODIFIER`
    WATCH_TOWERS,     # Increases: `THIEVERY_RESISTANCE_MODIFIER`, Decreases: `THIEVERY_DAMAGE_MODIFIER`
    UNIVERSITIES,     # Increases: `SCIENTIST_PRODUCTION_RATE`, `SCIENCE_PRODUCTION_MODIFIER`
    LIBRARIES,        # Increases: `SCIENCE_PRODUCTION_MODIFIER`
    STABLES,          # Produces: `HORSES`, Increases: `HORSE_PRODUCTION_RATE`
    DUNGEONS          # Houses: `PRISONERS`
]
```

## 9. Province interaction

### Attack Gains Formula:
**Gains** = Target Resource * Attack Type * `RPNW` * `RKNW` * `Multi-Attack Protection` * Race Modifier * Personality Modifier * Castles Protection * Relations Modifier * Stance Modifier * Siege Science * Emerald Dragon * Attack Time Adjustment Factor * `Ritual Bonus` * `Anonymity` * `Mist`
### Minimum Offense to Win
To guarantee a win, you must send Mod Offense that is at least 1 greater than the defender's Mod Defense.
On Conquest attacks, the minimum Offense to win is 51% of the opponents Defense.
### Attack Gains Modifiers

*   **Province Networth Factor (RPNW):**
    *   `rpnw` = Targets Networth / Self Networth
    *   `RPNW` = 0 if `rpnw` < 0.567 or `rpnw` > 1.6
    *   `RPNW` = 3 * `rpnw` - 1.7 if 0.567 < `rpnw` < 0.9
    *   `RPNW` = 1 if 0.9 < `rpnw` < 1.1
    *   `RPNW` = -2 * `rpnw` + 3.2 if 1.1 < `rpnw` < 1.6
*   **Kingdom Networth Factor (RKNW):**
    *   `rknw` = Target Kingdom Average Province Networth / Self Kingdom Average Province Networth
    *   `RKNW` = 0.8 if `rknw` < 0.5
    *   `RKNW` = `rknw` / 2 + 0.55 if 0.5 < `rknw` < 0.9
    *   `RKNW` = 1 if `rknw` > 0.9
*   **Multi-Attack Protection (MAP):** See [Multi-Attack Protection (MAP)]([LINK to MAP Formula Reference])
*   **Race Modifier:** See [Races] for specific racial bonuses/penalties.
*   **Personality Modifier:** See [Personalities] for specific personality bonuses/penalties.
*   **Castles Protection:** `CASTLES` reduce resource losses when attacked.
*   **Relations Modifier:** Based on relations (e.g., War, Hostile, etc.)
*   **Stance Modifier:** Based on current `STANCE`
*   **Siege Science:** `SCIENCE_POINTS[SIEGE]` increases battle gains.
*   **Emerald Dragon:** Active `EMERALD_DRAGON` modifies gains.
*   **Attack Time Adjustment Factor:**
    *   `-2` hours: `(-2 / base attack time) * 160%`
    *   `-1` hour: `(-1 / base attack time) * 150%`
    *   `+1` hour: `(1 / base attack time) * 80%`
    *   `+2` hours: `(2 / base attack time) * 70%`
    *   `+3` hours: `(3 / base attack time) * 60%`
    *   `+4` hours: `(4 / base attack time) * 50%`
*   **Ritual Bonus:** Active `RITUAL` modifies gains.
*   **Anonymity Spell:** Modifies gains according to the spell's effect.
*   **Mist Spell:** Modifies gains according to the spell's effect.

### Attack Types and Gains

*   **Traditional March:** Base gains are 12%, capped at 20% of your or your opponent's Acres (whichever is smaller).
*   **Ambush:** Returns 50% of the Acres lost in the attack. Unaffected by gains modifiers. Inflicts 15% increased Military Casualties.
*   **Plunder:** Base gains are 50% gold, 60% food, and 60% runes. Max gains are 1.75x base (67.5% gold, 81% runes/food).
*   **Learn:** Steals ~2% of target's allocated Science Books, plus ~20% of unallocated Books.
*   **Raze:** Destroys ~5% of*   **Raze:** Destroys ~5% of target's Land. In War, destroys ~30% Buildings.
*   **Conquest:** Base gains are 6.8% land for a full hit. Gains are decreased in proportion to the relative Offense sent vs. target's Defense. Available only within 5% Net Worth range outside Hostile/War, unless the attacker has access to Enhanced Conquest.
*   **Massacre:** Base gains are 9.5% peasants, 7.5% Thieves, and 5% Wizards. Efficiency increased roughly 3x during War.

### Attack Times

*   **Base Attack Time:** 14 hours (modified by the factors below)
*   **Formula:**
    ```
    Attack Time = Base Attack Time * Race Bonus * Personality Bonus * Barracks Bonus * Quick Feet * Attack Type * War * NW Mod * Ritual Bonus
    ```
*   **Modifiers:**
    *   **Race:** See [Races] for specific racial bonuses/penalties.
    *   **Personality:** See [Personalities] for specific personality bonuses/penalties.
    *   **Barracks:** `BUILDING_AMOUNT[BARRACKS]` reduces attack time.
    *   **Quick Feet Spell:** Reduces attack time by 10%.
    *   **Attack Type:**
        *   Intra-KD: 7 hours
        *   Different KD: 14 hours
    *   **War:** -15% attack time after 12 hours of war have passed.
    *   **NW Mod:** Based on the Net Worth difference between attacker and defender.
    *   **Ritual Bonus:** `RITUAL`(Haste)

### Minimum Defense

*   All land is protected by a minimum of 1 defense per Acre.
*   This does not apply to intra-kingdom attacks.

### Military Casualties

*   **Base Military Casualties:** 6.5%-8.5% on Offense, 5%-6.5% on Defense.
*   **Modifiers:**
    *   **Hospitals:** `BUILDING_AMOUNT[HOSPITALS]` reduces casualties.
    *   **Resilience Science:** `SCIENCE_POINTS[RESILIENCE]` reduces casualties.
    *   **Multi-Attack Protection:** Reduces casualties based on the number of attacks received.
    *   **Learn/Plunder:** -50% Defensive Military Casualties.
    *   **Orc Race:** +15% to all Military Casualties.
    *   **Stalwart Ritual:** -20% to all Military Casualties.
    *   **Onslaught Ritual:** +15% Offensive Military Casualties inflicted.
    *   **Pitfalls Spell:** +15% Defensive Military Casualties.
    *   **Bloodlust Spell:** +15% Offensive Military Casualties inflicted, +15% Self Military Casualties.
    *   **Salvation Spell:** -15% to all Military Casualties
    *   **Wrath Spell:** +20% Enemy Military Casualties.
    *   **Bribe Generals Operation:** 20% chance for +15% to all Military Casualties.
    *   **Emerald Dragon:** +20% to all Military Casualties.

## 10. Specific Modifiers from Races and Personalities

### Races

| Race       | Modifiers                                                                                                                                                                                                |
| :--------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Avian      | `ATTACK_TIME_MODIFIER`: -25%, `POPULATION_GROWTH_MODIFIER`: +50%, `THIEVERY_RESISTANCE_MODIFIER` (to Ambush): +100% (Immune), `MILITARY_CASUALTY_MODIFIER`: +15%, Cannot use Stables or Horses, Soldier(3/0, 0.75nw), OS(10/0, 4.0nw), DS(0/10, 5.0nw), Elite(13/4, 900gc, 6.5nw) |
| Dark Elf   | `RUNE_COST_MODIFIER` (except for rituals): 0%, `WPA_MODIFIER`: +10%, Can use `SPECIALIST_CREDITS` to train Thieves, `EXPLORATION_COST_MODIFIER`: +25%, `WAGE_MODIFIER`: +25%                |
| Dwarf      | `BUILDING_EFFICIENCY_MODIFIER`: +30%, `CONSTRUCTION_TIME_MODIFIER`: -50%, `FOOD_CONSUMPTION_MODIFIER`: +100%, Cannot use Accelerated Construction, Elite(13/6, 1000gc, 7.5nw)                                                              |
| Elf        | `WPA_MODIFIER`: +25%, `MILITARY_CASUALTY_MODIFIER`: -30%, `MANA_RECOVERY_MODIFIER`: +1 per tick, `WAGE_MODIFIER`: +30%, No access to Dungeons or Prisoners, Elite(12/5, 900gc, 6.5nw)                                       |
| Faerie     | `SPELL_DURATION_MODIFIER`: +25% (self), +25% (offensive), `MANA_RECOVERY_MODIFIER`: +1 per tick during war, `POPULATION_MODIFIER`: -10%, Elite(4/12, 850gc, 6.75nw)                                                                   |
| Halfling   | `POPULATION_MODIFIER`: +10%, `TPA_MODIFIER`: +25%, `POPULATION_GROWTH_MODIFIER`: -25%, Elite(11/5, 850gc, 6.25nw)                                                                                                      |
| Human      | `SCIENCE_PRODUCTION_MODIFIER`: +35%, `INCOME_MODIFIER`: +15%, `WPA_MODIFIER`: -30%, Elite(12/5, 900gc, 6.5nw)                                                                                                       |
| Orc        | `BATTLE_RESOURCE_GAINS_MODIFIER`: +30%, `DRAFT_COST_MODIFIER`: -25%, Can use `SPECIALIST_CREDITS` to train Elites, `SCIENCE_PRODUCTION_MODIFIER`: -30%, `MILITARY_CASUALTY_MODIFIER`: +15%, Elite(15/3, 950gc, 6.75nw)                                   |
| Dryad      | `ATTACK_TIME_MODIFIER`: +20%, `ENEMY_MILITARY_CASUALTY_MODIFIER`: -20%, Elite(18/2, 1050gc, 7.5nw)                                                                                                 |

### Personalities

| Personality | Modifiers                                                                                                                                                                                                                                                                                       |
| :---------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Artisan     | `BUILDING_CAPACITY_MODIFIER` (Homes, Stables, Dungeons): +50%, `BUILDING_PRODUCTION_MODIFIER` (Banks, Farms, Stables, Towers): +50%, `BUILDING_CREDITS_GAINS_MODIFIER`: +50%, `BUILDING_COST_MODIFIER`: -20% (when destroyed by Arson/Tornado/Raze), 100% success on Espionage Ops (double `STEALTH` cost), `SCIENCE_MODIFIERS[ARTISAN]`: +20%, Starts with +600 Soldiers, +600 `SPECIALIST_CREDITS` and +200 `BUILDING_CREDITS` |
| Cleric      | `MILITARY_UNIT_DEFENSE`[ELITES]: +1, `MILITARY_UNIT_DEFENSE`[DEFSPECS]: +1, `INSTANT_SPELL_DAMAGE_TAKEN_MODIFIER`: -25%, Access to `SPELLS`[Divine Shield, Revelation, Salvation], Starts with +800 Soldiers and +800 `SPECIALIST_CREDITS`    |
| Heretic     | `SPELL_DAMAGE_MODIFIER`: +20%, `SABOTAGE_DAMAGE_MODIFIER`: +20%, `WIZARD_PRODUCTION_MODIFIER`: +50%, `THIEF_LOSS_MODIFIER`: -50%, `SCIENCE_MODIFIERS[CHANNELING]`: +15%, `SCIENCE_MODIFIERS[CRIME]`: +10%, Access to `SPELLS`[Fool's Gold, Nightmares], Starts with +400 Wizards and +400 Thieves                                                                                    |
| Mystic      | `GUILD_EFFECT_MODIFIER`: +75%, `MANA_RECOVERY_MODIFIER`: +1% per tick, `SCIENCE_MODIFIERS[CHANNELING]`: +25%, Access to `SPELLS`[Chastity, Mage's Fury, Magic Ward, Meteor Showers, Mind Focus], Starts with +800 Wizards          |
| Rogue       | `THIEVES_DENS_EFFECT_MODIFIER`: +80%, `STEALTH_MODIFIER`: +1 per tick, Access to all Thievery Operations, `SCIENCE_MODIFIERS[CRIME]`: +25%, Starts with +800 thieves                                          |
| Shepherd    | `FOOD_PRODUCTION_MODIFIER`: +4 per acre (flat), `SCIENTIST_PRODUCTION_MODIFIER`: +30%, `EXPLORATION_COST_MODIFIER`: -30%, `CONSTRUCTION_TIME_MODIFIER`: -30%, `TRAINING_TIME_MODIFIER`: -30%, Immune to `SPELLS`[Droughts, Gluttony, Vermin], Access to `SPELL`[Reflect Magic], `SCIENCE_MODIFIERS[RESILIENCE]`: +25%, Starts with +800 Soldiers and +800 `SPECIALIST_CREDITS`                                                       |
| Tactician   | `ATTACK_TIME_MODIFIER`: -15%, `COMBAT_CREDITS_GAINS_MODIFIER`: +25%, `THIEF_LOSS_MODIFIER` (on Espionage Operations): -100%, Immune to Dragons, `SCIENCE_MODIFIERS[SIEGE]`: +25%, Starts with +800 soldiers and +800 `SPECIALIST_CREDITS`     |
| War Hero    | `ENEMY_MILITARY_CASUALTY_MODIFIER`: +30%, Converts Offensive Specialists to Elites on Successful Trad March and Conquest, Access to `SPELLS`[Animate Dead, Mist], `SCIENCE_MODIFIERS[HOUSING]`: +25%, Starts with +800 soldiers and +800 `SPECIALIST_CREDITS`                                                                                     |
| Warrior     | `OME_MODIFIER`: +15%, `MERCENARY_COST_MODIFIER`: -50%, `MILITARY_UNIT_OFFENSE`[MERCENARIES]: +2, `MILITARY_UNIT_OFFENSE`[PRISONERS]: +2, `SCIENCE_MODIFIERS[TACTICS]`: +25%, Starts with +800 soldiers and +800 `SPECIALIST_CREDITS`                                                                                                 |

## 11. Effects of Game Elements

### Buildings

| Building         | Effects                                                                                                       |
| :--------------- | :------------------------------------------------------------------------------------------------------------ |
| **HOMES**        | Increases: `POPULATION` (capacity), `POPULATION_GROWTH_MODIFIER`                                       |
| **FARMS**        | Increases: `FOOD`, `FOOD_PRODUCTION_MODIFIER`                                                                 |
| **MILLS**        | Increases: `BUILDING_EFFICIENCY_MODIFIER`, Decreases: `CONSTRUCTION_TIME_MODIFIER`, `EXPLORATION_COST_MODIFIER` |
| **BANKS**        | Increases: `GOLD`, `INCOME_MODIFIER`                                                                           |
| **TRAINING_GROUNDS** | Increases: `OME_MODIFIER`, Decreases: `TRAINING_TIME_MODIFIER`                                                |
| **ARMORIES**     | Decreases: `DRAFT_COST_MODIFIER`, `WAGE_MODIFIER`, `MILITARY_UNIT_COST`                                    |
| **BARRACKS**     | Decreases: `ATTACK_TIME_MODIFIER`                                                                              |
| **FORTS**        | Increases: `DME_MODIFIER`                                                                                      |
| **CASTLES**      | Decreases: Resource losses when attacked                                                                       |
| **HOSPITALS**    | Decreases: `MILITARY_CASUALTY_MODIFIER`, Increases: `POPULATION_GROWTH_MODIFIER`                               |
| **GUILDS**       | Trains: `WIZARDS`, Increases: `SPELL_DURATION_MODIFIER`                                                          |
| **TOWERS**       | Increases: `RUNE_PRODUCTION_MODIFIER`                                                                         |
| **THIEVES_DENS** | Increases: `TPA_MODIFIER`, `STEALTH`, Decreases: `THIEF_LOSS_MODIFIER`                                         |
| **WATCH_TOWERS** | Increases: `THIEVERY_RESISTANCE_MODIFIER`, Decreases: `THIEVERY_DAMAGE_MODIFIER`                                |
| **UNIVERSITIES** | Increases: `SCIENTIST_PRODUCTION_RATE`, `SCIENCE_PRODUCTION_MODIFIER`                                         |
| **LIBRARIES**    | Increases: `SCIENCE_PRODUCTION_MODIFIER`                                                                       |
| **STABLES**      | Produces: `HORSES`, Increases: `HORSE_PRODUCTION_RATE`                                                         |
| **DUNGEONS**      | Houses: `PRISONERS`                                                                                           |

### Sciences

| Science       | Category     | Effects                                                                             |
| :------------ | :----------- | :---------------------------------------------------------------------------------- |
| ALCHEMY       | ECONOMY      | Increases `INCOME_MODIFIER`                                                          |
| TOOLS         | ECONOMY      | Increases `BUILDING_EFFICIENCY_MODIFIER`                                             |
| HOUSING       | ECONOMY      | Increases `POPULATION` capacity                                                      |
| PRODUCTION    | ECONOMY      | Increases `FOOD_PRODUCTION_MODIFIER`, `RUNE_PRODUCTION_MODIFIER`                    |
| BOOKKEEPING   | ECONOMY      | Decreases `WAGE_MODIFIER`                                                             |
| ARTISAN       | ECONOMY      | Decreases `CONSTRUCTION_TIME_MODIFIER`, `CONSTRUCTION_COST_MODIFIER`, `RAZE_COST_MODIFIER` |
| STRATEGY      | MILITARY     | Increases `DME_MODIFIER`                                                             |
| SIEGE         | MILITARY     | Increases `BATTLE_RESOURCE_GAINS_MODIFIER`                                         |
| TACTICS       | MILITARY     | Increases `OME_MODIFIER`                                                             |
| VALOR         | MILITARY     | Decreases `MILITARY_UNIT_TRAINING_TIME`                                             |
| HEROISM       | MILITARY     | Decreases `DRAFT_COST_MODIFIER`, increases `DRAFT_RATE`                                |
| RESILIENCE     | MILITARY     | Decreases `MILITARY_CASUALTY_MODIFIER`                                                |
| CRIME         | ARCANE_ARTS  | Increases `TPA_MODIFIER`, `STEALTH`                                                   |
| CHANNELING    | ARCANE_ARTS  | Increases `WPA_MODIFIER`                                                             |
| SHIELDING     | ARCANE_ARTS  | Decreases damage from enemy thievery (`THIEVERY_RESISTANCE_MODIFIER`) and instant magic (`SPELL_RESISTANCE_MODIFIER`) |
| SORCERY       | ARCANE_ARTS  | Increases instant `SPELL_DAMAGE_MODIFIER`                                            |
| CUNNING       | ARCANE_ARTS  | Increases `THIEVERY_DAMAGE_MODIFIER`                                                 |
| FINESSE       | ARCANE_ARTS  | Decreases `THIEF_LOSS_MODIFIER`, `WIZARD_LOSS_MODIFIER` (on failed ops/spells)         |

### Spells

| Spell                    | Target    | Effects                                                                                                                              |
| :----------------------- | :-------- | :----------------------------------------------------------------------------------------------------------------------------------- |
| MINOR_PROTECTION         | SELF      | Increases `DME_MODIFIER` by 5%                                                                                                       |
| GREATER_PROTECTION       | SELF      | Increases `DME_MODIFIER` by 5%                                                                                                       |
| MAGIC_SHIELD             | SELF      | Increases defensive `WPA_MODIFIER` by 20%                                                                                            |
| MYSTIC_AURA              | SELF      | Next incoming spell fails                                                                                                             |
| FERTILE_LANDS            | SELF      | Increases `FOOD_PRODUCTION_MODIFIER` by 25%                                                                                          |
| NATURES_BLESSING         | SELF      | Immunity to `SPELLS`[Storms, Droughts], 33% chance to cure the Plague                                                                |
| LOVE_AND_PEACE           | SELF      | Increases `BIRTH_RATE` by 0.85% and `HORSE_PRODUCTION_RATE` by 40%                                                                    |
| DIVINE_SHIELD            | SELF      | Decreases `INSTANT_SPELL_DAMAGE_TAKEN_MODIFIER` by -25%                                                                                           |
| QUICK_FEET              | SELF      | Decreases `ATTACK_TIME_MODIFIER` by -10%                                                                                               |
| BUILDERS_BOON           | SELF      | Decreases `CONSTRUCTION_TIME_MODIFIER` by -25%                                                                                        |
| INSPIRE_ARMY             | SELF      | Decreases `WAGE_MODIFIER` by -15%, `TRAINING_TIME_MODIFIER` by -20%                                                                     |
| HEROS_INSPIRATION        | SELF      | Decreases `WAGE_MODIFIER` by -30% and `TRAINING_TIME_MODIFIER` by -30%                                                                     |
| SCIENTIFIC_INSIGHTS      | SELF      | Increases `SCIENCE_PRODUCTION_MODIFIER` by +10%                                                                                      |
| ANONYMITY                | SELF      | Hides province name in attacks, no honor gains, `GOLD_GAINS_MODIFIER` and `BATTLE_RESOURCE_GAINS_MODIFIER`: -15%, ambush immunity                                                    |
| ILLUMINATE_SHADOWS       | SELF      | Decreases incoming `THIEVERY_DAMAGE_MODIFIER` by -20%                                                                                    |
| SALVATION                | SELF      | Decreases `MILITARY_CASUALTY_MODIFIER` by -15%                                                                                       |
| WRATH                    | SELF      | Increases `ENEMY_MILITARY_CASUALTY_MODIFIER` by +20%                                                                                  |
| INVISIBILITY             | SELF      | Increases offensive `TPA_MODIFIER` by +10%, reduces `THIEF_LOSS_MODIFIER` by -20%                                                      |
| CLEAR_SIGHT              | SELF      | 25% chance to catch enemy thieves, `THIEVERY_RESISTANCE_MODIFIER`: +25%                                                                                                    |
| MAGES_FURY               | SELF      | Increases offensive `WPA_MODIFIER` by +25%, decreases defensive `WPA_MODIFIER` by -25%                                                     |
| WAR_SPOILS               | SELF      | Take control of land gained on attacks                                                                                                |
| MIND_FOCUS               | SELF      | Increases `WIZARD_PRODUCTION_MODIFIER` by +25%                                                                                       |
| FANATICISM               | SELF      | Increases `OME_MODIFIER` by +5%, decreases `DME_MODIFIER` by -5%                                                                       |
| GUILE                    | SELF      | Increases `SPELL_DAMAGE_MODIFIER` by +10%, `SABOTAGE_DAMAGE_MODIFIER` by +10%                                                                              |
| REVELATION               | SELF      | Increases `SCIENTIST_PRODUCTION_MODIFIER` by +20%                                                                                     |
| FOUNTAIN_OF_KNOWLEDGE    | SELF      | Increases `SCIENCE_PRODUCTION_MODIFIER` by +10%                                                                                      |
| TREE_OF_GOLD             | SELF      | Instantly generates 26.66% to 53.33% of daily income                                                                                   |
| TOWN_WATCH               | SELF      | Every 5 peasants defend with 1 strength, high casualties                                                                                |
| AGGRESSION               | SELF      | Soldiers are +2/-2 strength                                                                                                          |
| MINERS_MYSTIQUE          | SELF      | Peasants generate an extra 0.3 gold per tick                                                                                           |
| GHOST_WORKERS            | SELF      | Decreases jobs required for maximum `BUILDING_EFFICIENCY` by -25%                                                                      |
| ANIMATE_DEAD             | SELF      | 50% of defensive military casualties return as soldiers in next defense                                                                 |
| MIST                     | SELF      | `BATTLE_RESOURCE_GAINS_MODIFIER`: -10% in battle                                                                                                  |
| REFLECT_MAGIC            | SELF      | 20% chance to reflect offensive spells back at casters                                                                                |
| SHADOWLIGHT              | SELF      | Reveals origin province of next thievery op, next sabotage fails automatically                                                           |
| BLOODLUST                | SELF      | Increases `OME_MODIFIER` by +10%, `ENEMY_MILITARY_CASUALTY_MODIFIER` by +15%, own `MILITARY_CASUALTY_MODIFIER` by +15%                  |
| PATRIOTISM               | SELF      | Increases draft speed by +30%, decreases propaganda damage taken by -30%                                                                  |
| PARADISE                 | SELF      | Creates a few acres of land, consumes explore pool, not available during war or protection                                               |
| CAST_RITUAL              | SELF      | Increases progress towards the chosen `RITUAL`                                                                                     |
| STORMS                   | OFFENSIVE | Kills 1.5% of target's `POPULATION`, cancels `SPELLS`[Droughts], increases `SPELLS`[Tornadoes] damage taken by +15%                       |
| DROUGHTS                | OFFENSIVE | `FOOD_PRODUCTION_MODIFIER`: -25%, draft speed - 15%, kills some warhorses, cancels `SPELLS`[Storms], increases `THIEVERY_OPERATIONS`[Arson] damage taken by +15% |
| MAGIC_WARD               | OFFENSIVE | (Unfriendly, Hostile, War) Increases target's rune costs by 100%                                                                         |
| GLUTTINY                 | OFFENSIVE | `FOOD_CONSUMPTION_MODIFIER`: +25%                                                                                          |
| VERMIN                   | OFFENSIVE | Destroys on average 50% of target's `FOOD`                                                                                            |
| GREED                    | OFFENSIVE | `WAGE_MODIFIER`: +25%, `DRAFT_COST_MODIFIER`: +25%                                                                             |
| EXPOSE_THIEVES           | OFFENSIVE | (Unfriendly, Hostile, War) Reduces target's `STEALTH` by -5% each tick                                                                   |
| CHASTITY                 | OFFENSIVE | `POPULATION_GROWTH_MODIFIER`: -50%                                                                                          |
| SLOTH                    | OFFENSIVE | (Unfriendly, Hostile, War) Reduces draft rate by -50%, `DRAFT_COST_MODIFIER`: +100%                                           |
| FIREBALL                 | OFFENSIVE | (Unfriendly, Hostile, War) Kills ~6% of opponent's `PEASANTS`                                                                           |
| FOOLS_GOLD               | OFFENSIVE | (Unfriendly, Hostile, War) Destroys up to 25% of target's `GOLD`                                                                        |
| LIGHTNING_STRIKE         | OFFENSIVE | (Unfriendly, Hostile, War) Destroys up to 65% of target's `RUNES`                                                                        |
| EXPLOSIONS               | OFFENSIVE | When sending aid, 50% chance to reduce shipment to 55-80% if either sender or receiver is affected                                       |
| BLIZZARD                 | OFFENSIVE | `BUILDING_EFFICIENCY_MODIFIER`: -10%                                                                                     |
| PITFALLS                 | OFFENSIVE | Increases opponent's defensive `MILITARY_CASUALTY_MODIFIER` by +15%                                                                      |
| NIGHTMARES               | OFFENSIVE | (Unfriendly, Hostile, War) ~1.5% of target's troops are put back in training over 8 ticks                                               |
| TORNADOES                | OFFENSIVE | (Unfriendly, Hostile, War) Destroys several of opponent's `BUILDINGS`                                                                   |
| MYSTIC_VORTEX            | OFFENSIVE | (Unfriendly, Hostile, War) 50% chance to remove each active spell on target                                                              |
| AMNESIA                  | OFFENSIVE | (War) Temporarily removes ~5% of target's allocated `SCIENCE_BOOKS`, returns over 48 ticks                                               |
| METEOR_SHOWERS           | OFFENSIVE | (Hostile, War) Kills peasants and troops every tick                                                                                      |
| ABOLISH_RITUAL           | OFFENSIVE | (Unfriendly, Hostile, War) Reduces enemy `RITUAL` strength by -2%, limited to 10 casts per target                                         |
| LAND_LUST                | OFFENSIVE | (Unfriendly, Hostile, War) Captures 1-1.25% of opponent's acres                                                                        |

### Thievery Operations

| Operation          | Effects                                                                                                                      |
| :----------------- | :--------------------------------------------------------------------------------------------------------------------------- |
| SPY_ON_THRONE      | Reveals target's throne information                                                                                           |
| SPY_ON_DEFENSE     | Reveals target's defensive information                                                                                        |
| SNATCH_NEWS        | Copies latest news from target kingdom                                                                                       |
| INFILTRATE         | Attempts to steal gold, food, or runes                                                                                       |
| SURVEY             | Reveals target's land and building information                                                                                |
| SPY_ON_MILITARY    | Reveals target's military information                                                                                         |
| SPY_ON_SCIENCES    | Reveals target's science information                                                                                          |
| ROB_THE_GRANARIES  | Steals `FOOD` from the target                                                                                                  |
| ROB_THE_VAULTS     | Steals `GOLD` from the target                                                                                                   |
| ROB_THE_TOWERS     | Steals `RUNES` from the target                                                                                                   |
| KIDNAPPING         | Captures `PEASANTS` from the target                                                                                             |
| STEAL_HORSES       | Steals `HORSES` from the target                                                                                                 |
| PROPAGANDA         | Reduces target's honor and loyalty                                                                                           |
| SABOTAGE_WIZARDS   | Kills a number of target's `WIZARDS`                                                                                            |
| DESTABILIZE_GUILDS | Reduces target's `SPELL_DURATION_MODIFIER`                                                                                    |
| ARSON              | Destroys `BUILDINGS` on the target                                                                                             |
| GREATER_ARSON      | Destroys more `BUILDINGS` than Arson                                                                                            |
| NIGHT_STRIKE       | Kills a percentage of target's troops                                                                                         |
| INCITE_RIOTS       | Reduces target's `POPULATION_GROWTH_MODIFIER`                                                                                |
| BRIBE_THIEVES      | Reduces target's `STEALTH`                                                                                                     |
| BRIBE_GENERALS     | Increases target's `MILITARY_CASUALTY_MODIFIER`                                                                               |
| FREE_PRISONERS     | Frees some of your captured `PRISONERS`                                                                                         |
| ASSASSINATE_WIZARDS | Kills a number of target's `WIZARDS`                                                                                            |

### Rituals

| Ritual     | Effects                                                                                                       |
| :--------- | :------------------------------------------------------------------------------------------------------------ |
| BARRIER    | `SPELL_RESISTANCE_MODIFIER` to Instant Magic : -20%,  `THIEVERY_RESISTANCE_MODIFIER`: -20% ,  -10% Battle (Resource) Losses                       |
| EXPEDIENT  | `BUILDING_EFFICIENCY_MODIFIER`: +10%, `WAGE_MODIFIER`: -20%, `CONSTRUCTION_COST_MODIFIER`: -20%                                           |
| HASTE      | `ATTACK_TIME_MODIFIER`: -10%, `TRAINING_TIME_MODIFIER`: -25%, `CONSTRUCTION_TIME_MODIFIER`: -25%                                                    |
| HAVOC      | `WPA_MODIFIER`: +20% (offensive) , `TPA_MODIFIER`: +20% (offensive), `SPELL_DAMAGE_MODIFIER`: +10%, `SABOTAGE_DAMAGE_MODIFIER`: +10%                                  |
| ONSLAUGHT  | `OME_MODIFIER`: +10%, `ENEMY_MILITARY_CASUALTY_MODIFIER`: +15%                                  |
| STALWART   | `DME_MODIFIER`: +5%, `MILITARY_CASUALTY_MODIFIER`: -20%                                                    |

### Stances

| Stance      | Effects                                                                          |
| :---------- | :------------------------------------------------------------------------------- |
| NORMAL      | No Effects                                                                       |
| AGGRESSIVE  | `BATTLE_RESOURCE_GAINS_MODIFIER`: +10%,  `SPECIALIST_CREDITS` gain in combat : +30%, `MILITARY_CASUALTY_MODIFIER`: +15% (self, offensive), `SPELL_DAMAGE_MODIFIER`: +10% (instant, offensive), `SABOTAGE_DAMAGE_MODIFIER`: +10% (offensive), `WAGE_MODIFIER`: +20% |
| PEACEFUL    | `INCOME_MODIFIER`: +15%, `FOOD_PRODUCTION_MODIFIER`: +20%, `RUNE_PRODUCTION_MODIFIER`: +20%, `POPULATION_GROWTH_MODIFIER`: +20%, `OME_MODIFIER`: -15%,  `SPELL_DAMAGE_MODIFIER`: -20% (outgoing, instant), `SABOTAGE_DAMAGE_MODIFIER`: -20% (outgoing) |

This now includes all the specific effects of each game element on the relevant modifiers, resources, and properties. This should be much more helpful for modeling the game accurately. Let me know if you need any further refinements or have more questions.
