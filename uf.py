import requests
from trafilatura import extract
import re

def download_and_extract(url):
    """Downloads a URL and extracts the main content using Trafilatura."""
    try:
        response = requests.get(url)
        response.raise_for_status()  # Raise an exception for bad status codes
        content = extract(response.content, favor_recall=True)
        return content if content else ""
    except requests.exceptions.RequestException as e:
        print(f"Error downloading {url}: {e}")
        return ""

def main():
    """Downloads web pages, extracts content, and formats the prompt."""

    prompt_template = """
I play the game Utopia, in a way it's a spreadsheet game. You create a province, and there's variables that change. The documentation is a bit spread out, but i'll point to it.

[ref **races**] has races
[ref **personalities**] has personalities

Each province has both a race and personality, which modify certain base values

A province's population is filled with units see [ref **units**]. Most unit take population space, but some do not (horses, prisoners, mercaneries do not, horses need sables for space, prisoners dungeons, and nercs need to be bought (this mechanic is irrelevant for now))

A province also has buildings, some hav a capacity to hold resources, other just give an effect, others do both. See [ref **growth**]

A province also has resources (gold, runes, food) and in a certain way peasants and scientists / science points

Regarding science a province can allocate sciencepoints to science catagories [ref **science**]
Each science catagory again modifies a province property

Then there are spells a privince can cast (which depends on race/personality) that gurther effect modifiers:
[ref **magic**]
Spells can be cast on one self, and on another provinces, each modifies values temprarily for the spell duration

Thievery is like spells but can only be done against another province [ref **thievery**]

Some province properties like offense / defense / protection are relataed to attacking / defending, a way for provoinces to interact [ref **attacking**]

A final set of modifiers of properties result from dragons [ref **dragons**] rituals [ref **ritual**] and stances [ref **stances**]

=== References ===:
"""

    urls = {
        "races": "http://wiki.utopia-game.com/index.php?title=Race",
        "personalities": "http://wiki.utopia-game.com/index.php?title=Personality",
        "units": "http://wiki.utopia-game.com/index.php?title=Units",
        "growth": "http://wiki.utopia-game.com/index.php?title=Growth",
        "science": "http://wiki.utopia-game.com/index.php?title=Science_Formulas",
        "magic": "http://wiki.utopia-game.com/index.php?title=Magic_Formulas",
        "thievery": "http://wiki.utopia-game.com/index.php?title=Thievery_Formulas",
        "attacking": "http://wiki.utopia-game.com/index.php?title=Attacking_%26_Defending",
        "dragons": "http://wiki.utopia-game.com/index.php?title=Dragons",
        "ritual": "http://wiki.utopia-game.com/index.php?title=Ritual",
        "stances": "http://wiki.utopia-game.com/index.php?title=Stances"
    }

    references_content = ""
    for tag, url in urls.items():
        content = download_and_extract(url)
        references_content += f"<{tag}>\n{content}\n</{tag}>\n\n"
        

    final_prompt = prompt_template + "\n" + references_content

    with open("prompt.txt", "w", encoding="utf-8", errors="replace") as f:
        f.write(final_prompt)

    print("Prompt written to prompt.txt")

if __name__ == "__main__":
    main()