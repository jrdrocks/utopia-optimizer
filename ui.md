# UI Documentation

## Main Interface

The UI is organized into tabs within the Optimizer component. Each tab contains specific input fields and controls.

### Tab Structure

1. **Province Details** - Basic province information and optimization targets
2. **Building Setup** - Building configuration and constraints  
3. **Science Setup** - Science book allocations
4. **Other Targets** - Non-essential targets and multipliers
5. **Overrides** - Technical configuration overrides

## Inputs by Section

### Province Details Tab

- **Race**: Multi-select chip buttons for available races  
  JSON Path: `form.RACE_selected` (array of selected race keys)
  
- **Personality**: Multi-select chip buttons for personalities  
  JSON Path: `form.PERS_selected` (array of selected personality keys)
  
- **Acres**: Numeric input for province size  
  JSON Path: `ACRES`
  
- **Honor**: Dropdown select for honor level  
  JSON Path: `HONOR`
  
- **Science Days**: Numeric input for science training days  
  JSON Path: `SCIENCE_DAYS`
  
- **Total Science**: Numeric input for total science points  
  JSON Path: `SCIENCE`
  
- **Excluded Self Spells**: Multi-select chip buttons for excluded spells  
  JSON Path: `SELF_SPELLS_EXCLUDE` (array of excluded spell keys)
  
- **Daily Income**: Numeric input for net income  
  JSON Path: `INCOME`
  
- **Runes Produced**: Numeric input for rune production  
  JSON Path: `RUNES`
  
- **WPA**: Numeric input for Wizards Per Acre  
  JSON Path: `MOD_WPA`
  
- **TPA**: Numeric input for Thieves Per Acre  
  JSON Path: `MOD_TPA`
  
- **Optimization Target**: Chip buttons for optimization strategy (DPA, DPNW, OPA, OPNW, TURTLE_DEF)  
  JSON Path: `OPT_FOR_NW`, `OPT_FOR_TURTLE_DEF`
  
- **DPA**: Numeric input for minimum Defense Per Acre  
  JSON Path: `DPA`
  
- **OPA**: Numeric input for minimum Offense Per Acre  
  JSON Path: `OPA`
  
- **OPA:DPA Ratio**: Numeric input for offense to defense ratio  
  JSON Path: `OPA_DPA_RATIO`

### Building Setup Tab

- **Building Targets Table** (for each building type):
  - **Min**: Minimum percentage (0-50)  
    JSON Path: `BUILDING_TARGETS.{building_name}.min`
  
  - **Max**: Maximum percentage (0-50)  
    JSON Path: `BUILDING_TARGETS.{building_name}.max`
  
  - **Exact**: Specific percentage (0-50)  
    JSON Path: `BUILDING_TARGETS.{building_name}.eq`
  
  - **As Effective**: Checkbox to treat as effective percentage  
    JSON Path: `BUILDING_TARGETS.{building_name}.as_effective`

### Science Setup Tab  

- **Science Books Table** (for each science category):
  - **Minimum Allocation**: Numeric input for minimum books  
    JSON Path: `SCIENCE_BASE.{science_category}`

### Other Targets Tab

- **Multipliers Table** (for various non-essential targets):
  - **Mage Damage Multiplier**: Numeric input for mage damage multiplier  
    JSON Path: `TARGETS.MAGE_DAMAGE_MULTIPLIER`
  
  - **Thief Damage Multiplier**: Numeric input for thief damage multiplier  
    JSON Path: `TARGETS.THIEF_DAMAGE_MULTIPLIER`
  
  - **Combat Gains Multiplier**: Numeric input for combat gains multiplier  
    JSON Path: `TARGETS.COMBAT_GAINS_MULTIPLIER`
  
  - **Attack Time Multiplier**: Numeric input for attack time multiplier  
    JSON Path: `TARGETS.ATTACK_TIME_MULTIPLIER`
  
  - **Losses Multiplier**: Numeric input for losses multiplier  
    JSON Path: `TARGETS.LOSSES_MULTIPLIER`
  
  - **Land Loss Multiplier**: Numeric input for land loss multiplier  
    JSON Path: `TARGETS.LANDLOSS_MULTIPLIER`
  
  - **Thief OP Damage Receive Multiplier**: Numeric input for thief OP damage receive multiplier  
    JSON Path: `TARGETS.THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER`
  
  - **Thief Losses Multiplier**: Numeric input for thief losses multiplier  
    JSON Path: `TARGETS.THIEF_LOSSES_MULTIPLIER`
  
  - **Max Prisoners**: Numeric input for maximum prisoners  
    JSON Path: `TARGETS.MAX_PRISONERS`
  
  - **Max Elite OSpec Ratio**: Numeric input for maximum elite offensive specialist ratio  
    JSON Path: `TARGETS.MAX_ELITE_OSPEC_RATIO`
  
  - **Max Elite DSpec Ratio**: Numeric input for maximum elite defensive specialist ratio  
    JSON Path: `TARGETS.MAX_ELITE_DSPEC_RATIO`
  
  - Values above 1 are minimums, below 1 are maximums

### Overrides Tab

- **Technical Configuration**:
  - JSON view of current configuration
  - Code editor for manual overrides

## Submit Button

- **Run Calculations**: Button to submit the form and run optimizations
