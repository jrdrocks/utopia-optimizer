# API Documentation

## Endpoint: `/optimize`

### Method: POST

### Description:
This endpoint is used to optimize the server based on the provided form data and optimization target.

### Request Payload

The request payload should be in JSON format and include the following fields:

#### Form
- **RACE_selected**: An array of strings representing the selected races.
  - Example: `["DARKELF", "ELF"]`
- **PERS_selected**: An array of strings representing the selected personalities.
  - Example: `["ARTISAN", "CLERIC"]`

#### OptimizationTarget
- **RACE**: A string representing the selected race.
  - Example: `"DARKELF"`
- **PERS**: A string representing the selected personality.
  - Example: `"ARTISAN"`
- **HONOR**: A string representing the selected honor.
  - Example: `"KNIGHT"`
- **ACRES**: A number representing the acres.
  - Example: `1000`
- **SCIENCE**: (Optional) A number representing the science.
  - Example: `500`
- **SCIENCE_DAYS**: (Optional) A number representing the science days.
  - Example: `30`
- **SCIENCE_STATE**: (Optional) An object representing the science state.
  - Example:
    ```json
    {
      "scientists": [
        {"books": 0, "cat": "ECONOMY"},
        {"books": 0, "cat": "WAR"},
        {"books": 0, "cat": "ARCANE"}
      ],
      "training_done": 0
    }
    ```
- **SELF_SPELLS_EXCLUDE**: An array of strings representing the spells to exclude.
  - Example: `["CLEAR_SIGHT", "TOWN_WATCH"]`
- **INCOME**: A number representing the income.
  - Example: `10000`
- **RUNES**: A number representing the runes.
  - Example: `500`
- **MOD_WPA**: A number representing the modifier for WPA (Wizard Power Attack).
  - Example: `1.15`
- **MOD_TPA**: A number representing the modifier for TPA (Thief Power Attack).
  - Example: `1.25`
- **DPA**: (Optional) A number representing the DPA (Defensive Power Attack).
  - Example: `1.05`
- **OPA**: (Optional) A number representing the OPA (Offensive Power Attack).
  - Example: `1.1`
- **OPA_DPA_RATIO**: (Optional) A number representing the ratio of OPA to DPA.
  - Example: `1.05`
- **OPT_FOR_NW**: A Boolean indicating whether to optimize for NW (Net Worth).
  - Example: `true`
- **OPT_FOR_TURTLE_DEF**: A Boolean indicating whether to optimize for turtle defense.
  - Example: `false`
- **OPT_RAW_TPA_AND_WPA**: A Boolean indicating whether to optimize for raw TPA and WPA.
  - Example: `false`
- **BUILDING_TARGETS**: An object representing the building targets.
  - Example:
    ```json
    {
      "HOMES": {"min": 10, "max": 15, "as_effective": true},
      "GUILDS": {"min": 15, "as_effective": false},
      "TRAINING_GROUNDS": {"min": 0, "max": 15, "as_effective": true},
      "LIBRARIES": {"min": 0, "max": 15, "as_effective": true}
    }
    ```
- **TARGETS**: An object representing the extra targets.
  - Example:
    ```json
    {
      "MAGE_DAMAGE_MULTIPLIER": 1.1,
      "THIEF_DAMAGE_MULTIPLIER": 1.1,
      "COMBAT_GAINS_MULTIPLIER": 1.25,
      "ATTACK_TIME_MULTIPLIER": 0.85,
      "LOSSES_MULTIPLIER": 1.15,
      "LANDLOSS_MULTIPLIER": 1.15,
      "THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER": 1.1,
      "THIEF_LOSSES_MULTIPLIER": 0.8,
      "MAX_PRISONERS": 1000,
      "MAX_ELITE_OSPEC_RATIO": 0.5,
      "MAX_ELITE_DSPEC_RATIO": 0.5
    }
    ```
- **SCIENCE_BASE**: An object representing the base science values.
  - Example:
    ```json
    {
      "ALCHEMY": 60,
      "TOOLS": 80,
      "HOUSING": 100,
      "PRODUCTION": 120,
      "BOOKKEEPING": 1440,
      "ARTISAN": 5280,
      "STRATEGY": 12480,
      "SIEGE": 2,
      "TACTICS": 0.0724,
      "VALOR": 0.0524,
      "RESILIENCE": 0.0262,
      "HEROISM": 0.2172,
      "CRIME": 0.068,
      "CHANNELING": 0.0478,
      "SHIELDING": 0.0367,
      "CUNNING": 0.0367,
      "SORCERY": 0.0582,
      "FINESSE": 0.0367 * 4 / 3
    }
    ```

### Example Request
```json
{
  "form": {
    "RACE_selected": ["DARKELF", "ELF"],
    "PERS_selected": ["ARTISAN", "CLERIC"]
  },
  "RACE": "DARKELF",
  "PERS": "ARTISAN",
  "HONOR": "KNIGHT",
  "ACRES": 1000,
  "SCIENCE": 500,
  "SCIENCE_DAYS": 30,
  "SCIENCE_STATE": {
    "scientists": [
      {"books": 0, "cat": "ECONOMY"},
      {"books": 0, "cat": "WAR"},
      {"books": 0, "cat": "ARCANE"}
    ],
    "training_done": 0
  },
  "SELF_SPELLS_EXCLUDE": ["CLEAR_SIGHT", "TOWN_WATCH"],
  "INCOME": 10000,
  "RUNES": 500,
  "MOD_WPA": 1.15,
  "MOD_TPA": 1.25,
  "DPA": 1.05,
  "OPA": 1.1,
  "OPA_DPA_RATIO": 1.05,
  "OPT_FOR_NW": true,
  "OPT_FOR_TURTLE_DEF": false,
  "OPT_RAW_TPA_AND_WPA": false,
  "BUILDING_TARGETS": {
    "HOMES": {"base": 4, "be": true},
    "FARMS": {"flat_base": 60, "be": true, "flat_mul": "d -> d.BUILD_PROD_MULTIPLIER"}
  },
  "TARGETS": {
    "MAGE_DAMAGE_MULTIPLIER": 1.1,
    "THIEF_DAMAGE_MULTIPLIER": 1.1,
    "COMBAT_GAINS_MULTIPLIER": 1.25,
    "ATTACK_TIME_MULTIPLIER": 0.85,
    "LOSSES_MULTIPLIER": 1.15,
    "LANDLOSS_MULTIPLIER": 1.15,
    "THIEF_OP_DAMAGE_RECEIVE_MULTIPLIER": 1.1,
    "THIEF_LOSSES_MULTIPLIER": 0.8,
    "MAX_PRISONERS": 1000,
    "MAX_ELITE_OSPEC_RATIO": 0.5,
    "MAX_ELITE_DSPEC_RATIO": 0.5
  },
  "SCIENCE_BASE": {
    "ALCHEMY": 60,
    "TOOLS": 80,
    "HOUSING": 100,
    "PRODUCTION": 120,
    "BOOKKEEPING": 1440,
    "ARTISAN": 5280,
    "STRATEGY": 12480,
    "SIEGE": 2,
    "TACTICS": 0.0724,
    "VALOR": 0.0524,
    "RESILIENCE": 0.0262,
    "HEROISM": 0.2172,
    "CRIME": 0.068,
    "CHANNELING": 0.0478,
    "SHIELDING": 0.0367,
    "CUNNING": 0.0367,
    "SORCERY": 0.0582,
    "FINESSE": 0.0367 * 4 / 3
  }
}
```

### Response
The response will be a JSON array containing the optimization results for each combination of selected races and personalities.

#### Example Response
```json
[
  ["DARKELF", "ARTISAN", {
    "frames": [1, 2, 3],
    "values": [100, 200, 300],
    "definitions": ["DEF1", "DEF2", "DEF3"]
  }],
  ["DARKELF", "CLERIC", "infeasible"],
  ["ELF", "ARTISAN", {
    "frames": [1, 2, 3],
    "values": [150, 250, 350],
    "definitions": ["DEF1", "DEF2", "DEF3"]
  }],
  ["ELF", "CLERIC", "infeasible"]
]
