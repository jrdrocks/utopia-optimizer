Setup
=====

- Have a working install of https://julialang.org/ (developed using julia 1.9, whether newer version works, no idea)
- Run `julia --project=. julia/single-build.jl`

For more details how to get a working environment from `Project.toml` see https://pkgdocs.julialang.org/v1.2/environments/

If all fails (which is likely, julia projects are a bit of a mystery to me, somehow things seem tied to the julia version)
look at `Dockerfile` which uses *jlpkg* to install the dependencies and precompiles them.


Knobs
=====
*Code is a bit of a mess, usually `julia/single-build.jl` and `julia/full-matrix-custom.jl`  will work*

In `julia/src/data-structs.jl` most data structures are defined, take note of `OptimizationTarget` which holds all variables optimized for. Settings for the current age are defined in `julia/src/game-definitions.jl` and finally `julia/src/utopia.jl` is the optimizer with all formulas used in the game (or as close as possible)

**`julia/web-server.jl` is a backend, using the (svelte based) ui in `ui` to create a usable frontend. Currently hosted at [https://uo.sfury.com/](https://uo.sfury.com/)**



Gotcha
======
**Most self spells are taken into account when available, unless set to ignore**
- DIVINE_SHIELD (and magic damage reduction in general) is not used