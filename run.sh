#!/bin/bash

/usr/local/bin/julia /utopia-optimizer/julia/web-server.jl &

node /utopia-optimizer/ui/build/index.js &

wait -n

# Exit with status of process that exited first
exit $?
