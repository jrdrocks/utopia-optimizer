FROM node:22.9-bookworm-slim
RUN apt-get update && apt-get install coinor-cbc coinor-libcbc-dev wget curl git  p7zip-full -y

RUN if [ `uname -m` = aarch64 ] ; then wget "https://julialang-s3.julialang.org/bin/linux/`uname -m`/1.11/julia-1.11.2-linux-`uname -m`.tar.gz" ; else wget "https://julialang-s3.julialang.org/bin/linux/x64/1.11/julia-1.11.2-linux-x86_64.tar.gz" ; fi

RUN tar -xzvf julia* && ln -s /julia-1.11.2/bin/julia /usr/local/bin/julia

RUN julia -e "import Pkg; Pkg.add(\"jlpkg\"); import jlpkg; jlpkg.install()"

RUN ~/.julia/bin/jlpkg --update add Alpine AmplNLWriter Cbc DataFrames DataStructures Genie Ipopt JSON3@v1.13.2 JuMP Juniper NLopt NamedArrays Setfield Parameters PrettyTables StructTypes@v1.10.0  EnumX MLStyle

RUN julia -e "using Pkg; pkg\"build JuMP\""

RUN mkdir /utopia-optimizer
COPY ./ /utopia-optimizer
COPY ./ui/docker.env /utopia-optimizer/ui/.env

# vite is FANTASTIC, and won't build on arm64
# RUN npm run build
# extract the pre-build from build.7z
WORKDIR /utopia-optimizer
RUN 7z x build.7z

WORKDIR /utopia-optimizer/ui

RUN npm instal 

WORKDIR /

COPY run.sh run.sh

RUN chmod 777 run.sh

CMD ./run.sh