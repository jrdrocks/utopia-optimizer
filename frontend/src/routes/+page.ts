import type { PageLoad } from './$types';

export const load = (({ data }) => {
	return {
		sections: [
			{
				id: 'main'
			},
			{
				id: 'buildings'
			}
		]
	};
}) satisfies PageLoad;