import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';
import { getOptimizations } from '$lib/requests';

export const load: PageLoad = async () => {
    let optimizations = await getOptimizations();
	return {
        optimizations
    };

	error(404, 'Not found');
};