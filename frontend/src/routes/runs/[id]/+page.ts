import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';
import { loadOptimization } from '$lib/requests';

export const prerender = false;
export const ssr = false;

export const load: PageLoad = async ({ params }) => {
    let data = await loadOptimization(params.id);
	return {
        data
    };

	error(404, 'Not found');
};