import { JULIA_SERVER } from '$env/static/private';
import { json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import { produce } from 'sveltekit-sse';

import { createEventSource } from 'eventsource-client'

export const POST: RequestHandler = async ({ request, url }) => {
    let params = url.searchParams;
    console.log(params);
    // If ID is present, handle SSE connection
    if (params.has('id')) {
        const eventUrl = `${JULIA_SERVER}/optimize-progress/${params.get('id')}`;
        const evtSource = createEventSource(eventUrl);

        return produce(async function start({ emit , lock}) {

            for await (const { data } of evtSource) {
                
                console.log('Data: %s', data)
                emit("message", data);

                try {
                    let d = JSON.parse(data);
                    if (d['error']) { 
                        break
                    } else if (d['done']) {
                        break
                    } else if (!d['progress']) {
                        break
                    }
                    
                } catch (e) {
                    console.log(e);
                }
            }

            lock.set(false)

            

            if (evtSource) {
                evtSource.close();
            }
        });
    }

    // Otherwise, start new optimization
    const data = await request.json();
    const postUrl = `${JULIA_SERVER}/optimize-progress`;

    const response = await fetch(postUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });

    if (!response.ok) {
        return json({ error: 'Optimization failed' }, { status: 500 });
    }

    const result = await response.json();
    console.log(result);


    return Response.json(result); // Return result
};
