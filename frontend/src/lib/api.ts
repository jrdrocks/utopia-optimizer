import type { Readable } from 'svelte/store';
import type { JsonRequest } from './requests';
import { source } from 'sveltekit-sse'

export async function runOptimization(request: JsonRequest): Promise<Readable<string>> {
    const response = await fetch('/api/optimize-progress', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(request)
    });

    if (!response.ok) {
        throw new Error('Failed to start optimization');
    }

    let result = await response.json();
    let id = result['id'];



    return source(`/api/optimize-progress?id=${id}`, {
        error: (err) => {
            console.error("SHITTY");
        },
    }).select('message');
}
