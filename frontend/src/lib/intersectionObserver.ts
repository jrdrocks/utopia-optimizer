import { writable } from 'svelte/store';

// Store for active section ID
export const activeSection = writable<string>('');

// Store for the current path
export const currentPath = writable<string>(getCurrentPath());

// Options for the intersection observer
const observerOptions = {
  rootMargin: '-50% 0px -50% 0px'
};

// Store the observer instance
let observer: IntersectionObserver;

// Get or create the observer
function getObserver() {
  if (!observer) {
    observer = new IntersectionObserver((entries) => {
      const intersectingEntry = entries.find(entry => entry.isIntersecting);
      if (intersectingEntry) {
        activeSection.set(intersectingEntry.target.id);
      }
    }, observerOptions);
  }
  return observer;
}

// Custom action to observe sections
export function observeSection(element: HTMLElement) {
  const observer = getObserver();
  observer.observe(element);

  return {
    destroy() {
      observer.unobserve(element);
    }
  };
}

// Helper function to get the current page path
export function getCurrentPath() {
  if (typeof window !== 'undefined') {
    return window.location.pathname;
  }
  return '';
}

// Function to update the active section based on the current path and hash
function updateActiveSection() {
  const path = getCurrentPath();
  currentPath.set(path); // Update the currentPath store

  const hash = window.location.hash;
  if (hash) {
    const targetId = hash.substring(1);
    const targetElement = document.getElementById(targetId);
    if (targetElement) {
        // If the element is in the viewport, IntersectionObserver will catch it
        // If not, we set it as activeSection, and IntersectionObserver will
        // refine if another section becomes visible on scroll
      activeSection.set(targetId);
    } else {
      activeSection.set(''); // Reset if hash doesn't match an element
    }
  } else {
    activeSection.set(''); // Reset if no hash
  }
}

// Listen for path changes (pushState, replaceState, popstate)
if (typeof window !== 'undefined') {
  window.addEventListener('popstate', updateActiveSection);
  window.addEventListener('hashchange', updateActiveSection);

  const originalPushState = history.pushState;
  history.pushState = function (...args) {
    originalPushState.apply(history, args);
    updateActiveSection();
  };

  const originalReplaceState = history.replaceState;
  history.replaceState = function (...args) {
    originalReplaceState.apply(history, args);
    updateActiveSection();
  };
}

// Custom action for navigation links
export function navLink(element: HTMLAnchorElement) {
  const updateActive = () => {
    const href = element.getAttribute('href');
    if (!href) return;

    // Split the href into path and hash
    const [path, hash] = href.split('#');
    const basePath = path || '/'; // Default to '/' if no path

    activeSection.subscribe(currentSection => {
        currentPath.subscribe(currentPathValue => {
            const isActive = currentPathValue === basePath && (!hash || currentSection === hash);
            element.classList.toggle('nav-active', isActive);
        })
    });
  };

  updateActive(); // Call initially to set the correct state

  return {
    destroy() {
      // Cleanup if needed
    }
  };
}

export function navLinkPath(element: HTMLAnchorElement) {
    const updateActive = () => {
      const href = element.getAttribute('href');
      if (!href) return;
  
      currentPath.subscribe(currentPathValue => {
        // Check if the current path starts with the link's href
        // Consider it a match if it's exactly the href or if it's followed by a slash
        const isActive = currentPathValue === href || currentPathValue.startsWith(href + '/');
        element.classList.toggle('nav-active', isActive);
      });
    };
  
    updateActive();
  
    return {
      destroy() {
        // Cleanup if needed
      }
    };
  }