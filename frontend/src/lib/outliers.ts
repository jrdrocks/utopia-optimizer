
type OutlierValue = null | 'min' | 'max';
type InputValue = number | null;

export function detectOutliers(data: InputValue[][], xpct: number = 5): OutlierValue[][] {
    // Initialize result array
    const result: OutlierValue[][] = data.map(row => 
        Array(row.length).fill(null)
    );

    // Flatten array and create value-position mapping, excluding nulls
    const valuePositions = data.flatMap((row, rowIndex) => 
        row.map((value, colIndex) => ({
            value,
            row: rowIndex,
            col: colIndex
        }))
    ).filter(item => item.value !== null);

    // If no valid numbers, return the result array as is (all null)
    if (valuePositions.length === 0) {
        return result;
    }

    // Sort by value for percentile calculations
    const sortedPositions = [...valuePositions].sort((a, b) => 
        (a.value as number) - (b.value as number)
    );
    
    // Calculate quartiles
    const q1 = sortedPositions[Math.floor(sortedPositions.length * 0.25)].value as number;
    const q3 = sortedPositions[Math.floor(sortedPositions.length * 0.75)].value as number;
    
    // Calculate IQR
    const iqr = q3 - q1;
    
    // Define bounds
    const lowerBound = q1 - (1.5 * iqr);
    const upperBound = q3 + (1.5 * iqr);

    // Track number of outliers
    let minOutliers = 0;
    let maxOutliers = 0;
    const totalElements = valuePositions.length;
    const minRequired = Math.max(1, Math.ceil((xpct / 100) * totalElements));

    // First pass: Mark statistical outliers
    for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < data[i].length; j++) {
            const value = data[i][j];
            if (value === null) continue;
            
            if (value < lowerBound) {
                result[i][j] = 'min';
                minOutliers++;
            }
            if (value > upperBound) {
                result[i][j] = 'max';
                maxOutliers++;
            }
        }
    }

    // Second pass: If we don't have enough outliers, mark the extremes
    if (minOutliers < minRequired || maxOutliers < minRequired) {
        // Reset existing outliers
        result.forEach(row => row.fill(null));

        // Mark minimum values
        for (let i = 0; i < minRequired; i++) {
            const pos = sortedPositions[i];
            result[pos.row][pos.col] = 'min';
        }

        // Mark maximum values
        for (let i = 0; i < minRequired; i++) {
            const pos = sortedPositions[sortedPositions.length - 1 - i];
            result[pos.row][pos.col] = 'max';
        }
    }

    return result;
}
