<script lang="ts">
  import type { TableData } from '$lib/data';
  import Number from '$lib/components/Number.svelte';
  import { Table, TableBody, TableBodyCell, TableBodyRow, TableHead, TableHeadCell } from 'flowbite-svelte';

  // Cell and Row interfaces
  interface Cell {
    value: string | number | null;
    isHeader: boolean;
    isEmpty: boolean;
  }

  interface Row {
    cells: Cell[];
    isHeader: boolean;
    isHighlight: boolean; // For sum rows
  }

  export let tableData: TableData | [TableData, TableData];
  export let grouping: string | undefined = undefined;
  export let cumulative: string | undefined = undefined;
  export let splitBefore: string | string[] | undefined = undefined;

  // Data processing
  const processedTableData = Array.isArray(tableData) ? tableData[0] : tableData;
  const groupData = Array.isArray(tableData) ? tableData[1] : null;
  // Transform data to Row[]
  function transformDataToRows(
    tableData: TableData,
    groupData: TableData | null = null,
    grouping: string | undefined = undefined,
    cumulative: string | undefined = undefined,
  ): Row[] {
    let rows: Row[] = [];

    // Handle headers (only add once)
    const headerCells: Cell[] = tableData.colindex.names.map((name) => ({
      value: name,
      isHeader: true,
      isEmpty: false,
    }));
    const headerRow: Row = { cells: headerCells, isHeader: true, isHighlight: false };

    // Handle grouping and cumulative calculations if applicable
    if (grouping && groupData && cumulative) {
      const grouped: Record<string, Record<string, Cell>[]> = {};
      const groupIndex = tableData.colindex.lookup[grouping] - 1;

      tableData.columns.forEach((column, colIndex) => {
        column.forEach((value, rowIndex) => {
          const groupName = tableData.columns[groupIndex][rowIndex] as string;

          if (!grouped[groupName]) {
            grouped[groupName] = [];
          }

          const rowValues: Record<string, Cell> = {};
          tableData.colindex.names.forEach((name, index) => {
            const cellValue = tableData.columns[index][rowIndex];
            rowValues[name] = { value: cellValue, isHeader: false, isEmpty: cellValue === '' || cellValue === null };
          });

          if (!grouped[groupName].some((existingRow) => Object.keys(existingRow).every((key) => existingRow[key].value === rowValues[key].value))) {
            grouped[groupName].push(rowValues);
          }
        });
      });

      // Add group totals
      Object.entries(grouped).forEach(([groupName, groupRows]) => {
        const groupTotalBooks = groupData.columns[1][groupData.columns[0].indexOf(groupName)];
        groupRows.push({
          [grouping]: { value: '', isHeader: false, isEmpty: true },
          [cumulative]: { value: groupTotalBooks, isHeader: false, isEmpty: groupTotalBooks === '' || groupTotalBooks === null },
          ...tableData.colindex.names
            .filter((name) => name !== grouping && name !== cumulative)
            .reduce((obj, name) => ({ ...obj, [name]: { value: '', isHeader: false, isEmpty: true } }), {}),
        });
      });

      // Flatten grouped data into rows
      Object.entries(grouped).forEach(([groupName, groupRows]) => {
        groupRows.forEach((row, index) => {
          const isLastRow = index === groupRows.length - 1;
          const cells: Cell[] = tableData.colindex.names.map((name) => ({
            ...row[name],
            value: name === grouping && index === 0 ? groupName : row[name].value, // Add group name to the first row
          }));
          rows.push({ cells, isHeader: false, isHighlight: isLastRow });
        });
      });
    } else {
      // Handle regular data (no grouping)
      for (let i = 0; i < tableData.columns[0].length; i++) {
        const rowCells: Cell[] = tableData.columns.map((column) => {
          const cellValue = column[i];
          return { value: cellValue, isHeader: false, isEmpty: cellValue === '' || cellValue === null };
        });
        rows.push({ cells: rowCells, isHeader: false, isHighlight: false });
      }
    }
    rows.unshift(headerRow); //add the header as very first line

    return rows;
  }

  // Function to create sections for splitting
  function createTableSections(rows: Row[], splitBefore: string | string[] | undefined): Row[][] {
    if (!splitBefore) {
      return [rows]; // No splitting, one section
    }

    const splitValues = Array.isArray(splitBefore) ? splitBefore : [splitBefore];
    const splitIndices: number[] = [];

    // Find indices where to split (start from index 1 to skip the header row)
    rows.forEach((row, index) => {
      if (index > 0) {
        // Skip the header row (index 0)
        const firstCellValue = row.cells[0].value;
        if (typeof firstCellValue === 'string' && splitValues.includes(firstCellValue)) {
          splitIndices.push(index);
        }
      }
    });

    // Create sections based on split indices
    const sections: Row[][] = [];
    let startIndex = 1; // Start from index 1 to skip the header row
    splitIndices.forEach((splitIndex) => {
      sections.push(rows.slice(startIndex, splitIndex)); // Add section before split
      startIndex = splitIndex;
    });
    sections.push(rows.slice(startIndex)); // Add the last section

    // Pad sections with empty rows to make them equal length (consider header row)
    const maxSectionLength = Math.max(...sections.map((section) => section.length));
    sections.forEach((section) => {
      while (section.length < maxSectionLength) {
        const emptyCells: Cell[] = processedTableData.colindex.names.map(() => ({ value: '', isHeader: false, isEmpty: true }));
        section.push({ cells: emptyCells, isHeader: false, isHighlight: false });
      }
    });

    // Insert the header row at the beginning of each section
    sections.forEach((section) => {
      section.unshift(rows[0]); // Insert header row (rows[0]) at the beginning
    });

    return sections;
  }

  // Transform data to Row[]
  let dataRows: Row[] = transformDataToRows(processedTableData, groupData, grouping, cumulative);

  // Create table sections
  let tableSections: Row[][] = createTableSections(dataRows, splitBefore);
</script> 

  <Table class="table-fixed !m-0">
    <TableHead>
      {#each Array(tableSections.length) as _, sectionIndex}
        {#each tableSections[sectionIndex][0].cells as cell}
          <TableHeadCell>{cell.value}</TableHeadCell>
        {/each}
      {/each}
    </TableHead>
    <TableBody>
      {#each Array(tableSections[0].length - 1) as _, rowIndex}
        <TableBodyRow class={tableSections[0][rowIndex + 1]?.isHighlight ? 'bg-gray-50' : ''}>
          {#each tableSections as section}
            {#each section[rowIndex + 1].cells as cell}
              <TableBodyCell>
                {#if typeof cell.value === 'number'}
                  <Number number={cell.value} />
                {:else}
                  {cell.value}
                {/if}
              </TableBodyCell>
            {/each}
          {/each}
        </TableBodyRow>
      {/each}
    </TableBody>
  </Table> 