export interface BuildingTarget {
    min: number | null;
    max: number | null;
    eq: number | null;
    as_effective: boolean;
}

export interface OptimizationInput {
    races: Record<string, boolean>;
    personalities: Record<string, boolean>;
    acres: number | null;
    honor: string;
    science_days: number | null;
    science_books: number | null;
    excluded_self_spells: Record<string, boolean>;
    income: number | null;
    runes: number | null;
    wpa: number | null;
    tpa: number | null;
    opt_target: string;
    dpa: number | null;
    opa: number | null;
    opa_dpa_ratio: number | null;
    buildings: Record<string, BuildingTarget>;
    sciences: Record<string, number | null>;
    extra_targets: Record<string, number | null>;
    overrides: string | null;
}

export interface JsonRequest {
    form: {
        RACE_selected: string[];
        PERS_selected: string[];
    };
    RACE: string;
    PERS: string;
    HONOR: string;
    ACRES: number;
    SCIENCE: number | null;
    SCIENCE_DAYS: number | null;
    SCIENCE_STATE: {
        scientists: Array<{
            books: number;
            cat: null;
        }>;
        training_done: number;
    };
    SELF_SPELLS_EXCLUDE: string[];
    INCOME: number;
    RUNES: number;
    MOD_WPA: number;
    MOD_TPA: number;
    OPT_RAW_TPA_AND_WPA: boolean;
    OPA_DPA_RATIO?: number;
    DPA?: number;
    OPA?: number;
    OPT_FOR_NW: boolean;
    OPT_FOR_TURTLE_DEF: boolean;
    BUILDING_TARGETS: Record<string, {
        min?: number;
        max?: number;
        eq?: number;
        as_effective?: boolean;
    }>;
    TARGETS: Record<string, number>;
    SCIENCE_BASE: Record<string, number>;
    OVERRIDES: any[];
}

const DB_NAME = 'utopiaopt';
const DB_VERSION = 1;
const STORE_NAME = 'optimizations';

interface OptimizationResult {
    input: OptimizationInput;
    result: any;
    timestamp: number;
}

let db: IDBDatabase | null = null;

function initDB(): Promise<IDBDatabase> {
    return new Promise((resolve, reject) => {
        if (db) return resolve(db);

        const request = indexedDB.open(DB_NAME, DB_VERSION);

        request.onupgradeneeded = (event) => {
            const db = (event.target as IDBOpenDBRequest).result;
            if (!db.objectStoreNames.contains(STORE_NAME)) {
                db.createObjectStore(STORE_NAME, { keyPath: 'hash' });
            }
        };

        request.onsuccess = (event) => {
            db = (event.target as IDBOpenDBRequest).result;
            resolve(db);
        };

        request.onerror = (event) => {
            reject((event.target as IDBOpenDBRequest).error);
        };
    });
}

export function saveRawInput(input: OptimizationInput): void {
    try {
        localStorage.setItem('utopiaopt_raw_input', JSON.stringify(input));
    } catch (error) {
        console.error('Failed to save input to localStorage:', error);
    }
}

export async function saveOptimization(input: OptimizationInput, result: any): Promise<string> {
    try {
        const [hash] = createJsonRequest(input);
        const db = await initDB();
        const transaction = db.transaction(STORE_NAME, 'readwrite');
        const store = transaction.objectStore(STORE_NAME);
        
        const optimization: OptimizationResult = {
            input,
            result,
            timestamp: Date.now()
        };

        store.put({ ...optimization, hash });
        return hash;
    } catch (error) {
        console.error('Failed to save optimization:', error);
        throw error;
    }
}

export async function getOptimizations(): Promise<Record<string, OptimizationResult>> {
    try {
        const db = await initDB();
        const transaction = db.transaction(STORE_NAME, 'readonly');
        const store = transaction.objectStore(STORE_NAME);
        const request = store.getAll();

        return new Promise((resolve, reject) => {
            request.onsuccess = () => {
                const optimizations = request.result.reduce((acc, opt) => {
                    acc[opt.hash] = opt;
                    return acc;
                }, {} as Record<string, OptimizationResult>);
                resolve(optimizations);
            };

            request.onerror = () => {
                reject(request.error);
            };
        });
    } catch (error) {
        console.error('Failed to load optimizations:', error);
        return {};
    }
}

export async function loadOptimization(hash: string): Promise<OptimizationResult | null> {
    try {
        const db = await initDB();
        const transaction = db.transaction(STORE_NAME, 'readonly');
        const store = transaction.objectStore(STORE_NAME);
        const request = store.get(hash);

        return new Promise((resolve, reject) => {
            request.onsuccess = () => resolve(request.result || null);
            request.onerror = () => reject(request.error);
        });
    } catch (error) {
        console.error('Failed to load optimization:', error);
        return null;
    }
}

export function loadRawInput(): OptimizationInput | null {
    try {
        const stored = localStorage.getItem('utopiaopt_raw_input');
        return stored ? JSON.parse(stored) : null;
    } catch (error) {
        console.error('Failed to load input from localStorage:', error);
        return null;
    }
}

function hashCode(s: string): number {
    var hash = 0,
        i, chr;
    if (s.length === 0) return hash;
    for (i = 0; i < s.length; i++) {
        chr = s.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}

export function createJsonRequest(input: OptimizationInput): [ string, JsonRequest ] {
    // Convert input to json_request format
    const request: JsonRequest = {
        form: {
            RACE_selected: Object.entries(input.races)
                .filter(([_, selected]) => selected)
                .map(([race]) => race),
            PERS_selected: Object.entries(input.personalities)
                .filter(([_, selected]) => selected)
                .map(([pers]) => pers),
        },
        RACE: 'ELF',
        PERS: 'SAGE',
        HONOR: input.honor || '',
        ACRES: input.acres || 0,
        SCIENCE: input.science_books || null,
        SCIENCE_DAYS: input.science_days,
        SCIENCE_STATE: {
            scientists: Array(6).fill({ books: 0, cat: null }),
            training_done: 0
        },
        SELF_SPELLS_EXCLUDE: Object.entries(input.excluded_self_spells)
            .filter(([_, excluded]) => excluded)
            .map(([spell]) => spell),
        INCOME: input.income || 0,
        RUNES: input.runes || 0,
        MOD_WPA: input.wpa || 0,
        MOD_TPA: input.tpa || 0,
        OPT_RAW_TPA_AND_WPA: false,
        OPT_FOR_NW: input.opt_target === 'OPNW' || input.opt_target === 'DPNW',
        OPT_FOR_TURTLE_DEF: input.opt_target === 'TURTLE_DEF',
        BUILDING_TARGETS: {},
        TARGETS: {},
        SCIENCE_BASE: {},
        OVERRIDES: []
    };

    // Handle optimization targets
    if (input.opa_dpa_ratio !== null && input.opa_dpa_ratio >= 0) {
        request.OPA_DPA_RATIO = input.opa_dpa_ratio;
    } else if (input.opt_target === 'OPA' || input.opt_target === 'OPNW') {
        if (input.dpa !== null && input.dpa >= 0) request.DPA = input.dpa;
    } else if (input.opt_target === 'DPA' || input.opt_target === 'DPNW') {
        if (input.opa !== null && input.opa >= 0) request.OPA = input.opa;
    }

    // Convert building targets
    for (const [building, target] of Object.entries(input.buildings)) {
        if (target.min !== null || target.max !== null || target.eq !== null || target.as_effective) {
            request.BUILDING_TARGETS[building] = {
                ...(target.min !== null && target.min >= 0 && { min: target.min }),
                ...(target.max !== null && target.max >= 0 && { max: target.max }),
                ...(target.eq !== null && target.eq >= 0 && { eq: target.eq }),
                ...(target.as_effective && { as_effective: target.as_effective })
            };
        }
    }

    // Convert science targets
    for (const [science, value] of Object.entries(input.sciences)) {
        if (value !== null && value >= 0) {
            request.SCIENCE_BASE[science] = value;
        }
    }

    // Convert extra targets
    for (const [target, value] of Object.entries(input.extra_targets)) {
        if (value !== null && value >= 0) {
            request.TARGETS[target] = value;
        }
    }

    const hash = hashCode(JSON.stringify(request)).toString(24);

    return [ hash, request ];
}
